<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('is_weekender/{idSupplier}','ApiController@weekendbin');
Route::get('nondeliverydays/{idBinType}/{idSupplier}','ApiController@nondeliverydays');
Route::get('alwayson/{idSupplier}','ApiController@alwaysOn');
Route::get('bintype','ApiController@showBinType');
Route::get('bintype/{idBinType}','ApiController@findBinType');
Route::get('sizes','ApiController@showSizes');
Route::get('sizes/{idSize}','ApiController@findSize');
Route::get('binextras','ApiController@showbinextras');
Route::get('places','ApiController@showplaces');
Route::get('routes/{from}/{type}','ApiController@showroutes');
Route::get('trip/{idBinType}/{from}/{to}','ApiController@findtrip');
Route::get('binhire/{idBinType}/{deliverydate}/{collectiondate}/{from}/{to}/{own}','ApiController@showBinHire');
Route::get('binhire_multi/{idBinType}/{deliverydate}/{collectiondate}/{from}/{to}/{own}','ApiController@show_multiple_destination');
Route::get('showstate/','ApiController@showState');
Route::get('showarea/{zipcode}','ApiController@showArea');
Route::get('showpostcode/{zipcode}','ApiController@showPostcode');
Route::get('findarea/{zipcode}', 'ApiController@findArea');
Route::get('findarea/all/{zipcode}', 'ApiController@findAreaAll');
Route::get('binhireoptions/{idBinType}/{idSupplier}','ApiController@binhireOptions');
Route::get('payment/{idbinhire}/{zipcode}/{deliverydate}/{collectiondate}','ApiController@payment');