@extends('child::layouts.template_form')
@section('child_content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Green Waste Bin Hire</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Green Waste</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('basePrice'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('basePrice') }}
								</div>
							@endif
							@if ($errors->has('baseStock'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('baseStock') }}
								</div>
							@endif
							
							@if ($errors->has('array_binprice.*'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('array_binprice.*') }}
								</div>
							@endif
							
							@if ($errors->has('array_stock.*'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('array_stock.*') }}
								</div>
							@endif
						
						
							@if (!empty($result))
								@if ($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'warning')
									<div class="alert alert-warning" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'notif')
									<div class="alert alert-danger" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Manage Green Waste</strong>
								</div>
									
								<div class="card-body">
								
									<form class="mb-2" action="{{url('child/')}}/4/editMiscDetails" method="POST">
										{{csrf_field()}}
										<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
										<input type="hidden" name="idSupplierChild" value="{{$star->idSupplier}}">
										<input type="hidden" name="idBinType" value="4">
										<div class=" form-row mb-2"> 
											<div class="col-12">
												<label for="extraHireage"><strong>Extra Hireage:</strong></label>
											</div>
											<div class="col-12 col-md-4">
												<div class="row">
													<div class="col-1">
														<span>$</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->extraHireagePrice}}" id="extraHireage" type="text" class="form-control ml-2 mr-2" name="extraHireage" readonly />
														@else
															<input id="extraHireage" type="text" class="form-control ml-2 mr-2" name="extraHireage" readonly />
														@endif
														
													</div>
													<div class="col-3 text-center">
														<span>extra hire per day after</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->extraHireageDays}}" id="extraHireageDays" type="text" class="form-control ml-2 mr-2" name="extraHireageDays" readonly />
														@else
															<input id="extraHireageDays" type="text" class="form-control ml-2 mr-2" name="extraHireageDays"  readonly />
														@endif
													</div>
													<div class="col-2">
														<span>days</span>
													</div>
												</div>
											</div>
										</div>
										<div class=" form-row mb-2">
											<div class="col-12">
												<label for="excessWeight"><strong>Excess Weight: </strong></label>
											</div>
											<div class="col-12 col-md-5">
												<div class="row">
													<div class="col-1">
														<span>$</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->excessWeightPrice}}" id="excessWeight" type="text" class="form-control ml-2 mr-2" name="excessWeight" readonly />
														@else
															<input  id="excessWeight" type="text" class="form-control ml-2 mr-2" name="excessWeight" readonly />
														@endif
													</div>
													<div class="col-8 text-center">
														<label>per tonne additional customer charge for weights exceeding allowance of 150kg per cubic metre.</label>
													</div>
												</div>
											</div>
										</div>
										<?php /*<button name="submit" type="submit" class="btn btn-primary">Save</button> */ ?>
									</form>
									<hr />
									<div class="extraServices">
										 <div class="row">
											<div class="col-12">
												<p><strong>Extras</strong></p>
												
											</div> 
											<div class="col-12">
												<form class="mb-2" action="{{url('child/')}}/binServicesExtra/4" method="POST">
													{{csrf_field()}}
													<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
													<input type="hidden" name="idSupplierChild" value="{{$star->idSupplier}}">
													<input type="hidden" name="idBinType" value="4">
													@if(!is_null($extraservices))
														<div class="form-row align-items-center">
															<?php foreach($extraservices as $k=>$v):?>
																<?php $form_value = null; foreach($getbinextraservices as $kk=>$vv):?>
																	<?php
																		
																		if($vv->idExtraService == $v->idExtraServices){
																			$form_value = $vv->charge;
																		}
																	?>
																<?php endforeach;?>
																<div class="col-auto">
																	<label for="{{$v->slug}}">{{$v->name}}</label>
																	<input type="hidden"  name="extras_hidden[]" value="{{$v->idExtraServices}}">
																	<input name="extras[<?=$k?>]" type="text" class="form-control mb-2" id="{{$v->slug}}" required value="<?=(!is_null($form_value)) ? $form_value : 0 ;?>" readonly>
																</div>
															<?php endforeach; ?>
															
														</div>
														<?php /*
														<div class="form-row align-items-center">
															<div class="col-auto">
																<button name="submit" type="submit" class="btn button-yellow">Save</button>
															</div>
														</div>
														*/ ?>
													@else
														<div class="form-row align-items-center">
															<div class="col-auto">
																<p>Please add at least one extras for this bin type</p>
															</div>
														</div>
													@endif
												</form>
											</div>
										 </div>
									</div>
									<hr />
									<p>Use this page to manage your rates:</p>
									<ul>
										<li style="list-style-type:none">
											<i class="fa fa-pencil mr-2"></i> Click on the edit pencil button to change your daily rates. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-check mr-2"></i>  Click on the tick button to save your changes. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-trash mr-2"></i> Click on the delete trash button to reset all your rates and inventory to 0 for the selected bin size. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-ban mr-2"></i> Click on each checkbox to block any delivery dates. Skip bins will not be offered for dates that are checked.
										</li>
									</ul>

									<div class="schedule-wrapper">
										<div class="row">
											<div class="col-12">
												@if(!is_null($offset))
    														<?php $offsetPage = $offset; ?>
    													@else
    														<?php $offsetPage = $dateNow;?>
    													@endif
												<strong class="mb-2">{{$dateNow}}</strong>

												<table class="table table-responsive table-bordered" style="border:none;">
													
  													<tr>
  														
    													<th class="tg-yw4l" colspan="4">
    														<?php $prevDate = date('d-m-Y', $offsetPage)?>
    														<a href="{{ url('child/') }}/{{$star->idSupplier}}/4/{{$binsize}}/calendarOffset/<?= date('d-m-Y', strtotime($prevDate.'-2 weeks'))?>">Prev</a>
    													</th>
    													<th class="tg-yw4l" style="background:#eaeaea">Default</th>
    													
														
														@for ($i = 0; $i < 14; $i++)
															<?php $a[] = $offsetPage+($i*24*60*60);?>
    														<th class="tg-yw4l"><?php echo date('D', $a[$i]).' <br/>'.date('d-m', $a[$i])?></th>
    														<?php $lastDate = date('d-m-Y', $a[$i]);?>
														@endfor
    													<th class="tg-yw4l"><a href="{{ url('child/') }}/{{$star->idSupplier}}/4/{{$binsize}}/calendarOffset/<?= date('d-m-Y', strtotime($lastDate.'+1 day'))?>">Next</a></th>
  													</tr>

  													<tr>
    													<td class="tg-lqy6" colspan="5">Non Delivery Days</td>
														<td class="tg-yw4l text-center" colspan="15"><strong>Child account's non delivery dates are inherited from the main account's non delivery dates</strong></td>
														<?php /**
    													<form action="{{ url('child/') }}/4/editNonDeliveryDays" method="post">
    														{{csrf_field()}}
    														<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
															<input type="hidden" name="idSupplierChild" value="{{$star->idSupplier}}">
															<input type="hidden" name="idBinType" value="4">
															<?php $status = '';?>
									  						
    														@for ($i = 0; $i < 14; $i++)
															<?php $a[] = $offsetPage+($i*24*60*60);?>
																<?php $get_day_now = date('D', $a[$i]); ?>
																@if($get_day_now == 'Sat')
																	@if($supplierData->isOpenSaturday == 0)
																		<?php $status = 'checked'; ?>
																	@else
																		<?php $status = '';?>
																	@endif
																@elseif($get_day_now == 'Sun')
																	@if($supplierData->isOpenSunday == 0)
																		<?php $status = 'checked';?>
																	@else
																		<?php $status = '';?>
																	@endif
																@else 
																	<?php $status = '';?>
																@endif
																
																<?php $status_1 = ''?>
																@if(!is_null($nonDelivery))
									  								<?php foreach($nonDelivery as $dataDelivery):?>
																		<?php if($dataDelivery->date == date('Y-m-d', $a[$i])):?>
																			<?php $status_1 = 'checked';break;?>
																		<?php else:?>
																			<?php $status_1 = '';?>
																		<?php endif;?>
																	<?php endforeach;?>
																@else
																		<?php $status_1 = '';?>
																@endif
    															<th class="tg-yw4l">
    																<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																<input name="nondelivery[<?=$i;?>]" <?=$status;?> <?=(!is_null($status_1)) ? $status_1 : '';?> type="checkbox" id="nondelivery">
    															</th>
															@endfor
															<td class="tg-yw4l">
																<button name="submit" type="submit" class="btn btn-danger">
																	<i class="fa fa-ban mr-2"></i>
																</button>
															</td>
    													</form>
													**/ ?>
  													</tr>
  													@foreach($getBinSize as $size)
  													<form action="{{ url('child/') }}/4/editBinServicePrice" method="POST">
  														{{csrf_field()}}
    													<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
														<input type="hidden" name="idSupplierChild" value="{{$star->idSupplier}}">
														<input type="hidden" name="idBinType" value="4">
														<input type="hidden" name="idBinSize" value="{{$size->idSize}}">
  														<tr>
    														<td class="tg-baqh" colspan="3" rowspan="2">{{$size->size}}</td>
    														<td class="tg-yw4l">Price</td>
    														<td style="background:#eaeaea">
    															<?php $value = 0; ?>
																@if(!is_null($baseprice))
    																@if ($size->idSize == $binsize)
    																	<?php foreach($baseprice as $data):?>	
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					<?php $value = $items->baseprice ?>
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<input type="text" value="{{$value}}" class="form-control bin-input" name="basePrice" id="binPrice" required="">
    																@else
    																	<?php foreach($baseprice as $data):?>
    																		@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					<?php $value = $items->baseprice ?>
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		{{$value}}
																	@endif
																@else
																	<input type="text" value="" class="form-control bin-input" name="basePrice" id="binPrice" required="">
																@endif
    														</td>
																<?php $data = '';?>
    															@for ($i = 0; $i < 14; $i++)
																	<?php $a[] = $offsetPage+($i*24*60*60);?>
																	@if(!is_null($baseprice))
																		@if ($size->idSize == $binsize)
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							<?php $value = 0 ?>
																						@else
																							<?php $value = $baseitems->baseprice ?>
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							<?php $value =  $itemsvalue->price; break;?>
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<th class="tg-yw4l">
    																			<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																			<input type="text" value="{{$value}}" class="form-control bin-input" name="binPrice[<?=$i;?>]" id="binPrice[]">
    																		</th>
																		@else
																			<?php $value = 0; ?>
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							<?php $value = 0 ?>
																						@else
																							<?php $value = $baseitems->baseprice ?>
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							<?php $value =  $itemsvalue->price; break;?>
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<th class="tg-yw4l">
																				{{$value}}
																			</th>
																		@endif
																	@else
																		<?php $value = 0?>
																	@endif
																@endfor
															@if($size->idSize == $binsize)
															<td class="tg-yw4l" rowspan="2">
																<button name="button" type="submit" class="btn btn-primary">
																	<i class="fa fa-check"></i>
																</button>
															</td>
															@endif
  														</tr>
  														<tr>
    														<td class="tg-yw4l">Stock</td>
    														<td style="background:#eaeaea">
																@if(!is_null($baseprice))
    																@if ($size->idSize == $binsize)
    																	<?php foreach($baseprice as $data):?>	
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					<?php $value = $items->basestock ?>
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<input type="text" value="{{$value}}" class="form-control bin-input" name="baseStock" id="baseStock" required="">
    																@else
    																	<?php foreach($baseprice as $data):?>	
    																		@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					<?php $value = $items->basestock ?>
																				<?php endforeach?>
																			@endif
																		<?php endforeach?>
																		{{$value}}
																	@endif
																@else
																	<input type="text" value="{{$value}}" class="form-control bin-input" name="baseStock" id="baseStock" required="">
																@endif
    														</td>
																<?php $data = '';?>
    															@for ($i = 0; $i < 14; $i++)
																	<?php $a[] = $offsetPage+($i*24*60*60);?>
																	@if(!is_null($baseprice))
																		@if ($size->idSize == $binsize)
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->basestock))
																							<?php $value = 0 ?>
																						@else
																							<?php $value = $baseitems->basestock ?>
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							<?php $value =  $itemsvalue->stock; break;?>
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<td class="tg-yw4l">
    																			<input type="text" value="{{$value}}" class="form-control bin-input" name="stock[<?=$i;?>]" id="stock[]">
    																		</td>
																		@else
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->basestock))
																							<?php $value = 0 ?>
																						@else
																							<?php $value = $baseitems->basestock ?>
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							<?php $value =  $itemsvalue->stock; break;?>
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<td class="tg-yw4l">
																				{{$value}}
																			</td>
																		@endif
																	@else
																		<?php $value = 0?>
																	@endif
																@endfor
  														</tr>
  													</form>
  													@endforeach
												</table>
											</div>
										</div>
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
