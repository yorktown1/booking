@extends('template')

@section('content')
	<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Child Account</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item ">Child Account</li>
									<li class="breadcrumb-item active">Create Child Account</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
							@if ($errors->has('websiteAdminContact'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminContact') }}
								</div>
							@endif
							@if ($errors->has('websiteAdminPhone'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminPhone') }}
								</div>
							@endif
							@if ($errors->has('websiteAdminMobile'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminMobile') }}
								</div>
							@endif
							@if ($errors->has('customerServicesEmail1'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('customerServicesEmail1') }}
								</div>
							@endif
							
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<a href="{{url('child/')}}/create_child_account" class="mb-2 btn button-yellow">Create Child Accounts</a>
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Create Child Account</strong>
								</div>
									
								<div class="card-body">
									
									<form class="mb-2" action="{{url('child/add_child_account')}}" method="POST">
										{{csrf_field()}}
										<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
										<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
										
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminContact">Account Name</label>
												<input id="websiteAdminContact" type="text" class="form-control" name="websiteAdminContact" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServicesContact">Customer Services Contact</label>
												<input id="customerServicesContact" type="text" class="form-control" name="customerServicesContact"  />
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminPhone">Phone Number</label>
												<input id="websiteAdminPhone" type="text" class="form-control" name="websiteAdminPhone" value="{{$supplierData->phonenumber}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServicePhone">Customer Services Phone</label>
												<input id="customerServicePhone" type="text" class="form-control" name="customerServicePhone" value="{{$supplierData->customerServicePhone}}"/>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminMobile">Mobile Phone</label>
												<input id="websiteAdminMobile" type="text" class="form-control" name="websiteAdminMobile" value="{{$supplierData->mobilePhone}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServiceMobile">Customer Services Mobile</label>
												<input id="customerServiceMobile" type="text" class="form-control" name="customerServiceMobile" value="{{$supplierData->customerServiceMobile}}" />
											</div>
											
										</div>
										<div class="form-group">
											<label for="customerServicesEmail1">Email 1</label>
											<input id="customerServicesEmail1" type="text" class="form-control" name="customerServicesEmail1" value="{{$supplierData->email}}" required=""/>
										</div>
										<div class="form-group">
											<label for="customerServicesEmail2">Email 2</label>
											<input id="customerServicesEmail2" type="text" class="form-control" name="customerServicesEmail2" value="{{$supplierData->email2}}" />
										</div>
										
										<div class="form-group">
											<label for="fullAddress">Address</label>
											<textarea name="fullAddress" id="fullAddress" class="form-control">{{$supplierData->fullAddress}}</textarea>
										</div>
										<div class="form-group">
											<label for="fullAddress">ABN</label>
											<input id="abn" type="text" class="form-control" name="abn" value="{{$supplierData->abn}}" readonly />
										</div>
										<div class="form-group">
  											<p><strong>These field mean that your bin hire services are eligible to be booked on the weekend.</strong> <strong class="text-danger">This settings are inherited by your primary account's weekend bin delivery.</strong></p>
										</div>
										<button name="submit" type="submit" class="btn button-yellow">Save Child Account</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@stop
