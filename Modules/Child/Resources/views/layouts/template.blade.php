<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Supplier Child Account</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/plugins/datetimepicker/css/daterangepicker.css')}}" rel="stylesheet" /> 
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<body class="adminbody child">
	<div id="main">
		<div class="headerbar">
			<div class="headerbar-left">
				<a href="{{url('/away')}}" class="logo"><img alt="Logo" src="{{url('assets/images/white-logo.png')}}" /></a>
			</div>
			<nav class="navbar-custom">
				
				<ul class="list-inline float-right mb-0">
					<li class="list-inline-item"><h5>Child Account Panel</h5></li>
					<li class="list-inline-item dropdown notif">
						<a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<img src="{{url('assets/images/admin.png')}}" alt="Profile image" class="avatar-rounded">
						</a>
						<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5 class="text-overflow"><small>{{$star->name}} , {{$star->email}}</small> </h5>
							</div>
	
							<!-- item-->
							<a href="{{url('child/')}}/details_service_zone/{{$star->idSupplier}}" class="dropdown-item notify-item">
								<i class="fa fa-user"></i> <span>Profile</span>
							</a>
							
							<!-- item-->
							<a href="{{url('child/back_to_parent')}}" class="dropdown-item notify-item">
								<i class="fa fa-sign-out"></i> <span>Parent Area</span>
							</a>
						</div>
					</li>
				</ul>
				
				<ul class="list-inline menu-left mb-0">
					<li class="float-left">
						<button class="button-menu-mobile open-left">
							<i class="fa fa-fw fa-bars"></i>
						</button>
					</li>
				</ul>
			</nav>
		</div>
		<!-- End Navigation -->
		<!-- Left Sidebar -->
		<div class="left main-sidebar">
			<div class="sidebar-inner leftscroll">
				<div id="sidebar-menu">
					<ul>
						<li class="submenu">
							<?php $request = 'child/panel/'.$star->idSupplier; $request_1 = 'child/instructions/'.$star->idSupplier;?>
							<a {{{ (Request::is($request) ||  Request::is($request_1)  ? 'class=active' : '') }}} href="{{url('child/')}}/instructions/{{$star->idSupplier}}"><i class="fa fa-fw fa-info-circle"></i><span> Intructions & Informations</span> </a>
						</li>
						
						<li class="submenu">
							<?php $request = 'child/details_service_zone/'.$star->idSupplier;?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is('details_service_zone/{status}/{message}') ? 'class=active' : '') }}} href="{{url('child/')}}/details_service_zone/{{$star->idSupplier}}"><i class="fa fa-fw fa-id-card-o"></i><span> Details & Service Zone</span> </a>
						</li>
						<li class="submenu">
							<?php $request = 'child/mixed_renovation/'.$star->idSupplier; $request_1 = 'child/mixed_renovation/'.$star->idSupplier.'/{status}/{message}';?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is($request_1) ? 'class=active' : '') }}} href="{{url('child/')}}/mixed_renovation/{{$star->idSupplier}}"><i class="fa fa-fw fa-circle"></i><span> Mixed Builders Waste</span> </a>
						</li>
						<li class="submenu">
							<?php $request = 'child/heavy_landscaping_waste/'.$star->idSupplier; $request_1 = 'child/heavy_landscaping_waste/'.$star->idSupplier.'/{status}/{message}';?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is($request_1) ? 'class=active' : '') }}} href="{{url('child/')}}/heavy_landscaping_waste/{{$star->idSupplier}}"><i class="fa fa-fw fa-circle"></i><span> Heavy/Landscaping Waste</span> </a>
						</li>
						<li class="submenu">
							<?php $request = 'child/domestic_light/'.$star->idSupplier; $request_1 = 'child/domestic_light/'.$star->idSupplier.'/{status}/{message}';?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is($request_1) ? 'class=active' : '') }}} href="{{url('child/')}}/domestic_light/{{$star->idSupplier}}"><i class="fa fa-fw fa-circle"></i><span> Domestic Light Waste </span> </a>
						</li>
						<li class="submenu">
							<?php $request = 'child/green_waste/'.$star->idSupplier; $request_1 = 'child/green_waste/'.$star->idSupplier.'/{status}/{message}';?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is($request_1) ? 'class=active' : '') }}} href="{{url('child/')}}/green_waste/{{$star->idSupplier}}"><i class="fa fa-fw fa-circle"></i><span> Green Waste </span> </a>
						</li>
							<li class="submenu">
							<?php $request = 'child/other_bin/'.$star->idSupplier; $request_1 = 'child/other_bin/'.$star->idSupplier.'/{status}/{message}';?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is($request_1) ? 'class=active' : '') }}} href="{{url('child/')}}/other_bin/{{$star->idSupplier}}"><i class="fa fa-fw fa-circle"></i><span> Other Bin </span> </a>
						</li>
						<li class="submenu">
							<?php $request = 'child/seasonalpricing/'.$star->idSupplier; $request_1 = 'child/seasonal_filter/result/'.$star->idSupplier.'/{idSupplier}/{idBinType}';?>
							<a {{{ (Request::is($request) ? 'class=active' : '') }}} {{{ (Request::is($request_1) ? 'class=active' : '') }}} href="{{url('child/')}}/seasonalpricing/{{$star->idSupplier}}"><i class="fa fa-fw fa-dollar"></i><span> Seasonal Price</span> </a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- End Sidebar -->
		@section('child_content')
		@show
		<!-- END content-page -->
		
		<footer class="footer">
			<span class="text-right">
				Copyright <a target="_blank" href="https://somsweb.com.au">SOMS Web</a>
			</span>
		</footer>
	</div>
<!-- END main -->
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
</body>
</html>