@extends('child::layouts.template')
@section('child_content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Details & Service Area</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Details & Service Zone</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('websiteAdminContact'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminContact') }}
								</div>
							@endif
							@if ($errors->has('websiteAdminPhone'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminPhone') }}
								</div>
							@endif
							@if ($errors->has('websiteAdminMobile'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('websiteAdminMobile') }}
								</div>
							@endif
							@if ($errors->has('customerServicesEmail1'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('customerServicesEmail1') }}
								</div>
							@endif
							
							
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
							
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Child account information for: </strong> {{ $star->name}} 
								</div>
									
								<div class="card-body">
									<form action="{{url('child/editSupplierDetails')}}" method="post">
										{{csrf_field()}}
										<input type="hidden" value="{{$star->idSupplier}}" name="idSupplierChild">
										<input type="hidden" value="{{$supplierData->idUser}}" name="idUser">
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminContact">Account Name</label>
												<input id="websiteAdminContact" type="text" class="form-control" name="websiteAdminContact" value="{{$star->name}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServicesContact">Customer Services Contact</label>
												<input id="customerServicesContact" type="text" class="form-control" name="customerServicesContact" value="{{$star->customerServiceContact}}"  />
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminPhone">Phone Number</label>
												<input id="websiteAdminPhone" type="text" class="form-control" name="websiteAdminPhone" value="{{$star->phonenumber}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServicePhone">Customer Services Phone</label>
												<input id="customerServicePhone" type="text" class="form-control" name="customerServicePhone" value="{{$star->customerServicePhone}}"/>
											</div>
											
										</div>
										<div class="form-row">
											<div class="col-12 col-lg-6">
												<label for="websiteAdminMobile">Mobile Phone</label>
												<input id="websiteAdminMobile" type="text" class="form-control" name="websiteAdminMobile" value="{{$star->mobilePhone}}" required=""/>
											</div>
											<div class="col-12 col-lg-6">
												<label for="customerServiceMobile">Customer Services Mobile</label>
												<input id="customerServiceMobile" type="text" class="form-control" name="customerServiceMobile" value="{{$star->customerServiceMobile}}" />
											</div>
											
										</div>
										<div class="form-group">
											<label for="customerServicesEmail1">Email 1</label>
											<input id="customerServicesEmail1" type="text" class="form-control" name="customerServicesEmail1" value="{{$star->email}}" required=""/>
										</div>
										<div class="form-group">
											<label for="customerServicesEmail2">Email 2</label>
											<input id="customerServicesEmail2" type="text" class="form-control" name="customerServicesEmail2" value="{{$star->email2}}" />
										</div>
										
										<div class="form-group">
											<label for="fullAddress">Address</label>
											<textarea name="fullAddress" id="fullAddress" class="form-control">{{$star->fullAddress}}</textarea>
										</div>
										<div class="form-group">
											<label for="fullAddress">ABN</label>
											<input id="abn" type="text" class="form-control" name="abn" value="{{$star->abn}}" readonly />
										</div>
										<div class="form-group">
											<p><strong>Note: Your weekend delivery settings are inherited by your main account weekend delivery settings.</strong></p>
										</div>
										@if(!is_null($star->seasonal_on))
											<div class="form-group">
												<p><strong>By ticking this fields, you will be switched in to seasonal pricing mode instead of your regular bin price.  </strong><strong class="text-danger">Please note, in order to have this mode turned on, you must have filled your main (non-seasonal) pricing on the each of bin types and sizes.</strong></p>
												<p><strong class="text-danger"> Also, please make sure you have turned the always in stock mode to use this mode. Otherwise, the special seasonal mode will not activated. </strong></p>
											</div>
											<div class="form-group">
												<input class="seasonal_on" name="seasonal_on" type="checkbox"  <?=($star->seasonal_on == 1) ? 'checked' : ''?>  id="seasonal_on">
												<label class="form-check-label" for="seasonal_on">
													Turn on Seasonal Pricing Mode
												</label>
											</div>
											<div class="form-group">
												<label for="seasonal_title">Special Seasonal Price Title</label>
												<textarea name="seasonal_title" id="seasonal_title" class="form-control" >{{$star->seasonal_title}}</textarea>
											</div>
										@endif
										<button name="submit" type="submit" class="btn button-yellow">Save Child Account</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>

						

						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Service area for: </strong> {{ $star->name}}
									
								</div>
								<div class="card-body">
									
									<div id="accordion" role="tablist">
										<?php $i = 1;?>
										@if(!is_null($serviceAreaParent))
											@foreach ($serviceAreaParent as $data)
											<div class="card mb-2">
												<div class="card-header" role="tab" id="headingOne">
									  				<h5 class="mb-0">
														<a data-toggle="collapse" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
										  				{{$data->area}}
														</a>
									  				</h5>
												</div>

												<div id="collapse{{$i}}" class="collapse <?=($i == 1) ? 'show' : ''?>" role="tabpanel" aria-labelledby="heading{{$i}}" data-parent="#accordion">
									  				<div class="card-body">
														<div class="form-group">
															<input class="selectall" name="selectall"  type="checkbox" value="selectall" id="selectall">
  															<label class="form-check-label" for="zipcode">
   					 											Select All
  															</label>

  															<input class="unselectall" name="unselectall"  type="checkbox" value="unselectall" id="unselectall">
  															<label class="form-check-label" for="zipcode">
   					 											Un-select All
  															</label>
  														</div>
									  					<form class="areaform" action="{{url('child/')}}/editArea/{{$data->zipcode}}" method="post">
									  						{{csrf_field()}}
									  						<input type="hidden" name="zipcodeparent" value="{{$data->zipcode}}" />
									  						<input type="hidden" value="{{$supplierData->idUser}}" name="idUser" />
															<input type="hidden" value="{{$star->idSupplier}}" name="idSupplierChild" />
									  						<?php $index = 0;?>
															
									  						@if(!is_null($childArea))
																@foreach ($childArea[$data->idArea][$data->zipcode] as $child)
																	<div class="form-group">
																		<?php $status = '';?>
																		@if(!is_null($childServiceArea))
																		<?php foreach($childServiceArea[$child->parentareacode][$child->idArea] as $servArea):?>
																			<?php if($servArea->idServiceArea == $child->idArea):?>
																				<?php $status = 'checked';break;?>
																			<?php else:?>
																				<?php $status = '';?>
																			<?php endif;?>
																		<?php endforeach;?>
																		@else
																			<?php $status = '';?>
																		@endif
																		<input class="zipcode" name="idArea[]" <?=(!is_null($status) ? $status : '')?> type="checkbox" value="{{$child->idArea}}" id="zipcode">
																		<label class="form-check-label" for="zipcode">
																				{{$child->zipcode}} - {{$child->area}}
																		</label>
																	</div>
	
																	<?php $index++;?>
																@endforeach
															@endif
									  						<button name="submit" type="submit" class="btn button-yellow">Save Area</button>
									  					</form>
													</div>
												</div>
											</div>
											<?php $i++?>
											@endforeach
										@endif
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
