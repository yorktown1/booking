@extends('template')

@section('content')
	<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Child Account</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Child Account</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
							@if ($errors->has('no_child'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('no_child') }}
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						
						<div class="col-12">
							<a href="{{url('child/')}}/create_child_account" class="mb-2 btn button-yellow">Create Child Accounts</a> 
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Manage Child Account</strong>
								</div>
									
								<div class="card-body">
									
									@if ($supplierData->hasChild == 0 || is_null($supplierData->hasChild))
										<div class="alert alert-danger mr-2 text-center" role="alert">
											You have no child account. <br />
											<a href="{{url('child/create_child_account')}}" class="btn button-yellow">Create one now</a>
										</div>
									@else
										<div class="table-responsive">
											<table class="table table-bordered" style="border:none;">
												<thead class="text-center">
													<th>Account ID</th>
													<th>Name</th>
													<th>Action</th>
												</thead>
												<tbody class="text-center">
													@foreach($child as $data)
														<tr>
															<td>{{$data->idSupplier}}</td>
															<td>{{$data->name}}</td>
															<td>
																@if($data->is_disabled != 1)
																	<a href="{{ url('/child') }}/panel/{{$data->idSupplier}}" class="mb-2 mr-2 ">
																		<button name="button" type="button" class="btn button-yellow">
																			<i class="fa fa-code-fork"></i> Interface
																		</button>
																	</a>
																	
																	<a href="{{ url('/child') }}/disabled/{{$data->idSupplier}}" class="mb-2 mr-2 ">
																		<button name="button" type="button" class="btn button-yellow">
																			<i class="fa fa-close"></i> Disable
																		</button>
																	</a>
																@else
																	<a href="{{ url('/child') }}/enable/{{$data->idSupplier}}" class="mb-2 mr-2 ">
																		<button name="button" type="button" class="btn button-yellow">
																			<i class="fa fa-fire"></i> Enable
																		</button>
																	</a>
																@endif
															</td>
														</tr>	
													@endforeach
												</tbody>
											</table>		
										</div>
									@endif
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@stop
