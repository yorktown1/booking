<?php

namespace Modules\Child\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\tbluse;
use App\tblsupplier;
use App\tblsupplierchild;
use App\tblbinservice;
use Validator;
use Redirect;
use Illuminate\Support\Facades\DB;

class ChildController extends Controller
{
	/**
	* Display a listing of the resource.
	* @return Response
	*/
	public function index()
	{
		$idUser = 	session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$child = null;
		if ($supplierData->hasChild == 1){
			$child = $this->getchilddata($supplierData->idSupplier);
		}
		return view('child::index', ['supplierData' => $supplierData, 'child' => $child]);
	}
	
	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 'tblsupplier.stocking_off',
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',  'tblsupplier.abn', 'tblsupplier.hasChild',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 'tblsupplier.stocking_off',
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress', 'tblsupplier.abn','tblsupplier.hasChild',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}
	
	private function getchilddata($idSupplier){
		$child = DB::table('tblsupplierchild')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tblsupplierchild.idParent')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn', 'tblsupplierchild.is_disabled')
			->where('tblsupplierchild.idParent', $idSupplier)
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber', 
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn', 'tblsupplierchild.is_disabled')
			->get();
		return $child;
	}
	
	private function show_specific_child($idSupplier){
		$child = DB::table('tblsupplierchild')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber', 
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn', 'tblsupplierchild.is_disabled')
			->where('tblsupplierchild.idSupplier', $idSupplier)
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn', 'tblsupplierchild.is_disabled')
			->first();
		return $child;
	}
	
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
		$idUser = 	session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		
		return view('child::create', ['supplierData' => $supplierData]);
    }

	/**
	* Store a newly created resource in storage.
	* @param  Request $request
	* @return Response
	*/
	public function store(Request $request){
		$idSupplier = $request['idSupplier'];
		$idUser = $request['idUser'];
		$supplierData = $this->getSupplierData($idUser);
		
		$websiteAdminContact = $request['websiteAdminContact']; //required
		$customerServiceContact = $request['customerServicesContact']; 
		$websiteAdminPhone = $request['websiteAdminPhone']; //required
		$customerServicePhone = $request['customerServicePhone'];
		$websiteAdminMobile = $request['websiteAdminMobile']; //required
		$customerServiceMobile = $request['customerServiceMobile'];
		$customerServicesEmail1 = $request['customerServicesEmail1']; //required
		$customerServicesEmail2 = $request['customerServicesEmail2'];
		$abn = $request['abn'];
		$fullAddress = $request['fullAddress'];
		if (isset($request['openSaturday'])){
			$openSaturday = 1;
		} else {
			$openSaturday = 0;
		}

		if (isset($request['openSunday'])){
			$openSunday = 1;
		} else {
			$openSunday = 0;
		}
		if(!is_null($supplierData->stocking_off)){
			$alwayson = $supplierData->stocking_off;
		} else {
			$alwayson = '0';
		}
		
		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'websiteAdminContact' => 'required|string',
				'websiteAdminPhone' => 'required|string',
				'websiteAdminMobile' => 'required|string',
				'customerServicesEmail1' => 'required|email',
			],[
				'customerServicesEmail1.email' => 'Please input a valid email address on Email 1 Field',
				'websiteAdminContact.string' => 'Please input a proper contact name',
				'websiteAdminPhone.string' => 'Please input a proper website admin phone number',
				'websiteAdminMobile.string' => 'Please input a proper website admin monile number',
			]);

			if($validate->fails()){
				return Redirect::route('child_supplier_form')->withErrors($validate)->withInput();
			}else {
				$supplier = new tblsupplierchild();
				$count_child = count($supplier::where(['idParent' => $idSupplier])->select('idSupplier')->get())+1;
				$supplier->idSupplier = 'c'.$idSupplier.'_'.$count_child;
				$supplier->idParent = $idSupplier;
				$supplier->name = $websiteAdminContact;
				$supplier->mobilePhone = $websiteAdminMobile;
				$supplier->phoneNumber = $websiteAdminPhone;
				$supplier->email = $customerServicesEmail1;
				$supplier->email2 = $customerServicesEmail2;
				$supplier->fullAddress = $fullAddress;
				$supplier->customerServiceContact = $customerServiceContact;
				$supplier->customerServicePhone = $customerServicePhone;
				$supplier->customerServiceMobile = $customerServiceMobile;
				$supplier ->isOpenSaturday = $openSaturday;
				$supplier->isOpenSunday = $openSunday;
				$supplier->abn = $abn;
				$supplier->is_disabled = 0;
 				$supplier->stocking_off = $alwayson;
				
				if ($supplier->save()){
					$parentSupplier = new tblsupplier();
					$parentSupplier::where(['idSupplier' => $idSupplier])
					->update(['hasChild' => 1]);
					
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating supplier data');
					return redirect()->route('child_supplier_management');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update supplier data failed');
					return redirect()->route('child_supplier_management');
				}
			}
		}
	}

    public function dashboard(Request $request){
		$idChild = $request['idSupplier'];
		$child = $this->show_specific_child($idChild);
		if($child->is_disabled == 1){
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'This account is disabled');
			return redirect()->route('child_supplier_management');
		}
		return view('child::dashboard', ['star' => $child]);
    }

	public function away(){
		return redirect('/instructions');
	}
    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('child::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
	 * Update only their disability status
     */
    public function update(Request $request){
		$idSupplier = $request['idSupplier'];
		
		if(!is_null($idSupplier)){
			$supplier = new tblsupplierchild();
			$supplier::where(['idSupplier' => $idSupplier])
			->update(['is_disabled' => 1]);
			
			$binhire = new tblbinservice();
			$binhire::where(['idSupplier' => $idSupplier])
			->update(['unqueriable' => 1]);
			
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'Account disabled');
			return redirect()->route('child_supplier_management');
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'Error occured');
			return redirect()->route('child_supplier_management');
		}
		
	}
	
	 public function reenable(Request $request){
		$idSupplier = $request['idSupplier'];
		
		if(!is_null($idSupplier)){
			$supplier = new tblsupplierchild();
			$supplier::where(['idSupplier' => $idSupplier])
			->update(['is_disabled' => 0]);
			
			$binhire = new tblbinservice();
			$binhire::where(['idSupplier' => $idSupplier])
			->update(['unqueriable' => 0]);
			
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'Account enabled');
			return redirect()->route('child_supplier_management');
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'Error occured');
			return redirect()->route('child_supplier_management');
		}
		
	}
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
