<?php

namespace Modules\Child\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\tblservicearea;
use App\tblsupplier;
use App\tblsupplierchild;
use App\tbluse;
use App\tbluserservicearea;
use App\tblbookingservice;
use App\CsvData;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;

class ServiceAreaController extends Controller
{
	/**
	* Display a listing of the resource.
	* @return Response
	*/
	public function index(Request $request){
		
		$idUser = 	session('idUser');
		$childArea = null;
		$childServiceArea = null;
		$result = null; 

		$supplierData = $this->getSupplierData($idUser);
		$star = $this->show_specific_child($request['idSupplier']);
		
		$serviceAreaParent = $this->service_area_parent();
		
		if(!is_null($serviceAreaParent)){
			foreach ($serviceAreaParent as $row){
				$childArea[$row->idArea][$row->zipcode] = DB::table('tblservicearea')
					->select('idArea', 'zipcode','area', 'parentareacode')
					->where(['parentareacode' => $row->zipcode])
					->get();
			}
			
			if(!is_null($childArea)){
				foreach($childArea as $ca){
					foreach ($ca as $sca){
						foreach ($sca as $ssca){
							$childServiceArea[$ssca->parentareacode][$ssca->idArea]= DB::table('tbluserservicearea')
							->select('idUser', 'idSupplier','idServiceArea', 'serviceAreaParent')
							->where(['idSupplier' => $star->idSupplier])
							->get();
						}
					}
				}
			}
			return view('child::detail_service_area', ['supplierData' => $supplierData, 'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result, 'star' => $star]);
		} else {
			return view('child::detail_service_area', ['supplierData' => $supplierData, 'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result, 'star' => $star]);
		}
	}
	
	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',  'tblsupplier.abn', 'tblsupplier.hasChild','tblsupplier.seasonal_on','tblsupplier.seasonal_title',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress', 'tblsupplier.abn','tblsupplier.hasChild','tblsupplier.seasonal_on','tblsupplier.seasonal_title',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}
	
	private function getchilddata($idSupplier){
		$child = DB::table('tblsupplierchild')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tblsupplierchild.idParent')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn','tblsupplierchild.seasonal_on', 'tblsupplierchild.seasonal_title')
			->where('tblsupplierchild.idParent', $idSupplier)
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber', 
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn','tblsupplierchild.seasonal_on', 'tblsupplierchild.seasonal_title')
			->get();
		return $child;
	}
	
	private function show_specific_child($idSupplier){
		$child = DB::table('tblsupplierchild')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber', 
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn','tblsupplierchild.seasonal_on', 'tblsupplierchild.seasonal_title')
			->where('tblsupplierchild.idSupplier', $idSupplier)
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn','tblsupplierchild.seasonal_on', 'tblsupplierchild.seasonal_title')
			->first();
		return $child;
	}
	
	private function service_area_parent(){
		$serviceAreaParent = DB::table('tblservicearea')
			->select('idArea', 'zipcode','area', 'parentareacode')
			->where(['parentareacode' => '0000'])
			->orderBy('area','asc')
			->get();
		return $serviceAreaParent;
	}
	
	public function editSupplierDetails (Request $request){
		$idSupplier = $request['idSupplierChild'];
		$idUser = $request['idUser'];
		$websiteAdminContact = $request['websiteAdminContact']; //required
		$customerServiceContact = $request['customerServicesContact']; 
		$websiteAdminPhone = $request['websiteAdminPhone']; //required
		$customerServicePhone = $request['customerServicePhone'];
		$websiteAdminMobile = $request['websiteAdminMobile']; //required
		$customerServiceMobile = $request['customerServiceMobile'];
		$customerServicesEmail1 = $request['customerServicesEmail1']; //required
		$customerServicesEmail2 = $request['customerServicesEmail2'];
		$abn = $request['abn'];
		$fullAddress = $request['fullAddress'];
		if (isset($request['seasonal_on'])){
			$seasonal_on = 1;
		} else {
			$seasonal_on = 0;
		}
		$seasonal_title = $request['seasonal_title'];
		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'websiteAdminContact' => 'required|string',
				'websiteAdminPhone' => 'required|string',
				'websiteAdminMobile' => 'required|string',
				'customerServicesEmail1' => 'required|email',
			],[
				'customerServicesEmail1.email' => 'Please input a valid email address on Email 1 Field',
				'websiteAdminContact.string' => 'Please input a proper contact name',
				'websiteAdminPhone.string' => 'Please input a proper website admin phone number',
				'websiteAdminMobile.string' => 'Please input a proper website admin monile number',
			]);

			if($validate->fails()){
				return Redirect::route('child_details_service_zone',['idSupplier' => $idSupplier])->withErrors($validate)->withInput();
			}else {
				$supplier = new tblsupplierchild();
				$supplier::where(['idSupplier' => $idSupplier])
				->update([
					'name' => $websiteAdminContact, 
					'phonenumber' => $websiteAdminPhone, 
					'email' => $customerServicesEmail1, 
					'mobilePhone' => $websiteAdminMobile,
					'email2' => $customerServicesEmail2,
					'customerServiceContact' => $customerServiceContact,
					'customerServicePhone' => $customerServicePhone,
					'customerServiceMobile' => $customerServiceMobile,
					'fullAddress' => $fullAddress,
					'abn' => $abn,
					'seasonal_on' => $seasonal_on, 
					'seasonal_title' => $seasonal_title, 
				]);
				if ($supplier){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating supplier data');
					return redirect()->route('child_details_service_zone',['idSupplier' => $idSupplier]);
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update supplier data failed');
					return redirect()->route('child_details_service_zone', ['idSupplier' => $idSupplier]);
				}
			}
		}
	}
	
	public function editServiceArea(Request $request){
		$zipcode = $request['postalcode'];
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplierChild'];
		$idArea = $request['idArea'];

		$userservicearea = new tbluserservicearea();
		$userservicearea->where([
				'idUser' => $idUser,
				'idSupplier' => $idSupplier,
				'serviceAreaParent' => $zipcode
			])->delete();

		if(!is_null($idArea)){
			foreach($idArea as $area){
				$userservicearea = new tbluserservicearea();
				$userservicearea->idUser = $idUser;
				$userservicearea->idSupplier = $idSupplier;
				$userservicearea->idServiceArea = $area;
				$userservicearea->serviceAreaParent = $zipcode;
				if($userservicearea->save()){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating supplier service area');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update supplier service area failed');
				}
			}
			return redirect()->route('child_details_service_zone', ['idSupplier' => $idSupplier]);
		} else {
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', 'Success updating supplier service area');
			return redirect()->route('child_details_service_zone', ['idSupplier' => $idSupplier]);
		}
	}
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('child::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('child::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('child::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
