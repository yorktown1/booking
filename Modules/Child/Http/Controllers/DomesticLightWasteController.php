<?php

namespace Modules\Child\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use App\tblorderservice;
use App\tblbinextraservices;
use App\tblextraservices;
use Validator;
use Redirect;
use Illuminate\Support\Facades\DB;

class DomesticLightWasteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request){
		$idUser = 	session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$star = $this->show_specific_child($request['idSupplier']);
		
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));

		$offset = time();
		$binservicebaseprice = null;
		$binservicediscprice = null;

		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $star->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $star->idSupplier);
		$baseprice = $this->getbinservicebaseprice($star->idSupplier);
			
		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($star->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($star->idSupplier);
		
		return view('child::clean_fills',['star' => $star, 'supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset,'serviceOptions' => $serviceOptions, 'nonDelivery' => $nonDelivery, 
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices]);
    }
	
	public function showResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		
		$idUser = 	session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$star = $this->show_specific_child($request['idSupplier']);
		
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $star->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $star->idSupplier);
		$baseprice = $this->getbinservicebaseprice($star->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($star->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($star->idSupplier);
		return view('child::clean_fills',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices, 'star' => $star]);
	}

	public function showForm(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$star = $this->show_specific_child($request['idSupplier']);
		
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));

		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $star->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $star->idSupplier);
		$baseprice = $this->getbinservicebaseprice($star->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($star->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($star->idSupplier);
		
		return view('child::clean_fills_form',['star' => $star, 'supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices]);
	}
	
	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',  'tblsupplier.abn', 'tblsupplier.hasChild',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress', 'tblsupplier.abn','tblsupplier.hasChild',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}
	
	private function getchilddata($idSupplier){
		$child = DB::table('tblsupplierchild')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tblsupplierchild.idParent')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn')
			->where('tblsupplierchild.idParent', $idSupplier)
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber', 
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn')
			->get();
		return $child;
	}
	
	private function show_specific_child($idSupplier){
		$child = DB::table('tblsupplierchild')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber', 
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn')
			->where('tblsupplierchild.idSupplier', $idSupplier)
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.idParent', 'tblsupplierchild.name','tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phonenumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress',  'tblsupplierchild.abn')
			->first();
		return $child;
	}
	
	private function getBinSize (){
		$sizes = DB::table('tblsize')
				->select('idSize','size', 'dimensions')
				->orderBy('idSize', 'asc')
				->get();
		return $sizes;
	}
	
	private function getServiceOptions($idUser, $idSupplier){
		$serviceOptions = DB::table('tblbinserviceoptions')
						->select('idUser', 'idSupplier', 'idBinType', 'extraHireagePrice', 'extraHireageDays', 'excessWeightPrice')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier,
							'idBinType' => 3
						])
						->first();
		return $serviceOptions;
	}
	
	private function getNonDeliveryDays($idUser, $idSupplier){
		$nonDelivery = DB::table('tblbinnondelivery')
					->select('date')
					->where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3
					])
					->get();
		return $nonDelivery;
	}
	
	private function getbinservicebaseprice($idSupplier){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinType' => 3,
							])
						->get();
		return $binserviceprice;
	}

	private function getbinservicediscprice($idBinService){
		$binserviceprice = DB::table('tblbinserviceupdates')
						->select('tblbinserviceupdates.price','tblbinserviceupdates.stock','tblbinserviceupdates.date')
						->where([
							'tblbinserviceupdates.idBinService' => $idBinService,
							])
						->get();
		return $binserviceprice;
	}

	private function getspecificbaseprice($idSupplier, $idBinSize){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinSize' => $idBinSize,
							'tblbinservice.idBinType' => 3,
							])
						->get();
		return $binserviceprice;
	}
	
	private function getextraservices(){
		$extras = DB::table('tblextraservices')
				->select('tblextraservices.idExtraServices', 'tblextraservices.name', 'tblextraservices.code', 'tblextraservices.slug')
				->get();
		return $extras;
	}
	
	private function getbinextraservices($idSupplier){
		$extras = DB::table('tblbinserviceextra')
				->select('tblbinserviceextra.idBinServiceExtra', 'tblbinserviceextra.idBinType', 'tblbinserviceextra.idExtraService', 'tblbinserviceextra.idSupplier', 'tblbinserviceextra.charge')
				->where([
					'tblbinserviceextra.idBinType' => 3,
					'tblbinserviceextra.idSupplier' => $idSupplier
				])
				->get();
		return $extras;
	}
	
	public function editMiscDetail(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplierChild'];
		$idBinType = $request['idBinType'];
		$extraHireage = $request['extraHireage'];
		$extraHireageDays = $request['extraHireageDays'];
		$excessWeight = $request['excessWeight'];
		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'extraHireage' => 'required|numeric',
				'extraHireageDays' => 'required|numeric',
				'excessWeight' => 'required|numeric',
			],[
				'extraHireage.numeric' => 'Extra hireage price should be numeric',
				'extraHireageDays.numeric' => 'Extra hireage days should be numeric',
				'excessWeight.numeric' => 'Excess weight amount should be numeric',
			]);
			if($validate->fails()){
				return Redirect::route('child_bintype_3',['idSupplier' => $idSupplier])->withErrors($validate)->withInput();
			} else {
				$check = DB::table('tblbinserviceoptions')
						->select('idUser','idSupplier', 'idBinType')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier, 
							'idBinType' => 3
						])
						->first();
				if (!is_null($check)){
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions::where(['idUser' => $idUser, 'idSupplier' => $idSupplier, 'idBinType' => $idBinType])
					->update([
						'extraHireagePrice' => $extraHireage, 
						'extraHireageDays' => $extraHireageDays, 
						'excessWeightPrice' => $excessWeight
					]);
					if ($serviceOptions){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rates optional data');
						return redirect()->route('child_bintype_3',['idSupplier' => $idSupplier]);
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rates optional data failed');
						return redirect()->route('child_bintype_3',['idSupplier' => $idSupplier]);
					}
				} else {
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions->idUser = $idUser;
					$serviceOptions->idSupplier = $idSupplier;
					$serviceOptions->idBinType = 3;
					$serviceOptions->extraHireagePrice = $extraHireage;
					$serviceOptions->extraHireageDays = $extraHireageDays;
					$serviceOptions->excessWeightPrice = $excessWeight;
					if ($serviceOptions->save()){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rates optional data');
						return redirect()->route('child_bintype_3',['idSupplier' => $idSupplier]);
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rates optional data failed');
						return redirect()->route('child_bintype_3',['idSupplier' => $idSupplier]);
					}
				}
			}
		}
	}
	
	public function editBinServicesExtra(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplierChild'];
		$idBinType = $request['idBinType'];
		$extras_code[] = $request['extras_hidden'];
		$extras[] = $request['extras'];
		
		$extras_pivot = array('idExtraServices' => $extras_code, 'charge' => $extras);
		
		/** check for its existence, if exists override, if its not just add **/
		/** start the form function **/
		if ($request->isMethod('post') == 'POST'){
			
			/** validate extra field **/ 
			$validate = Validator::make($request->all(), [
				"extras_hidden"    => "required|array|min:1",
				'extras_hidden.*' => 'required|numeric',
				"extras"    => "required|array|min:1",
				'extras.*' => 'required|numeric',
			]);
			
			if($validate->fails()){
				return Redirect::route('child_bintype_3',['idSupplier' => $idSupplier])->withErrors($validate)->withInput();
			} else {
				/** start the loop over the extra field **/ 
				foreach($extras_pivot['idExtraServices'] as $k=>$v){
					foreach($v  as $r => $d){
						$check = DB::table('tblbinserviceextra')
						->select('idSupplier', 'idBinType', 'idExtraService', 'charge')
						->where([
							'idSupplier' => $idSupplier,
							'idBinType' => 3,
							'idExtraService' => $extras_pivot['idExtraServices'][$k][$r]
						])
						->first();
						
						if (!is_null($check)){
							$tblbinextraservices = new tblbinextraservices();
							$tblbinextraservices::where([
								'idSupplier' => $idSupplier,
								'idBinType' => 3,
								'idExtraService' => $extras_pivot['idExtraServices'][$k][$r]
								])
								->update(['charge' => $extras_pivot['charge'][$k][$r]]);
							$request->session()->flash('status', 'success');
							$request->session()->flash('message', 'Success extra charge optional data');
						} else {
							$tblbinextraservices = new tblbinextraservices();
							
							$tblbinextraservices->idBinType = 3;
							$tblbinextraservices->idExtraService =  $extras_pivot['idExtraServices'][$k][$r];
							$tblbinextraservices->idSupplier = $idSupplier;
							$tblbinextraservices->charge = $extras_pivot['charge'][$k][$r];
							$tblbinextraservices->save();
							$request->session()->flash('status', 'success');
							$request->session()->flash('message', 'Success extra charge optional data');
						}
					}
				}
				
				return redirect()->route('child_bintype_3',['idSupplier' => $idSupplier]);
			}
		}
	}
	
	public function editNonDeliveryDays(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplierChild'];
		$idBinType = $request['idBinType'];
		$date[] = $request['date'];
		$nonDelivery[] = $request['nondelivery'];

		$nondeliverydays = array('date' => $date, 'nondelivery' => $nonDelivery);
		/** Delete first **/
		$tblbinnondelivery = new tblbinnondelivery();
		//check for deletion first
		if (is_null($nondeliverydays['nondelivery'][0])){
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}

					if($deletedRows){
						$result['status'] = 'success';
						$result['message'] = 'Success updating non delivery data';
					} else {
						$result['status'] = 'success';
						$result['message'] = 'Date updated';
					}
				}
			}
		} else{
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}
				}
			}
			
			/** Then insert **/
			foreach($nondeliverydays['nondelivery'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 3,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 3;
						if($tblbinnondelivery->save()){
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
					}
				}
			}
			
		}

		return redirect()->route('child.bintype3', ['idSupplier' => $idSupplier,'status' => $result['status'], 'message' => $result['message']]);
	}
	
	private function validatePricing($basePrice, $baseStock){
		$error = array();
		if ($basePrice == ''){
			$error[] = 'Please fill a specific base price for your bin';
		} 
		if ($baseStock == ''){
			$error[] = 'Please fill a specific stock for your bin';
		} 
		return $error;
	}
	
	public function editBinServicePrice(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplierChild'];
		$idBinType = $request['idBinType'];
		$idBinSize = $request['idBinSize'];
		$basePrice = $request['basePrice'];
		$date[] = $request['date'];
		$binPrice[] = $request['binPrice'];
		$baseStock = $request['baseStock'];
		$stock[] = $request['stock'];

		/* Main pricing data */
		$mainData = array(
			'date' => $date,
			'binPrice' => $binPrice,
			'stock' => $stock
		);
		

		if ($request->isMethod('POST') == 'POST'){
			$error = $this->validatePricing($binPrice, $stock);
			if(count($error) > 0){
				$result['status'] = 'danger';
				$result['message'] = $error;
				return redirect()->route('child.bintype3.form', ['idSupplier' => $idSupplier, 'status' => $result['status'], 'message' => $result['message']]);
			} else {
				/* check base price first */
				
				$checkBasePrice = DB::table('tblbinservice')
							->select('idBinService', 'price', 'stock')
							->where([
								'idSupplier' => $idSupplier,
								'idBinSize' => $idBinSize,
								'idBinType' => $idBinType
							])
							->first();

				if (!is_null($checkBasePrice)){
					$validate = Validator::make($request->all(), [
						'basePrice' => 'required|numeric',
						'baseStock' => 'required|numeric',
					],[
						'basePrice.numeric' => 'The bin base price should be on numeric value',
						'baseStock.numeric' => 'The bin base stock should be on numeric value',
					]);
					if($validate->fails()){
						return Redirect::route('child.bintype3.pricing.form', ['idSupplier' => $idSupplier, 'bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
					}
					
					$binservice = new tblbinservice();

					$updates = $binservice::where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType,
					])
					->update(['price' => $basePrice, 'stock' => $baseStock]);

					$checkupdatesSuccessful = DB::table('tblbinservice')
											->select('idBinService', 'price', 'stock')
											->where([
												'idSupplier' => $idSupplier,
												'idBinSize' => $idBinSize,
												'idBinType' => $idBinType,
												'price' => $basePrice,
												'stock' => $baseStock
											])
											->first();
					if(!is_null($checkupdatesSuccessful)){
						$tempResultBasePrice = 'success';
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating bin base price data');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update bin base price failed');
						return redirect()->route('child_bintype_3', ['idSupplier' => $idSupplier]);
					}

					/*fetch last id */
					$lastid = DB::table('tblbinservice')
					->select('idBinService', 'price', 'stock')
					->where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType
					])
					->first();

				} else {
					$validate = Validator::make($request->all(), [
						'basePrice' => 'required|numeric',
						'baseStock' => 'required|numeric',
					],[
						'basePrice.numeric' => 'The bin base price should be on numeric value',
						'baseStock.numeric' => 'The bin base stock should be on numeric value',
					]);
					if($validate->fails()){
						return Redirect::route('child.bintype3.pricing.form', ['idSupplier' => $idSupplier,'bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
					}
					
					$binservice = new tblbinservice();
					$binservice->idSupplier = $idSupplier;
					$binservice->idBinType = $idBinType;
					$binservice->idBinSize = $idBinSize;
					$binservice->price = $basePrice;
					$binservice->stock = $baseStock;
					$binservice->unqueriable = 0;

					if ($binservice->save()){
						$tempResultBasePrice = 'success';
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating bin base price data');
						/*fetch last id */
						$lastid = DB::table('tblbinservice')
							->select('idBinService', 'price', 'stock')
							->orderBy('idBinService', 'desc')
							->first();
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update bin base price failed');
						return redirect()->route('child_bintype_3', ['idSupplier' => $idSupplier]);
					}
				}
				
				/* validate numeric discount value */
				foreach ($mainData['binPrice'] as $k => $v){
					foreach($v  as $d){
						$array_binprice[] = $d;
					}
					
				}
				foreach ($mainData['stock'] as $k => $v){
					foreach($v  as $d){
						$array_stock[] = $d;
					}
				}
					/* price */
				$validate = Validator::make(compact('array_binprice'), [
					'array_binprice' => 'array',
					'array_binprice.*' => 'numeric',
				],[
					'array_binprice.*.numeric' => 'The bin price * should has numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('child.bintype3.pricing.form', ['idSupplier' => $idSupplier, 'bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
					/* stock */
				$validate = Validator::make(compact('array_stock'), [
					'array_stock' => 'array',
					'array_stock.*' => 'numeric',
				],[
					'array_stock.*.numeric' => 'The bin stock * should has numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('child.bintype3.pricing.form', ['idSupplier' => $idSupplier,'bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				/* check and update the discount on specific date*/
				if (!is_null($mainData['binPrice'])){
					foreach($mainData['binPrice'] as $k => $v){
						foreach($v as $r => $d){
							if(!is_null($mainData['binPrice'][$k][$r])){
								$checkDisc = DB::table('tblbinserviceupdates')
									->select('idBinService', 'price', 'stock')
									->where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
								->first();

								if (!is_null($checkDisc)){
									$binserviceupdates = new tblbinserviceupdates();
									$update = $binserviceupdates::where([
										'idBinService' => $lastid->idBinService,
										'date' => $mainData['date'][$k][$r],
									])
									->update(['price' => $mainData['binPrice'][$k][$r], 'stock' => $mainData['stock'][$k][$r]]);

									if ($update) {
										$request->session()->flash('status', 'success');
										$request->session()->flash('message', 'Success updating rate data');
									
									} else {
										$request->session()->flash('status', 'danger');
										$request->session()->flash('message', 'Update rate data failed');
									}
								} else {
									if(!is_null($checkBasePrice)){
										if(($checkBasePrice->price != $mainData['binPrice'][$k][$r]) || ($checkBasePrice->stock != $mainData['stock'][$k][$r])){
											$binserviceupdates = new tblbinserviceupdates();
											$binserviceupdates->idBinService = $lastid->idBinService;
											$binserviceupdates->price = $mainData['binPrice'][$k][$r];
											$binserviceupdates->stock = $mainData['stock'][$k][$r];
											$binserviceupdates->date = $mainData['date'][$k][$r];
											if ($binserviceupdates->save()){
												$request->session()->flash('status', 'success');
												$request->session()->flash('message', 'Success updating rate data');
											} else {
												$request->session()->flash('status', 'danger');
												$request->session()->flash('message', 'Update rate data failed');
											}
										} else {
											$request->session()->flash('status', 'success');
											$request->session()->flash('message', 'Success updating rate data');
										}
									}
								}
							}
						}
					}
				}
				

				
				if ($tempResultBasePrice == 'success'){
					$result['status'] = 'success';
					$result['message'] = 'Success updating rates data';
				} else {
					$result['status'] = 'notif';
					$result['message'] = 'Update rates data failed';
					
				}
				return redirect()->route('child_bintype_3', ['idSupplier' => $idSupplier]);
			}
		}
	}

	public function resetRates(Request $request){
		$idUser = session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$star = $this->show_specific_child($request['idSupplier']);
		
		$idBinType = $request['bintype'];
		$idBinSize = $request['binsize'];

		$binservice = new tblbinservice();
		$binserviceupdates = new tblbinserviceupdates();
		$orderservice = new tblorderservice();
		
		$getbinservice = DB::table('tblbinservice')
						->select('idBinService')
						->where([
							'idSupplier' => $star->idSupplier,
							'idBinSize' => $idBinSize,
							'idBinType' => $idBinType
						])
						->first();
		if (!is_null($getbinservice)){
			$deletedRowsBinUpdates = $binserviceupdates::where([
			'idBinService' => $getbinservice->idBinService
			])->delete();
			
			//$deletedRowsBinOrder = $orderservice::where([
			//'idBinService' => $getbinservice->idBinService
			//])->delete();
			
			$deletedRowsBin = $binservice::where([
				'idSupplier' => $star->idSupplier,
				'idBinSize' => $idBinSize,
				'idBinType' => $idBinType
			])->update(['price' => 0, 'stock' => 0]);

			if ($deletedRowsBin){
				$request->session()->flash('status', 'success');
				$request->session()->flash('message', 'Success deleting rates data');
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Delete rates data failed');
			}
		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'No data selected');
		}
		return redirect()->route('child_bintype_3',['idSupplier' => $star->idSupplier]);
	}

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('child::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('child::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('child::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
