<?php

Route::group(['middleware' => 'web', 'prefix' => 'child', 'namespace' => 'Modules\Child\Http\Controllers'], function()
{
	
	Route::get('/',[
		'as' => 'child_supplier_management',
		'uses' => 'ChildController@index'
	])->middleware('checkRole:1,2');
	
	Route::get('/create_child_account',[
		'as' => 'child_supplier_form',
		'uses' => 'ChildController@create'
	])->middleware('checkRole:1,2');
	
	Route::get('/back_to_parent',[
		'as' => 'child_back',
		'uses' => 'ChildController@away'
	])->middleware('checkRole:1,2');
	
	Route::post('/add_child_account','ChildController@store')->middleware('checkRole:1,2');
	Route::get('/disabled/{idSupplier}','ChildController@update')->middleware('checkRole:1,2');
	Route::get('/enable/{idSupplier}','ChildController@reenable')->middleware('checkRole:1,2');
	/****** INSTRUCTION PAGE ***/
	Route::get('/panel/{idSupplier}', [
		'as' => 'child_dashboard',
		'uses' => 'ChildController@dashboard'
	])->middleware('checkRole:1,2');
	
	Route::get('/instructions/{idSupplier}', [
		'as' => 'child_instructions',
		'uses' => 'ChildController@dashboard'
	])->middleware('checkRole:1,2');
	
	/****** DETAILS & SERVICE AREA ***/
	Route::get('/details_service_zone/{idSupplier}', [
		'as' => 'child_details_service_zone',
		'uses' => 'ServiceAreaController@index'
	])->middleware('checkRole:1,2');
	
	Route::get('/details_service_zone/{idSupplier}/{status}/{message}',[
		'as' => 'child.details.service.zone',
		'uses' => 'ServiceAreaController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::post('/editSupplierDetails', 'ServiceAreaController@editSupplierDetails')->middleware('checkRole:1,2');
	Route::post('/editArea/{postalcode}', 'ServiceAreaController@editServiceArea')->middleware('checkRole:1,2');
	
	/****** BIN TYPE 1  ***/
	Route::get('/mixed_renovation/{idSupplier}', [
		'as' => 'child_bintype_1',
		'uses' => 'GeneralWasteController@index'
	])->middleware('checkRole:1,2');
	
	
	Route::get('/mixed_renovation/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype1',
		'uses' => 'GeneralWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/1/calendarOffset/{offset}',[
		'uses' => 'GeneralWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/1/{binsize}/calendarOffset/{offset}',[
		'uses' => 'GeneralWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::post('/editMiscDetails/1', 'GeneralWasteController@editMiscDetail')->middleware('checkRole:1,2');
	Route::post('/binServicesExtra/1', 'GeneralWasteController@editBinServicesExtra')->middleware('checkRole:1,2');
	Route::post('1/editNonDeliveryDays', 'GeneralWasteController@editNonDeliveryDays')->middleware('checkRole:1,2');
	
	Route::get('1/showEditPricingForm/{idSupplier}/{bintype}/{binsize}',[
		'as' => 'child.bintype1.pricing.form',
		'uses' => 'GeneralWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::get('1/showForm/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype1.form',
		'uses' => 'GeneralWasteController@showFormResult'
	])->middleware('checkRole:1,2');
	
	Route::post('1/editBinServicePrice', 'GeneralWasteController@editBinServicePrice')->middleware('checkRole:1,2');
	Route::delete('/1/resetPricing/{bintype}/{binsize}', 'GeneralWasteController@resetRates')->middleware('checkRole:1,2');
	
	/****** BIN TYPE 2  ***/
	Route::get('/heavy_landscaping_waste/{idSupplier}', [
		'as' => 'child_bintype_2',
		'uses' => 'MixedHeavyWasteController@index'
	])->middleware('checkRole:1,2');
	
	
	Route::get('/heavy_landscaping_waste/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype2',
		'uses' => 'MixedHeavyWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/2/calendarOffset/{offset}',[
		'uses' => 'MixedHeavyWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/2/{binsize}/calendarOffset/{offset}',[
		'uses' => 'MixedHeavyWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::post('/editMiscDetails/2', 'MixedHeavyWasteController@editMiscDetail')->middleware('checkRole:1,2');
	Route::post('/binServicesExtra/2', 'MixedHeavyWasteController@editBinServicesExtra')->middleware('checkRole:1,2');
	Route::post('2/editNonDeliveryDays', 'MixedHeavyWasteController@editNonDeliveryDays')->middleware('checkRole:1,2');
	
	Route::get('2/showEditPricingForm/{idSupplier}/{bintype}/{binsize}',[
		'as' => 'child.bintype2.pricing.form',
		'uses' => 'MixedHeavyWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::get('2/showForm/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype2.form',
		'uses' => 'MixedHeavyWasteController@showFormResult'
	])->middleware('checkRole:1,2');
	
	Route::post('2/editBinServicePrice', 'MixedHeavyWasteController@editBinServicePrice')->middleware('checkRole:1,2');
	Route::delete('/2/resetPricing/{bintype}/{binsize}', 'MixedHeavyWasteController@resetRates')->middleware('checkRole:1,2');
	
		/****** BIN TYPE 3  ***/
	Route::get('/domestic_light/{idSupplier}', [
		'as' => 'child_bintype_3',
		'uses' => 'DomesticLightWasteController@index'
	])->middleware('checkRole:1,2');
	
	
	Route::get('/domestic_light/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype3',
		'uses' => 'DomesticLightWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/3/calendarOffset/{offset}',[
		'uses' => 'DomesticLightWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/3/{binsize}/calendarOffset/{offset}',[
		'uses' => 'DomesticLightWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::post('/editMiscDetails/3', 'DomesticLightWasteController@editMiscDetail')->middleware('checkRole:1,2');
	Route::post('/binServicesExtra/3', 'DomesticLightWasteController@editBinServicesExtra')->middleware('checkRole:1,2');
	Route::post('3/editNonDeliveryDays', 'DomesticLightWasteController@editNonDeliveryDays')->middleware('checkRole:1,2');
	
	Route::get('3/showEditPricingForm/{idSupplier}/{bintype}/{binsize}',[
		'as' => 'child.bintype3.pricing.form',
		'uses' => 'DomesticLightWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::get('3/showForm/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype3.form',
		'uses' => 'DomesticLightWasteController@showFormResult'
	])->middleware('checkRole:1,2');
	
	Route::post('3/editBinServicePrice', 'DomesticLightWasteController@editBinServicePrice')->middleware('checkRole:1,2');
	Route::delete('/3/resetPricing/{bintype}/{binsize}', 'DomesticLightWasteController@resetRates')->middleware('checkRole:1,2');
	
	/****** BIN TYPE 4  ***/
	Route::get('/green_waste/{idSupplier}', [
		'as' => 'child_bintype_4',
		'uses' => 'GreenWasteController@index'
	])->middleware('checkRole:1,2');
	
	
	Route::get('/green_waste/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype4',
		'uses' => 'GreenWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/4/calendarOffset/{offset}',[
		'uses' => 'GreenWasteController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/4/{binsize}/calendarOffset/{offset}',[
		'uses' => 'GreenWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::post('/editMiscDetails/4', 'GreenWasteController@editMiscDetail')->middleware('checkRole:1,2');
	Route::post('/binServicesExtra/4', 'GreenWasteController@editBinServicesExtra')->middleware('checkRole:1,2');
	Route::post('4/editNonDeliveryDays', 'GreenWasteController@editNonDeliveryDays')->middleware('checkRole:1,2');
	
	Route::get('4/showEditPricingForm/{idSupplier}/{bintype}/{binsize}',[
		'as' => 'child.bintype4.pricing.form',
		'uses' => 'GreenWasteController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::get('4/showForm/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype4.form',
		'uses' => 'GreenWasteController@showFormResult'
	])->middleware('checkRole:1,2');
	
	Route::post('4/editBinServicePrice', 'GreenWasteController@editBinServicePrice')->middleware('checkRole:1,2');
	Route::delete('/4/resetPricing/{bintype}/{binsize}', 'GreenWasteController@resetRates')->middleware('checkRole:1,2');
	
	/****** BIN TYPE 5 ***/
	Route::get('/other_bin/{idSupplier}', [
		'as' => 'child_bintype_5',
		'uses' => 'OtherBinController@index'
	])->middleware('checkRole:1,2');
	
	
	Route::get('/other_bin/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype5',
		'uses' => 'OtherBinController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/5/calendarOffset/{offset}',[
		'uses' => 'OtherBinController@showResult'
	])->middleware('checkRole:1,2');
	
	Route::get('{idSupplier}/5/{binsize}/calendarOffset/{offset}',[
		'uses' => 'OtherBinController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::post('/editMiscDetails/5', 'OtherBinController@editMiscDetail')->middleware('checkRole:1,2');
	Route::post('/binServicesExtra/5', 'OtherBinController@editBinServicesExtra')->middleware('checkRole:1,2');
	Route::post('5/editNonDeliveryDays', 'OtherBinController@editNonDeliveryDays')->middleware('checkRole:1,2');
	
	Route::get('5/showEditPricingForm/{idSupplier}/{bintype}/{binsize}',[
		'as' => 'child.bintype5.pricing.form',
		'uses' => 'OtherBinController@showForm'
	])->middleware('checkRole:1,2');
	
	Route::get('5/showForm/{idSupplier}/{status}/{message}',[
		'as' => 'child.bintype5.form',
		'uses' => 'OtherBinController@showFormResult'
	])->middleware('checkRole:1,2');
	
	Route::post('5/editBinServicePrice', 'OtherBinController@editBinServicePrice')->middleware('checkRole:1,2');
	Route::delete('/5/resetPricing/{bintype}/{binsize}', 'OtherBinController@resetRates')->middleware('checkRole:1,2');
	
	/** SEASONAL PRICING **/
	Route::get('seasonalpricing/{idSupplier}','SeasonalpricingController@index')->name('child_seasonalprice')->middleware('checkRole:1,2');
	Route::post('seasonal_filter/','SeasonalpricingController@filter')->name('child_filter_seasonalprice')->middleware('checkRole:1,2');
	Route::get('child/seasonal_filter/result/{idSupplier}/{idBinType}','SeasonalpricingController@filtered')->name('child_filter_result_seasonalprice')->middleware('checkRole:1,2');
	Route::get('editspecialdays/{idSize}/{idBinType}/{idSupplier}','SeasonalpricingController@edit')->name('child_editseasonalprice')->middleware('checkRole:1,2');
	Route::post('updateseasonalprice/','SeasonalpricingController@update')->name('child_update_seasonalprice')->middleware('checkRole:1,2');
});
