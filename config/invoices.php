<?php

return [
	'company'     			=> 'Coastal Waste',
	'full_address'			=> '159 Moylan Rd, Wattleup, WA',
	'zipcode'                => '6165',
	'phone'					=> '1300 110 132',
	'email'              	 => 'info@coastalwaste.com.au',
	'abn'             		 => '84 607 972 896',
];
