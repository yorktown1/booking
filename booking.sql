-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2019 at 09:41 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `csv_data`
--

CREATE TABLE `csv_data` (
  `id` int(11) NOT NULL,
  `csv_filename` varchar(255) DEFAULT NULL,
  `csv_header` tinyint(1) DEFAULT NULL,
  `csv_data` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_06_11_052427_create_bin_service', 1),
(3, '2018_06_11_053118_create_bin_service_updates', 1),
(4, '2018_06_11_053255_create_bin_type', 1),
(5, '2018_06_11_054938_create_supplier_table', 1),
(6, '2018_06_11_055545_create_service_area', 1),
(7, '2018_06_11_055941_create_size_table', 1),
(8, '2018_06_11_060117_create_order_service_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinnondelivery`
--

CREATE TABLE `tblbinnondelivery` (
  `idNonDeliveryDays` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `idSupplier` varchar(10) DEFAULT NULL,
  `idUser` int(10) DEFAULT NULL,
  `idBinType` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservice`
--

CREATE TABLE `tblbinservice` (
  `idBinService` int(10) UNSIGNED NOT NULL,
  `idSupplier` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idBinType` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `idBinSize` int(11) DEFAULT NULL,
  `unqueriable` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `own` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinservice`
--

INSERT INTO `tblbinservice` (`idBinService`, `idSupplier`, `idBinType`, `price`, `stock`, `idBinSize`, `unqueriable`, `own`) VALUES
(13, '11', 1, 39.00, 1, 1, '0', '1'),
(14, '11', 1, 39.00, 1, 1, '0', '2'),
(15, '11', 1, 39.00, 1, 1, '0', '3'),
(16, '11', 1, 39.00, 1, 2, '0', '1'),
(17, '11', 1, 39.00, 1, 2, '0', '2'),
(18, '11', 1, 39.00, 1, 2, '0', '3'),
(19, '11', 1, 39.00, 1, 3, '0', '1'),
(20, '11', 1, 39.00, 1, 3, '0', '2'),
(21, '11', 1, 39.00, 1, 3, '0', '3'),
(22, '11', 1, 39.00, 1, 4, '0', '1'),
(23, '11', 1, 39.00, 1, 4, '0', '2'),
(24, '11', 1, 39.00, 1, 4, '0', '3'),
(25, '11', 2, 28.00, 1, 13, '0', '1'),
(26, '11', 2, 28.00, 1, 13, '0', '2'),
(27, '11', 2, 28.00, 1, 13, '0', '3'),
(28, '11', 1, 29.00, 1, 5, '0', '1'),
(29, '11', 1, 29.00, 1, 5, '0', '2'),
(30, '11', 1, 29.00, 1, 5, '0', '3'),
(31, '11', 1, 29.00, 1, 6, '0', '1'),
(32, '11', 1, 29.00, 1, 6, '0', '2'),
(33, '11', 1, 29.00, 1, 6, '0', '3'),
(34, '11', 1, 39.00, 1, 7, '0', '1'),
(35, '11', 1, 39.00, 1, 7, '0', '2'),
(36, '11', 1, 39.00, 1, 7, '0', '3'),
(37, '11', 1, 39.00, 1, 10, '0', '1'),
(38, '11', 1, 39.00, 1, 10, '0', '2'),
(39, '11', 1, 39.00, 1, 10, '0', '3'),
(40, '11', 1, 39.00, 1, 11, '0', '1'),
(41, '11', 1, 39.00, 1, 11, '0', '2'),
(42, '11', 1, 39.00, 1, 11, '0', '3'),
(43, '11', 1, 39.00, 1, 12, '0', '1'),
(44, '11', 1, 39.00, 1, 12, '0', '2'),
(45, '11', 1, 39.00, 1, 12, '0', '3'),
(46, '11', 1, 39.00, 1, 13, '0', '1'),
(47, '11', 1, 39.00, 1, 13, '0', '2'),
(48, '11', 1, 39.00, 1, 13, '0', '3'),
(49, '11', 1, 39.00, 1, 14, '0', '1'),
(50, '11', 1, 39.00, 1, 14, '0', '2'),
(51, '11', 1, 39.00, 1, 14, '0', '3'),
(52, '11', 1, 39.00, 1, 15, '0', '1'),
(53, '11', 1, 39.00, 1, 15, '0', '2'),
(54, '11', 1, 39.00, 1, 15, '0', '3'),
(55, '11', 1, 39.00, 1, 16, '0', '1'),
(56, '11', 1, 39.00, 1, 16, '0', '2'),
(57, '11', 1, 39.00, 1, 16, '0', '3'),
(58, '11', 1, 39.00, 1, 17, '0', '1'),
(59, '11', 1, 39.00, 1, 17, '0', '2'),
(60, '11', 1, 39.00, 1, 17, '0', '3'),
(61, '11', 1, 39.00, 1, 18, '0', '1'),
(62, '11', 1, 39.00, 1, 18, '0', '2'),
(63, '11', 1, 39.00, 1, 18, '0', '3'),
(64, '11', 1, 39.00, 1, 19, '0', '1'),
(65, '11', 1, 39.00, 1, 19, '0', '2'),
(66, '11', 1, 39.00, 1, 19, '0', '3'),
(67, '11', 1, 39.00, 1, 20, '0', '1'),
(68, '11', 1, 39.00, 1, 20, '0', '2'),
(69, '11', 1, 39.00, 1, 20, '0', '3'),
(70, '11', 1, 39.00, 1, 21, '0', '1'),
(71, '11', 1, 39.00, 1, 21, '0', '2'),
(72, '11', 1, 39.00, 1, 21, '0', '3'),
(73, '11', 1, 39.00, 1, 22, '0', '1'),
(74, '11', 1, 39.00, 1, 22, '0', '2'),
(75, '11', 1, 39.00, 1, 22, '0', '3'),
(76, '11', 1, 19.00, 1, 23, '0', '1'),
(77, '11', 1, 19.00, 1, 23, '0', '2'),
(78, '11', 1, 19.00, 1, 23, '0', '3'),
(79, '11', 1, 19.00, 1, 24, '0', '1'),
(80, '11', 1, 19.00, 1, 24, '0', '2'),
(81, '11', 1, 19.00, 1, 24, '0', '3'),
(82, '11', 1, 19.00, 1, 25, '0', '1'),
(83, '11', 1, 19.00, 1, 25, '0', '2'),
(84, '11', 1, 19.00, 1, 25, '0', '3'),
(85, '11', 1, 19.00, 1, 26, '0', '1'),
(86, '11', 1, 19.00, 1, 26, '0', '2'),
(87, '11', 1, 19.00, 1, 26, '0', '3'),
(88, '11', 1, 49.00, 1, 27, '0', '1'),
(89, '11', 1, 49.00, 1, 27, '0', '2'),
(90, '11', 1, 49.00, 1, 27, '0', '3'),
(91, '11', 1, 49.00, 1, 28, '0', '1'),
(92, '11', 1, 49.00, 1, 28, '0', '2'),
(93, '11', 1, 49.00, 1, 28, '0', '3'),
(94, '11', 1, 49.00, 1, 29, '0', '1'),
(95, '11', 1, 49.00, 1, 29, '0', '2'),
(96, '11', 1, 49.00, 1, 29, '0', '3'),
(97, '11', 1, 49.00, 1, 30, '0', '1'),
(98, '11', 1, 49.00, 1, 30, '0', '2'),
(99, '11', 1, 49.00, 1, 30, '0', '3'),
(100, '11', 1, 39.00, 1, 31, '0', '1'),
(101, '11', 1, 39.00, 1, 31, '0', '2'),
(102, '11', 1, 39.00, 1, 31, '0', '3'),
(103, '11', 1, 39.00, 1, 32, '0', '1'),
(104, '11', 1, 39.00, 1, 32, '0', '2'),
(105, '11', 1, 39.00, 1, 32, '0', '3'),
(106, '11', 1, 39.00, 1, 33, '0', '1'),
(107, '11', 1, 39.00, 1, 33, '0', '2'),
(108, '11', 1, 39.00, 1, 33, '0', '3'),
(109, '11', 1, 39.00, 1, 34, '0', '1'),
(110, '11', 1, 39.00, 1, 34, '0', '2'),
(111, '11', 1, 39.00, 1, 34, '0', '3'),
(112, '11', 1, 39.00, 1, 38, '0', '1'),
(113, '11', 1, 39.00, 1, 38, '0', '2'),
(114, '11', 1, 39.00, 1, 38, '0', '3'),
(115, '11', 1, 39.00, 1, 39, '0', '1'),
(116, '11', 1, 39.00, 1, 39, '0', '2'),
(117, '11', 1, 39.00, 1, 39, '0', '3'),
(118, '11', 1, 39.00, 1, 40, '0', '1'),
(119, '11', 1, 39.00, 1, 40, '0', '2'),
(120, '11', 1, 39.00, 1, 40, '0', '3'),
(121, '11', 1, 39.00, 1, 41, '0', '1'),
(122, '11', 1, 39.00, 1, 41, '0', '2'),
(123, '11', 1, 39.00, 1, 41, '0', '3'),
(124, '11', 1, 39.00, 1, 42, '0', '1'),
(125, '11', 1, 39.00, 1, 42, '0', '2'),
(126, '11', 1, 39.00, 1, 42, '0', '3'),
(127, '11', 1, 39.00, 1, 43, '0', '1'),
(128, '11', 1, 39.00, 1, 43, '0', '2'),
(129, '11', 1, 39.00, 1, 43, '0', '3'),
(130, '11', 1, 39.00, 1, 44, '0', '1'),
(131, '11', 1, 39.00, 1, 44, '0', '2'),
(132, '11', 1, 39.00, 1, 44, '0', '3'),
(133, '11', 1, 39.00, 1, 45, '0', '1'),
(134, '11', 1, 39.00, 1, 45, '0', '2'),
(135, '11', 1, 39.00, 1, 45, '0', '3'),
(136, '11', 1, 39.00, 1, 46, '0', '1'),
(137, '11', 1, 39.00, 1, 46, '0', '2'),
(138, '11', 1, 39.00, 1, 46, '0', '3'),
(139, '11', 1, 39.00, 1, 47, '0', '1'),
(140, '11', 1, 39.00, 1, 47, '0', '2'),
(141, '11', 1, 39.00, 1, 47, '0', '3'),
(142, '11', 1, 39.00, 1, 48, '0', '1'),
(143, '11', 1, 39.00, 1, 48, '0', '2'),
(144, '11', 1, 39.00, 1, 48, '0', '3'),
(145, '11', 1, 29.00, 1, 49, '0', '1'),
(146, '11', 1, 29.00, 1, 49, '0', '2'),
(147, '11', 1, 29.00, 1, 49, '0', '3'),
(148, '11', 1, 29.00, 1, 50, '0', '1'),
(149, '11', 1, 29.00, 1, 50, '0', '2'),
(150, '11', 1, 29.00, 1, 50, '0', '3'),
(151, '11', 1, 39.00, 1, 51, '0', '1'),
(152, '11', 1, 39.00, 1, 51, '0', '2'),
(153, '11', 1, 39.00, 1, 51, '0', '3'),
(154, '11', 1, 39.00, 1, 52, '0', '1'),
(155, '11', 1, 39.00, 1, 52, '0', '2'),
(156, '11', 1, 39.00, 1, 52, '0', '3'),
(157, '11', 1, 39.00, 1, 53, '0', '1'),
(158, '11', 1, 39.00, 1, 53, '0', '2'),
(159, '11', 1, 39.00, 1, 53, '0', '3'),
(160, '11', 1, 39.00, 1, 54, '0', '1'),
(161, '11', 1, 39.00, 1, 54, '0', '2'),
(162, '11', 1, 39.00, 1, 54, '0', '3'),
(163, '11', 1, 49.00, 1, 55, '0', '1'),
(164, '11', 1, 49.00, 1, 55, '0', '2'),
(165, '11', 1, 49.00, 1, 55, '0', '3'),
(166, '11', 1, 49.00, 1, 56, '0', '1'),
(167, '11', 1, 49.00, 1, 56, '0', '2'),
(168, '11', 1, 49.00, 1, 56, '0', '3'),
(169, '11', 1, 49.00, 1, 57, '0', '1'),
(170, '11', 1, 49.00, 1, 57, '0', '2'),
(171, '11', 1, 49.00, 1, 57, '0', '3'),
(172, '11', 1, 19.00, 1, 58, '0', '1'),
(173, '11', 1, 19.00, 1, 58, '0', '2'),
(174, '11', 1, 19.00, 1, 58, '0', '3'),
(175, '11', 1, 19.00, 1, 59, '0', '1'),
(176, '11', 1, 19.00, 1, 59, '0', '2'),
(177, '11', 1, 19.00, 1, 59, '0', '3'),
(178, '11', 1, 19.00, 1, 60, '0', '1'),
(179, '11', 1, 19.00, 1, 60, '0', '2'),
(180, '11', 1, 19.00, 1, 60, '0', '3'),
(181, '11', 1, 19.00, 1, 61, '0', '1'),
(182, '11', 1, 19.00, 1, 61, '0', '2'),
(183, '11', 1, 19.00, 1, 61, '0', '3'),
(184, '11', 1, 49.00, 1, 62, '0', '1'),
(185, '11', 1, 49.00, 1, 62, '0', '2'),
(186, '11', 1, 49.00, 1, 62, '0', '3'),
(187, '11', 1, 49.00, 1, 63, '0', '1'),
(188, '11', 1, 49.00, 1, 63, '0', '2'),
(189, '11', 1, 49.00, 1, 63, '0', '3'),
(190, '11', 1, 49.00, 1, 64, '0', '1'),
(191, '11', 1, 49.00, 1, 64, '0', '2'),
(192, '11', 1, 49.00, 1, 64, '0', '3'),
(193, '11', 1, 49.00, 1, 65, '0', '1'),
(194, '11', 1, 49.00, 1, 65, '0', '2'),
(195, '11', 1, 49.00, 1, 65, '0', '3'),
(196, '11', 1, 49.00, 1, 66, '0', '1'),
(197, '11', 1, 49.00, 1, 66, '0', '2'),
(198, '11', 1, 49.00, 1, 66, '0', '3'),
(199, '11', 1, 49.00, 1, 67, '0', '1'),
(200, '11', 1, 49.00, 1, 67, '0', '2'),
(201, '11', 1, 49.00, 1, 67, '0', '3'),
(202, '11', 1, 49.00, 1, 68, '0', '1'),
(203, '11', 1, 49.00, 1, 68, '0', '2'),
(204, '11', 1, 49.00, 1, 68, '0', '3'),
(205, '11', 1, 49.00, 1, 69, '0', '1'),
(206, '11', 1, 49.00, 1, 69, '0', '2'),
(207, '11', 1, 49.00, 1, 69, '0', '3'),
(208, '11', 1, 49.00, 1, 70, '0', '1'),
(209, '11', 1, 49.00, 1, 70, '0', '2'),
(210, '11', 1, 49.00, 1, 70, '0', '3'),
(211, '11', 1, 49.00, 1, 71, '0', '1'),
(212, '11', 1, 49.00, 1, 71, '0', '2'),
(213, '11', 1, 49.00, 1, 71, '0', '3'),
(214, '11', 1, 49.00, 1, 72, '0', '1'),
(215, '11', 1, 49.00, 1, 72, '0', '2'),
(216, '11', 1, 49.00, 1, 72, '0', '3'),
(217, '11', 1, 49.00, 1, 73, '0', '1'),
(218, '11', 1, 49.00, 1, 73, '0', '2'),
(219, '11', 1, 49.00, 1, 73, '0', '3'),
(220, '11', 1, 49.00, 1, 74, '0', '1'),
(221, '11', 1, 49.00, 1, 74, '0', '2'),
(222, '11', 1, 49.00, 1, 74, '0', '3'),
(223, '11', 1, 49.00, 1, 75, '0', '1'),
(224, '11', 1, 49.00, 1, 75, '0', '2'),
(225, '11', 1, 49.00, 1, 75, '0', '3'),
(226, '11', 1, 49.00, 1, 76, '0', '1'),
(227, '11', 1, 49.00, 1, 76, '0', '2'),
(228, '11', 1, 49.00, 1, 76, '0', '3'),
(229, '11', 1, 49.00, 1, 77, '0', '1'),
(230, '11', 1, 49.00, 1, 77, '0', '2'),
(231, '11', 1, 49.00, 1, 77, '0', '3'),
(232, '11', 1, 49.00, 1, 78, '0', '1'),
(233, '11', 1, 49.00, 1, 78, '0', '2'),
(234, '11', 1, 49.00, 1, 78, '0', '3'),
(235, '11', 1, 49.00, 1, 79, '0', '1'),
(236, '11', 1, 49.00, 1, 79, '0', '2'),
(237, '11', 1, 49.00, 1, 79, '0', '3'),
(238, '11', 1, 49.00, 1, 80, '0', '1'),
(239, '11', 1, 49.00, 1, 80, '0', '2'),
(240, '11', 1, 49.00, 1, 80, '0', '3'),
(241, '11', 1, 49.00, 1, 81, '0', '1'),
(242, '11', 1, 49.00, 1, 81, '0', '2'),
(243, '11', 1, 49.00, 1, 81, '0', '3'),
(244, '11', 1, 49.00, 1, 82, '0', '1'),
(245, '11', 1, 49.00, 1, 82, '0', '2'),
(246, '11', 1, 49.00, 1, 82, '0', '3'),
(247, '11', 1, 49.00, 1, 83, '0', '1'),
(248, '11', 1, 49.00, 1, 83, '0', '2'),
(249, '11', 1, 49.00, 1, 83, '0', '3'),
(250, '11', 1, 49.00, 1, 84, '0', '1'),
(251, '11', 1, 49.00, 1, 84, '0', '2'),
(252, '11', 1, 49.00, 1, 84, '0', '3'),
(253, '11', 1, 49.00, 1, 85, '0', '1'),
(254, '11', 1, 49.00, 1, 85, '0', '2'),
(255, '11', 1, 49.00, 1, 85, '0', '3'),
(256, '11', 1, 49.00, 1, 86, '0', '1'),
(257, '11', 1, 49.00, 1, 86, '0', '2'),
(258, '11', 1, 49.00, 1, 86, '0', '3'),
(259, '11', 1, 49.00, 1, 87, '0', '1'),
(260, '11', 1, 49.00, 1, 87, '0', '2'),
(261, '11', 1, 49.00, 1, 87, '0', '3'),
(262, '11', 1, 49.00, 1, 88, '0', '1'),
(263, '11', 1, 49.00, 1, 88, '0', '2'),
(264, '11', 1, 49.00, 1, 88, '0', '3'),
(265, '11', 1, 49.00, 1, 89, '0', '1'),
(266, '11', 1, 49.00, 1, 89, '0', '2'),
(267, '11', 1, 49.00, 1, 89, '0', '3'),
(268, '11', 1, 49.00, 1, 90, '0', '1'),
(269, '11', 1, 49.00, 1, 90, '0', '2'),
(270, '11', 1, 49.00, 1, 90, '0', '3'),
(271, '11', 1, 49.00, 1, 91, '0', '1'),
(272, '11', 1, 49.00, 1, 91, '0', '2'),
(273, '11', 1, 49.00, 1, 91, '0', '3'),
(274, '11', 1, 49.00, 1, 92, '0', '1'),
(275, '11', 1, 49.00, 1, 92, '0', '2'),
(276, '11', 1, 49.00, 1, 92, '0', '3'),
(277, '11', 1, 49.00, 1, 93, '0', '1'),
(278, '11', 1, 49.00, 1, 93, '0', '2'),
(279, '11', 1, 49.00, 1, 93, '0', '3'),
(280, '11', 1, 49.00, 1, 94, '0', '1'),
(281, '11', 1, 49.00, 1, 94, '0', '2'),
(282, '11', 1, 49.00, 1, 94, '0', '3'),
(283, '11', 1, 19.00, 1, 95, '0', '1'),
(284, '11', 1, 19.00, 1, 95, '0', '2'),
(285, '11', 1, 19.00, 1, 95, '0', '3'),
(286, '11', 1, 19.00, 1, 96, '0', '1'),
(287, '11', 1, 19.00, 1, 96, '0', '2'),
(288, '11', 1, 19.00, 1, 96, '0', '3'),
(289, '11', 1, 19.00, 1, 97, '0', '1'),
(290, '11', 1, 19.00, 1, 97, '0', '2'),
(291, '11', 1, 19.00, 1, 97, '0', '3'),
(292, '11', 1, 19.00, 1, 98, '0', '1'),
(293, '11', 1, 19.00, 1, 98, '0', '2'),
(294, '11', 1, 19.00, 1, 98, '0', '3'),
(295, '11', 1, 19.00, 1, 99, '0', '1'),
(296, '11', 1, 19.00, 1, 99, '0', '2'),
(297, '11', 1, 19.00, 1, 99, '0', '3'),
(298, '11', 1, 19.00, 1, 100, '0', '1'),
(299, '11', 1, 19.00, 1, 100, '0', '2'),
(300, '11', 1, 19.00, 1, 100, '0', '3'),
(301, '11', 1, 49.00, 1, 101, '0', '1'),
(302, '11', 1, 49.00, 1, 101, '0', '2'),
(303, '11', 1, 49.00, 1, 101, '0', '3'),
(304, '11', 1, 19.00, 1, 102, '0', '1'),
(305, '11', 1, 19.00, 1, 102, '0', '2'),
(306, '11', 1, 19.00, 1, 102, '0', '3'),
(307, '11', 1, 19.00, 1, 103, '0', '1'),
(308, '11', 1, 19.00, 1, 103, '0', '2'),
(309, '11', 1, 19.00, 1, 103, '0', '3'),
(310, '11', 1, 19.00, 1, 104, '0', '1'),
(311, '11', 1, 19.00, 1, 104, '0', '2'),
(312, '11', 1, 19.00, 1, 104, '0', '3'),
(313, '11', 1, 19.00, 1, 105, '0', '1'),
(314, '11', 1, 19.00, 1, 105, '0', '2'),
(315, '11', 1, 19.00, 1, 105, '0', '3'),
(316, '11', 1, 19.00, 1, 106, '0', '1'),
(317, '11', 1, 19.00, 1, 106, '0', '2'),
(318, '11', 1, 19.00, 1, 106, '0', '3'),
(319, '11', 1, 19.00, 1, 107, '0', '1'),
(320, '11', 1, 19.00, 1, 107, '0', '2'),
(321, '11', 1, 19.00, 1, 107, '0', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceextra`
--
-- Error reading structure for table booking.tblbinserviceextra: #1146 - Table 'booking.tblbinserviceextra' doesn't exist
-- Error reading data for table booking.tblbinserviceextra: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `booking`.`tblbinserviceextra`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceoptions`
--

CREATE TABLE `tblbinserviceoptions` (
  `idBinServiceOptions` int(12) NOT NULL,
  `idUser` int(12) DEFAULT NULL,
  `idSupplier` varchar(12) DEFAULT NULL,
  `idBinType` int(12) DEFAULT NULL,
  `extraHireagePrice` bigint(20) DEFAULT NULL,
  `extraHireageDays` int(11) DEFAULT NULL,
  `excessWeightPrice` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservicespecialprice`
--
-- Error reading structure for table booking.tblbinservicespecialprice: #1146 - Table 'booking.tblbinservicespecialprice' doesn't exist
-- Error reading data for table booking.tblbinservicespecialprice: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `booking`.`tblbinservicespecialprice`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceupdates`
--

CREATE TABLE `tblbinserviceupdates` (
  `idBinServiceUpdates` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `own` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblbintype`
--

CREATE TABLE `tblbintype` (
  `idBinType` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodeType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description2` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbintype`
--

INSERT INTO `tblbintype` (`idBinType`, `name`, `CodeType`, `description`, `description2`) VALUES
(1, 'Single', '1210', '<ul>\r\n	<li stlye=\"margin:1px;\">No Asbestos.</li>\r\n	<li stlye=\"margin:1px;\">No Liquids.</li>\r\n	<li stlye=\"margin:1px;\">No Car/Truck Batteries.</li>\r\n	<li stlye=\"margin:1px;\">No Mattresses.</li>\r\n	<li stlye=\"margin:1px;\">No Tyres.</li>\r\n	<li stlye=\"margin:1px;\">No Food Waste.</li>\r\n	<li stlye=\"margin:1px;\">No Gas Bottles.</li>\r\n	<li stlye=\"margin:1px;\">No Gas Flares.</li>\r\n</ul>', '<ul>\r\n	<li stlye=\"margin:1px;\">Concrete.</li>\r\n	<li stlye=\"margin:1px;\">Bricks & Rubble.</li>\r\n	<li stlye=\"margin:1px;\">Some Sand & Soil. </li>\r\n	<li stlye=\"margin:1px;\">Plasterboard. </li>\r\n	<li stlye=\"margin:1px;\">Timber. </li>\r\n	<li stlye=\"margin:1px;\">Packaging. </li>\r\n	<li stlye=\"margin:1px;\">Scrap Metal. </li>\r\n	<li stlye=\"margin:1px;\">Etc. </li>\r\n</ul>'),
(2, 'Return', '1211', '<ul>\r\n	<li stlye=\"margin:1px;\">No Asbestos.</li>\r\n	<li stlye=\"margin:1px;\">No Liquids.</li>\r\n	<li stlye=\"margin:1px;\">No Car/Truck Batteries.</li>\r\n	<li stlye=\"margin:1px;\">No Mattresses.</li>\r\n	<li stlye=\"margin:1px;\">No Tyres.</li>\r\n	<li stlye=\"margin:1px;\">No Food Waste.</li>\r\n	<li stlye=\"margin:1px;\">No Gas Bottles.</li>\r\n	<li stlye=\"margin:1px;\">No Gas Flares.</li>\r\n</ul>', '<ul>\r\n	<li stlye=\"margin:1px;\">Soil.</li>\r\n	<li stlye=\"margin:1px;\">Sand.</li>\r\n	<li stlye=\"margin:1px;\">Lawn. </li>\r\n	<li stlye=\"margin:1px;\">Paving. </li>\r\n	<li stlye=\"margin:1px;\">Concrete. </li>\r\n	<li stlye=\"margin:1px;\">Bricks. </li>\r\n	<li stlye=\"margin:1px;\">Etc. </li>\r\n</ul>'),
(3, 'Multi Destinations', '1212', '<ul>\r\n	<li stlye=\"margin:1px;\">No Asbestos.</li>\r\n	<li stlye=\"margin:1px;\">No Liquids.</li>\r\n	<li stlye=\"margin:1px;\">No Car/Truck Batteries.</li>\r\n	<li stlye=\"margin:1px;\">No Mattresses.</li>\r\n	<li stlye=\"margin:1px;\">No Tyres.</li>\r\n	<li stlye=\"margin:1px;\">No Gas Bottles.</li>\r\n	<li stlye=\"margin:1px;\">No Gas Flares.</li>\r\n</ul>', '<ul>\r\n	<li stlye=\"margin:1px;\">Function or Party Waste.</li>\r\n	<li stlye=\"margin:1px;\">Foodwaste.</li>\r\n	<li stlye=\"margin:1px;\">Garden Waste. </li>\r\n	<li stlye=\"margin:1px;\">Plastics. </li>\r\n	<li stlye=\"margin:1px;\">Cardboard. </li>\r\n	<li stlye=\"margin:1px;\">Paper. </li>\r\n	<li stlye=\"margin:1px;\">General Recyclables. </li>\r\n	<li stlye=\"margin:1px;\">Scrap Metals. </li>\r\n	<li stlye=\"margin:1px;\">Timber. </li>\r\n	<li stlye=\"margin:1px;\">Etc. </li>\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tblbookingprice`
--

CREATE TABLE `tblbookingprice` (
  `idBookingPrice` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbookingprice`
--

INSERT INTO `tblbookingprice` (`idBookingPrice`, `price`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 0, 5, '2019-06-04 09:42:48', '2019-06-03 23:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `idCustomer` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `address` text,
  `unit_lot` text,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblextraservices`
--
-- Error reading structure for table booking.tblextraservices: #1146 - Table 'booking.tblextraservices' doesn't exist
-- Error reading data for table booking.tblextraservices: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `booking`.`tblextraservices`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `tblorderservice`
--

CREATE TABLE `tblorderservice` (
  `idOrderService` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `idConsumer` int(11) NOT NULL,
  `paymentUniqueCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryDate` date NOT NULL,
  `totalServiceCharge` double(8,2) NOT NULL,
  `gst` double(8,2) NOT NULL,
  `subtotal` double(8,2) NOT NULL,
  `bookingfee` double(8,2) NOT NULL,
  `collectionDate` date NOT NULL,
  `deliveryAddress` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_lot` text COLLATE utf8mb4_unicode_ci,
  `deliveryComments` text COLLATE utf8mb4_unicode_ci,
  `idSupplier` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `card_category` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_holder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblorderserviceextra`
--

CREATE TABLE `tblorderserviceextra` (
  `idOrderServiceExtra` int(12) NOT NULL,
  `idBinServiceExtra` int(12) NOT NULL,
  `idOrderService` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblorderstatus`
--

CREATE TABLE `tblorderstatus` (
  `idOrderStatus` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `status` char(1) NOT NULL COMMENT '1 = Paid 2= Accepted',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operator` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymentformtemp`
--

CREATE TABLE `tblpaymentformtemp` (
  `idPaymentForm` int(11) NOT NULL,
  `idPaymentTemp` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `unit_lot` text,
  `suburb` varchar(100) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `special_note` text,
  `paymentUniqueCode` varchar(20) DEFAULT NULL,
  `totalprice` float DEFAULT NULL,
  `gst` float NOT NULL,
  `subtotal` float NOT NULL,
  `bookingfee` float NOT NULL,
  `seasonal_mode` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymentformtemp_extra`
--

CREATE TABLE `tblpaymentformtemp_extra` (
  `idPaymentFormExtra` int(12) NOT NULL,
  `idPaymentForm` int(12) NOT NULL,
  `idBinServiceExtra` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymenttemp`
--

CREATE TABLE `tblpaymenttemp` (
  `idPaymentTemp` int(11) NOT NULL,
  `idBinHire` int(11) NOT NULL,
  `deliveryDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `paid` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblplaces`
--

CREATE TABLE `tblplaces` (
  `idplace` int(11) NOT NULL,
  `place` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblplaces`
--

INSERT INTO `tblplaces` (`idplace`, `place`) VALUES
(1, 'Amed'),
(2, 'Gili Air (Gili Islands)'),
(3, 'Gili Meno (Gili Islands)'),
(4, 'Gili Trawangan (Gili Islands)'),
(5, 'Nusa Lembongan'),
(6, 'Nusa Penida'),
(7, 'Padangbai'),
(8, 'Sanur'),
(9, 'Senggigi (Lombok)'),
(10, 'Bangsal (Lombok)');

-- --------------------------------------------------------

--
-- Table structure for table `tblroutes`
--

CREATE TABLE `tblroutes` (
  `idroute` int(11) NOT NULL,
  `destination` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `idtype` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblroutes`
--

INSERT INTO `tblroutes` (`idroute`, `destination`, `to`, `idtype`) VALUES
(1, 10, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1),
(5, 6, 7, 1),
(6, 5, 7, 1),
(7, 4, 7, 1),
(8, 3, 7, 1),
(9, 2, 7, 1),
(10, 10, 7, 1),
(11, 5, 8, 1),
(12, 9, 8, 1),
(13, 2, 8, 1),
(14, 3, 8, 1),
(15, 4, 8, 1),
(16, 1, 2, 1),
(17, 1, 3, 1),
(18, 1, 4, 1),
(19, 1, 10, 1),
(20, 7, 2, 1),
(21, 7, 3, 1),
(22, 7, 4, 1),
(23, 7, 5, 1),
(24, 7, 6, 1),
(25, 7, 10, 1),
(26, 1, 2, 3),
(27, 2, 4, 3),
(28, 4, 7, 3),
(29, 2, 7, 3),
(30, 7, 4, 3),
(31, 4, 2, 3),
(32, 7, 2, 3),
(33, 2, 1, 3),
(34, 8, 4, 1),
(35, 8, 3, 1),
(36, 8, 2, 1),
(37, 8, 5, 1),
(38, 8, 9, 1),
(39, 2, 5, 1),
(40, 3, 5, 1),
(41, 4, 5, 1),
(42, 10, 5, 1),
(43, 9, 5, 1),
(44, 5, 2, 1),
(45, 5, 3, 1),
(46, 5, 4, 1),
(47, 5, 9, 1),
(48, 5, 10, 1),
(49, 2, 6, 1),
(50, 3, 6, 1),
(51, 4, 6, 1),
(52, 10, 6, 1),
(53, 6, 2, 1),
(54, 6, 3, 1),
(55, 6, 4, 1),
(56, 6, 10, 1),
(57, 9, 2, 1),
(58, 9, 3, 1),
(59, 9, 4, 1),
(60, 2, 9, 1),
(61, 3, 9, 1),
(62, 4, 9, 1),
(63, 10, 6, 1),
(64, 5, 8, 1),
(65, 8, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblservicearea`
--

CREATE TABLE `tblservicearea` (
  `idArea` int(11) NOT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `area` text,
  `parentareacode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsize`
--

CREATE TABLE `tblsize` (
  `idSize` int(10) UNSIGNED NOT NULL,
  `size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dimensions` text COLLATE utf8mb4_unicode_ci,
  `idtype` int(11) DEFAULT NULL,
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsize`
--

INSERT INTO `tblsize` (`idSize`, `size`, `dimensions`, `idtype`, `from`, `to`, `time`) VALUES
(1, 'Bangsal (Lombok) - Amed', '', 1, 10, 1, '11:45:00'),
(2, 'Gili Air (Gili Islands) - Amed', '', 1, 2, 1, '11:30:00'),
(3, 'Gili Meno (Gili Islands) - Amed', '', 1, 3, 1, '11:15:00'),
(4, 'Gili Trawangan (Gili Islands) - Amed', '', 1, 4, 1, '11:00:00'),
(5, 'Nusa Penida - Padangbai', '', 1, 6, 7, '09:35:00'),
(6, 'Nusa Lembongan - Padangbai', '', 1, 5, 7, '09:15:00'),
(7, 'Gili Trawangan (Gili Islands) - Padangbai', '', 1, 4, 7, '11:30:00'),
(10, 'Gili Meno (Gili Islands) - Padangbai', '', 1, 3, 7, '11:45:00'),
(11, 'Gili Air (Gili Islands) - Padangbai', '', 1, 2, 7, '12:00:00'),
(12, 'Bangsal (Lombok) - Padangbai', '', 1, 10, 7, '12:15:00'),
(13, 'Bangsal (Lombok) - Padangbai', NULL, 1, 10, 7, '10:30:00'),
(14, 'Gili Air (Gili Islands) - Padangbai', NULL, 1, 2, 7, '10:30:00'),
(15, 'Gili Trawangan (Gili Islands) - Padangbai', NULL, 1, 4, 7, '10:30:00'),
(16, 'Gili Trawangan (Gili Islands) - Padangbai', NULL, 1, 4, 7, '13:00:00'),
(17, 'Gili Meno (Gili Islands) - Padangbai', NULL, 1, 3, 7, '13:15:00'),
(18, 'Gili Air (Gili Islands) - Padangbai', NULL, 1, 2, 7, '13:30:00'),
(19, 'Bangsal (Lombok) - Padangbai', NULL, 1, 10, 7, '13:45:00'),
(20, 'Gili Trawangan (Gili Islands) - Padangbai', NULL, 1, 4, 7, '16:00:00'),
(21, 'Gili Air (Gili Islands) - Padangbai', NULL, 1, 2, 7, '16:30:00'),
(22, 'Bangsal (Lombok) - Padangbai', NULL, 1, 10, 7, '16:45:00'),
(23, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '08:30:00'),
(24, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '10:45:00'),
(25, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '13:00:00'),
(26, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '16:30:00'),
(27, 'Senggigi (Lombok - Sanur)', NULL, 1, 9, 8, '12:30:00'),
(28, 'Gili Air (Gili Islands) - Sanur', NULL, 1, 2, 8, '12:45:00'),
(29, 'Gili Meno (Gili Islands) - Sanur', NULL, 1, 3, 8, '13:00:00'),
(30, 'Gili Trawangan (Gili Islands) - Sanur', NULL, 1, 4, 8, '13:45:00'),
(31, 'Amed - Gili Trawangan (Gili Islands)', NULL, 1, 1, 4, '09:30:00'),
(32, 'Amed - Gili Meno (Gili Islands)', NULL, 1, 1, 3, '09:30:00'),
(33, 'Amed - Gili Air (Gili Islands)', NULL, 1, 1, 2, '09:30:00'),
(34, 'Amed - Bangsal (Lombok)', NULL, 1, 1, 10, '09:30:00'),
(38, 'Padangbai - Gili Trawangan (Gili Islands)', NULL, 1, 7, 4, '09:00:00'),
(39, 'Padangbai - Gili Trawangan (Gili Islands)', NULL, 1, 7, 4, '09:30:00'),
(40, 'Padangbai - Gili Trawangan (Gili Islands)', NULL, 1, 7, 4, '10:30:00'),
(41, 'Padangbai - Gili Trawangan (Gili Islands)', NULL, 1, 7, 4, '13:30:00'),
(42, 'Padangbai - Gili Meno (Gili Islands)', NULL, 1, 7, 3, '09:00:00'),
(43, 'Padangbai - Gili Meno (Gili Islands)', NULL, 1, 7, 3, '10:30:00'),
(44, 'Padangbai - Gili Meno (Gili Islands)', NULL, 1, 7, 3, '13:30:00'),
(45, 'Padangbai - Gili Air (Gili Islands)', NULL, 1, 7, 2, '09:00:00'),
(46, 'Padangbai - Gili Air (Gili Islands)', NULL, 1, 7, 2, '09:30:00'),
(47, 'Padangbai - Gili Air (Gili Islands)', NULL, 1, 7, 2, '10:30:00'),
(48, 'Padangbai - Gili Air (Gili Islands)', NULL, 1, 7, 2, '13:30:00'),
(49, 'Padangbai - Nusa Penida', NULL, 1, 7, 6, '15:00:00'),
(50, 'Padangbai - Nusa Lembongan', NULL, 1, 7, 5, '15:00:00'),
(51, 'Padangbai - Bangsal (Lombok)', NULL, 1, 7, 10, '09:00:00'),
(52, 'Padangbai - Bangsal (Lombok)', NULL, 1, 7, 10, '09:30:00'),
(53, 'Padangbai - Bangsal (Lombok)', NULL, 1, 7, 10, '10:30:00'),
(54, 'Padangbai - Bangsal (Lombok)', NULL, 1, 7, 10, '13:30:00'),
(55, 'Sanur - Gili Trawangan (Gili Islands)', NULL, 1, 8, 4, '09:30:00'),
(56, 'Sanur - Gili Meno (Gili Islands)', NULL, 1, 8, 3, '09:30:00'),
(57, 'Sanur - Gili Air (Gili Islands)', NULL, 1, 8, 2, '09:30:00'),
(58, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '09:30:00'),
(59, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '11:45:00'),
(60, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '14:30:00'),
(61, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '17:00:00'),
(62, 'Sanur - Senggigi (Lombok)', NULL, 1, 8, 9, '09:30:00'),
(63, 'Gili Trawangan (Gili Islands) - Nusa Lembongan', NULL, 1, 4, 5, '08:30:00'),
(64, 'Gili Trawangan (Gili Islands) - Nusa Lembongan', NULL, 1, 4, 5, '12:00:00'),
(65, 'Gili Trawangan (Gili Islands) - Nusa Lembongan', NULL, 1, 4, 5, '13:45:00'),
(66, 'Gili Meno (Gili Islands) - Nusa Lembongan', NULL, 1, 3, 5, '08:45:00'),
(67, 'Gili Meno (Gili Islands) - Nusa Lembongan', NULL, 1, 3, 5, '12:15:00'),
(68, 'Gili Meno (Gili Islands) - Nusa Lembongan', NULL, 1, 3, 5, '13:00:00'),
(69, 'Gili Air (Gili Islands) - Nusa Lembongan', NULL, 1, 2, 5, '09:15:00'),
(70, 'Gili Air (Gili Islands) - Nusa Lembongan', NULL, 1, 2, 5, '12:30:00'),
(71, 'Gili Air (Gili Islands) - Nusa Lembongan', NULL, 1, 2, 5, '12:45:00'),
(72, 'Bangsal (Lombok)- Nusa Lembongan', NULL, 1, 10, 5, '08:30:00'),
(73, 'Bangsal (Lombok)- Nusa Lembongan', NULL, 1, 10, 5, '13:00:00'),
(74, 'Senggigi (Lombok) - Nusa Lembongan', NULL, 1, 9, 5, '12:30:00'),
(75, 'Nusa Lembongan - Gili Trawangan (Gili Islands)', NULL, 1, 5, 4, '09:15:00'),
(76, 'Nusa Lembongan - Gili Trawangan (Gili Islands)', NULL, 1, 5, 4, '10:45:00'),
(77, 'Nusa Lembongan - Gili Meno (Gili Islands)', NULL, 1, 4, 3, '09:15:00'),
(78, 'Nusa Lembongan - Gili Meno (Gili Islands)', NULL, 1, 4, 3, '10:45:00'),
(79, 'Nusa Lembongan - Gili Air (Gili Islands)', NULL, 1, 4, 2, '09:15:00'),
(80, 'Nusa Lembongan - Gili Air (Gili Islands)', NULL, 1, 4, 2, '10:45:00'),
(81, 'Nusa Lembongan - Bangsal (Lombok)', NULL, 1, 5, 10, '09:15:00'),
(82, 'Nusa Lembongan - Senggigi (Lombok)', NULL, 1, 5, 9, '10:45:00'),
(83, 'Gili Trawangan (Gili Islands) - Nusa Penida', NULL, 1, 4, 6, '08:30:00'),
(84, 'Gili Trawangan (Gili Islands) - Nusa Penida', NULL, 1, 4, 6, '12:00:00'),
(85, 'Gili Air (Gili Islands) - Nusa Penida', NULL, 1, 2, 6, '09:15:00'),
(86, 'Gili Air (Gili Islands) - Nusa Penida', NULL, 1, 2, 6, '12:30:00'),
(87, 'Gili Meno (Gili Islands) - Nusa Penida', NULL, 1, 3, 6, '08:45:00'),
(88, 'Gili Meno (Gili Islands) - Nusa Penida', NULL, 1, 3, 6, '12:15:00'),
(89, 'Bangsal (Lombok) - Nusa Penida', NULL, 1, 10, 6, '09:30:00'),
(90, 'Bangsal (Lombok) - Nusa Penida', NULL, 1, 10, 6, '13:00:00'),
(91, 'Nusa Penida - Gili Trawangan (Gili Islands)', NULL, 1, 6, 4, '09:35:00'),
(92, 'Nusa Penida - Gili Meno (Gili Islands)', NULL, 1, 6, 3, '09:35:00'),
(93, 'Nusa Penida - Gili Air (Gili Islands)', NULL, 1, 6, 2, '09:35:00'),
(94, 'Nusa Penida - Bangsal (Lombok)', NULL, 1, 6, 10, '09:35:00'),
(95, 'Senggigi (Lombok) - Gili Trawangan (Gili Islands)', NULL, 1, 9, 4, '12:30:00'),
(96, 'Senggigi (Lombok) - Gili Meno (Gili Islands)', NULL, 1, 9, 3, '12:30:00'),
(97, 'Senggigi (Lombok) - Gili Air (Gili Islands)', NULL, 1, 9, 2, '12:30:00'),
(98, 'Gili Trawangan (Gili Islands) - Senggigi (Lombok)', NULL, 1, 4, 9, '13:45:00'),
(99, 'Gili Meno (Gili Islands) - Senggigi (Lombok)', NULL, 1, 3, 9, '14:00:00'),
(100, 'Gili Air (Gili Islands) - Senggigi (Lombok)', NULL, 1, 2, 9, '14:15:00'),
(101, 'Bangsal (Lombok) - Nusa Penida', NULL, 1, 10, 6, '08:30:00'),
(102, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '08:15:00'),
(103, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '11:00:00'),
(104, 'Nusa Lembongan - Sanur', NULL, 1, 5, 8, '16:15:00'),
(105, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '09:15:00'),
(106, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '12:30:00'),
(107, 'Sanur - Nusa Lembongan', NULL, 1, 8, 5, '17:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblspecialdays`
--
-- Error reading structure for table booking.tblspecialdays: #1146 - Table 'booking.tblspecialdays' doesn't exist
-- Error reading data for table booking.tblspecialdays: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `booking`.`tblspecialdays`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `tblsupplier`
--

CREATE TABLE `tblsupplier` (
  `idSupplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobilePhone` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `email2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSaturday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSunday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mainServiceArea` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceContact` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServicePhone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceMobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullAddress` text COLLATE utf8mb4_unicode_ci,
  `abn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasChild` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stocking_off` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seasonal_on` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seasonal_title` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsupplier`
--

INSERT INTO `tblsupplier` (`idSupplier`, `name`, `contactName`, `phonenumber`, `email`, `mobilePhone`, `comments`, `email2`, `isOpenSaturday`, `isOpenSunday`, `mainServiceArea`, `customerServiceContact`, `customerServicePhone`, `customerServiceMobile`, `fullAddress`, `abn`, `hasChild`, `stocking_off`, `seasonal_on`, `seasonal_title`) VALUES
(11, 'Sea Express ', 'Sea Express Admin', '089658595043', 'tutux.hollowbody@gmail.com', '089658595043', 'test', NULL, '0', '0', '2', 'Sea Express Support', '089658595043', '089658595043', NULL, '84 607 972 896', '1', '1', '0', 'Winter 2019 Special Pricing');

-- --------------------------------------------------------

--
-- Table structure for table `tblsupplierchild`
--
-- Error reading structure for table booking.tblsupplierchild: #1146 - Table 'booking.tblsupplierchild' doesn't exist
-- Error reading data for table booking.tblsupplierchild: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `booking`.`tblsupplierchild`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `idUser` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminApproved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userStatus` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registerDate` date NOT NULL,
  `adminNotifications` char(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`idUser`, `idSupplier`, `username`, `password`, `isActive`, `role`, `adminApproved`, `userStatus`, `registerDate`, `adminNotifications`) VALUES
(5, 11, 'webmaster', '62c8ad0a15d9d1ca38d5dee762a16e01', '1', '1', '1', '1', '2018-06-01', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbluserservicearea`
--

CREATE TABLE `tbluserservicearea` (
  `idUserServiceArea` int(10) NOT NULL,
  `idUser` int(10) NOT NULL,
  `idSupplier` varchar(10) NOT NULL,
  `idServiceArea` int(10) NOT NULL,
  `serviceAreaParent` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_confirmation`
--

CREATE TABLE `user_confirmation` (
  `idConfirmation` int(11) NOT NULL,
  `idSupplier` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_confirmation`
--

INSERT INTO `user_confirmation` (`idConfirmation`, `idSupplier`, `idUser`, `token`) VALUES
(6, 17, 11, 'flPpj8K862cJCeQ0y120SyTlb89MIL');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `csv_data`
--
ALTER TABLE `csv_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  ADD PRIMARY KEY (`idNonDeliveryDays`);

--
-- Indexes for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  ADD PRIMARY KEY (`idBinService`);

--
-- Indexes for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  ADD PRIMARY KEY (`idBinServiceOptions`);

--
-- Indexes for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  ADD PRIMARY KEY (`idBinServiceUpdates`);

--
-- Indexes for table `tblbintype`
--
ALTER TABLE `tblbintype`
  ADD PRIMARY KEY (`idBinType`);

--
-- Indexes for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  ADD PRIMARY KEY (`idBookingPrice`);

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  ADD PRIMARY KEY (`idOrderService`);

--
-- Indexes for table `tblorderserviceextra`
--
ALTER TABLE `tblorderserviceextra`
  ADD PRIMARY KEY (`idOrderServiceExtra`);

--
-- Indexes for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  ADD PRIMARY KEY (`idOrderStatus`);

--
-- Indexes for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  ADD PRIMARY KEY (`idPaymentForm`);

--
-- Indexes for table `tblpaymentformtemp_extra`
--
ALTER TABLE `tblpaymentformtemp_extra`
  ADD PRIMARY KEY (`idPaymentFormExtra`);

--
-- Indexes for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  ADD PRIMARY KEY (`idPaymentTemp`);

--
-- Indexes for table `tblplaces`
--
ALTER TABLE `tblplaces`
  ADD PRIMARY KEY (`idplace`);

--
-- Indexes for table `tblroutes`
--
ALTER TABLE `tblroutes`
  ADD PRIMARY KEY (`idroute`);

--
-- Indexes for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  ADD PRIMARY KEY (`idArea`);

--
-- Indexes for table `tblsize`
--
ALTER TABLE `tblsize`
  ADD PRIMARY KEY (`idSize`);

--
-- Indexes for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  ADD PRIMARY KEY (`idSupplier`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  ADD PRIMARY KEY (`idUserServiceArea`);

--
-- Indexes for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  ADD PRIMARY KEY (`idConfirmation`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `csv_data`
--
ALTER TABLE `csv_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  MODIFY `idNonDeliveryDays` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  MODIFY `idBinService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=322;

--
-- AUTO_INCREMENT for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  MODIFY `idBinServiceOptions` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  MODIFY `idBinServiceUpdates` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblbintype`
--
ALTER TABLE `tblbintype`
  MODIFY `idBinType` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  MODIFY `idBookingPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `idCustomer` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  MODIFY `idOrderService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblorderserviceextra`
--
ALTER TABLE `tblorderserviceextra`
  MODIFY `idOrderServiceExtra` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  MODIFY `idOrderStatus` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  MODIFY `idPaymentForm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpaymentformtemp_extra`
--
ALTER TABLE `tblpaymentformtemp_extra`
  MODIFY `idPaymentFormExtra` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  MODIFY `idPaymentTemp` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblplaces`
--
ALTER TABLE `tblplaces`
  MODIFY `idplace` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblroutes`
--
ALTER TABLE `tblroutes`
  MODIFY `idroute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  MODIFY `idArea` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblsize`
--
ALTER TABLE `tblsize`
  MODIFY `idSize` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  MODIFY `idSupplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `idUser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  MODIFY `idUserServiceArea` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  MODIFY `idConfirmation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
