<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class place extends Model
{
	protected $table = 'tblplaces';
	public $timestamps = false;
}
