<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblorderservice extends Model
{
	protected $table = 'tblorderservice';
	public $timestamps = false;
}
