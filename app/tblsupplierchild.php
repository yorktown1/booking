<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblsupplierchild extends Model
{
	protected $table = 'tblsupplierchild';
	public $timestamps = false;
}
