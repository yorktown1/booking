<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblbintype extends Model
{
	protected $table = 'tblbintype';
	public $timestamps = false;
}
