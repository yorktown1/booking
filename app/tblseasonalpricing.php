<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblseasonalpricing extends Model
{
	protected $table = 'tblbinservicespecialprice';
	public $timestamps = false;
}
