<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblextraservices extends Model
{
	protected $table = 'tblextraservices';
	public $timestamps = false;
}
