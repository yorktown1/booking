<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class routes extends Model
{
	protected $table = 'tblroutes';
	public $timestamps = false;
}
