<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblpaymentformtemp_extra extends Model
{
	protected $table = 'tblpaymentformtemp_extra';
	public $timestamps = false;
}
