<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\tblsupplier;
use App\tblbintype;
use App\tblsize;
use App\tblservicearea;
use App\tblbinnondelivery;
use App\place;
use App\routes;
use Validator;
use App\Transformer\bintypeTransformer;
use App\Transformer\sizeTransformer;
use App\Transformer\binhireTransformer;
use App\Transformer\stateTransformer;
use App\Transformer\binhireoptionsTransformer;
use App\Transformer\binhire_dailyTransformer;
use App\Transformer\paymentTransformer;
use App\Transformer\nondeliveryTransformer;
use App\Transformer\extraservicesTransformer;
use App\Transformer\supplierTransformer;
use App\Transformer\placeTransformer;
use App\Transformer\routesTransformer;
use App\Transformer\tripTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    protected $respose;
    protected $custZipCode;
    public function __construct(Response $response){
        $this->response = $response;
    }

    /************
	** Show service variant code
	***********/
    public function showSizes(Request $request){
        if ($request->isMethod('get')){
            $size = tblsize::paginate();
            if (!$size) {
                return $this->response->errorNotFound('No bin size data');
            } else {
                return $this->response->withPaginator($size, new  sizeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }
	
	/************
	** Show service variant
	***********/
    public function findSize(Request $request){
        if ($request->isMethod('get')){
            $idSize = $request['idSize'];
            $size = tblsize::where(['idSize' => $idSize])->first();
            if (!$size) {
                return $this->response->errorNotFound('No bin size data');
            } else {
                return $this->response->withItem($size, new  sizeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

     /************
	** Show service types
	***********/
    public function showBinType(Request $request){
        if ($request->isMethod('get')){
            $bintype = tblbintype::paginate();
            if (!$bintype) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withPaginator($bintype, new  bintypeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }
	
	 /************
	** Show specific service type
	***********/
     public function findBinType(Request $request){
        if ($request->isMethod('get')){
            $idBinType = $request['idBinType'];
            $bintype = tblbintype::where(['idBinType' => $idBinType])->first();
            if (!$bintype) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withItem($bintype, new  bintypeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

	 /************
	** Show extras
	***********/
	public function showbinextras(Request $request){
		if ($request->isMethod('get')){
			$extraservices = tblextraservices::all();
			if(!$extraservices){
				return $this->response->errorNotFound('No bin type data');
			} else {
				return $this->response->withCollection($extraservices, new  extraservicesTransformer());
			}
		} else {
			return $this->response->errorInternalError('Internal server error');
		}
		return $this->response->errorInternalError('Internal server error');
	}
	
	/************
	** Show available pickup place
	***********/
	public function showplaces(Request $request){
		if ($request->isMethod('get')){
			$places = place::all();
			if(!$places){
				return $this->response->errorNotFound('No bin type data');
			} else {
				return $this->response->withCollection($places, new  placeTransformer());
			}
		} else {
			return $this->response->errorInternalError('Internal server error');
		}
		return $this->response->errorInternalError('Internal server error');
	}
	
	/************
	** Show available routes based on pickup place
	***********/
	
	public function showroutes(Request $request){
		if ($request->isMethod('get')){
			$routes = DB::table('tblroutes')
				->leftJoin('tblplaces', 'tblplaces.idplace', '=',  'tblroutes.to')
				->where([
					'tblroutes.destination' => $request['from'],
					'tblroutes.idtype' => $request['type']
				])
				->select('tblplaces.idplace', 'tblplaces.place', 'tblroutes.idtype')
				->get();
			if(!$routes){
				return $this->response->errorNotFound('No bin type data');
			} else {
				return $this->response->withCollection($routes, new  routesTransformer());
			}
		} else {
			return $this->response->errorInternalError('Internal server error');
		}
		return $this->response->errorInternalError('Internal server error');
	}
	
	/************
	** Show available trips
	***********/
	
	public function findtrip(Request $request){
		$idBinType = $request['idBinType'];
		$from = $request['from'];
		$to = $request['to'];
		if ($request->isMethod('get')){
			$binhire = DB::table('tblbinservice')
			->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
			->leftJoin('tblbintype', 'tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize','=','tblbinservice.idBinSize')
			->leftJoin('tblroutes', 'tblroutes.idtype','=','tblsize.idtype')
			->where([
				'tblsize.from' => $from,
				'tblsize.to' => $to,
				'tblsize.idtype' => $idBinType
			])
			->where('tblbinservice.stock','>',0)
			->where('tblbinservice.price','>',0)
			->select('tblsize.size', 'tblbinservice.idBinService', 'tblbinservice.idBinSize', 'tblbinservice.own', 'tblbinservice.price', 'tblsize.from', 'tblsize.to', 'tblsize.idtype')
			->groupBy('tblsize.size', 'tblbinservice.idBinService', 'tblbinservice.idBinSize', 'tblbinservice.own', 'tblbinservice.price', 'tblsize.from', 'tblsize.to', 'tblsize.idtype')
			->orderBy('tblbinservice.price','asc')
			->orderBy('tblbinservice.own','asc')
			->get();
			
			if(!$binhire){
				return $this->response->errorNotFound('No bin type data');
			} else {
				return $this->response->withCollection($binhire, new tripTransformer());
			}
		} else {
			return $this->response->errorInternalError('Internal server error');
		}
		return $this->response->errorInternalError('Internal server error');
	}
	
    /************
	** Get the best boooking rate
	***********/
    public function showBinHire(Request $request){
        $idBinType = $request['idBinType'];
        $date1= $request['deliverydate'];
        $date2 = $request['collectiondate'];
        $nondeliveryfilter = null;
		$today = date('l', strtotime('now'));
		$day_deliv = date('l', strtotime($date1));
		$day_col = date('l', strtotime($date2));
		$from = $request['from'];
		$to = $request['to'];
		$own = $request['own'];
		
        if ($request->isMethod('get')){
			if($idBinType == 1){
				$nondeliveryfilter = $this->single_lookup($idBinType, $date1, $date2,$nondeliveryfilter,
				$today,$day_deliv, $day_col, $from, $to, $own );
			} elseif($idBinType == 2) {
				$nondeliveryfilter = $this->return_lookup($idBinType, $date1, $date2, $nondeliveryfilter, 
				$today,$day_deliv, $day_col, $from, $to, $own);
			}
            if (is_null($nondeliveryfilter)) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withCollection($nondeliveryfilter, new  binhireTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }
	
	/************
	** Get the best boooking rate by multiple destination
	***********/
    public function show_multiple_destination(Request $request){
        $idBinType = $request['idBinType'];
        $date1= $request['deliverydate'];
        $date2 = $request['collectiondate'];
        $nondeliveryfilter = null;
		$today = date('l', strtotime('now'));
		$day_deliv = date('l', strtotime($date1));
		$day_col = date('l', strtotime($date2));
		$from = $request['from'];
		$to = $request['to'];
		$own = $request['own'];
		
        if ($request->isMethod('get')){
			$nondeliveryfilter = $this->multi_lookup($idBinType, $date1, $date2, $nondeliveryfilter, 
			$today,$day_deliv, $day_col, $from, $to, $own);
            if (is_null($nondeliveryfilter)) {
                return $this->response->errorNotFound('No bin type data');
            } else {
                return $this->response->withCollection($nondeliveryfilter, new  binhireTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }
	
	
	private function single_lookup($idBinType, $date1, $date2, $nondeliveryfilter = null, $today, $day_deliv, $day_col, $from, $to, $own){
		
		if($idBinType == 1){
			$binhire = DB::table('tblbinservice')
			->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
			->leftJoin('tblbintype', 'tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize','=','tblbinservice.idBinSize')
			->leftJoin('tblroutes', 'tblroutes.idtype','=','tblsize.idtype')
			->leftJoin('tblplaces', 'tblroutes.to','=','tblplaces.idplace')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->where([
				'tblsize.from' => $from,
				'tblsize.to' => $to,
				'tblsize.idtype' => $idBinType,
				'tblbinservice.own' => $own
			])
			->select('tblbinservice.price','tblbinservice.stock', 'tblbinservice.idBinService',
				'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinservice.own',
				'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to', 'tblsize.time')
			->groupBy('tblbinservice.price','tblbinservice.stock', 'tblbinservice.idBinService',
				'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinservice.own',
				'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to', 'tblsize.time')
			->orderBy('tblbinservice.price','asc')
			->get();
		}
		return $binhire;
	}
	
	private function return_lookup($idBinType, $date1, $date2, $nondeliveryfilter = null, $today, $day_deliv, $day_col, $from, $to, $own){
		if($idBinType == 2){
			$binhire = DB::table('tblbinservice')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
				->leftJoin('tblbintype', 'tblbinservice.idBinType','=','tblbintype.idBinType')
				->leftJoin('tblsize', 'tblsize.idSize','=','tblsize.idtype')
				->leftJoin('tblroutes', 'tblroutes.idtype','=','tblsize.idtype')
				->leftJoin('tblplaces', 'tblroutes.to','=','tblplaces.idplace')
				->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
				->where([
					'tblbinservice.unqueriable' => '0',
					'tblbinservice.own' => $own
				])
				->where(function($q) use ($from, $to, $idBinType)  {
					$q->where(['tblsize.from' => $from,
					'tblsize.to' => $to,
					'tblsize.idtype' => $idBinType])
					->orWhere(['tblsize.from' => $to,
					'tblsize.to' => $from,
					'tblsize.idtype' => $idBinType]);
				})
				->where('tblbinservice.stock','>',0)
				->where('tblbinservice.price','>',0)
				->select('tblbinservice.price','tblbinservice.stock', 'tblbinservice.idBinService',
					'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinservice.own',
					'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to')
				->groupBy('tblbinservice.price','tblbinservice.stock', 'tblbinservice.idBinService',
					'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinservice.own',
					'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to')
				->orderBy('tblbinservice.price','asc')
				->first();
		}
		return $binhire;
	}
	
	private function multi_lookup($idBinType, $date1, $date2, $nondeliveryfilter = null, $today, $day_deliv, $day_col, $from, $to, $own){
		if($idBinType == 3){
			$binhire = DB::table('tblbinservice')
			->leftJoin('tblsupplier', 'tblbinservice.idSupplier','=','tblsupplier.idSupplier')
			->leftJoin('tblbintype', 'tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize','=','tblbinservice.idBinSize')
			->leftJoin('tblroutes', 'tblroutes.idtype','=','tblsize.idtype')
			->where([
				'tblbinservice.unqueriable' => '0',
				'tblbinservice.own' => $own,
			])
			->where(function($q, $from, $to, $idBinType) {
				$q->where(['tblroutes.destination' => $from,
				'tblsize.idtype' => $idBinType])
				->orWhere(['tblroutes.destination' => $to,
				'tblsize.idtype' => $idBinType]);
			})
			->where('tblbinservice.stock','>',0)
			->where('tblbinservice.price','>',0)
			->orderBy('tblbinservice.price','asc')
			->get();
			$i=1;
			while ($i<=count($binhire)){
				$binhire = DB::table('tblbinservice')
				->leftJoin('tblbinserviceupdates', 'tblbinservice.idBinService', '=', 'tblbinserviceupdates.idBinService')
				->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
				->leftJoin('tblbintype', 'tblbinservice.idBinType','=','tblbintype.idBinType')
				->leftJoin('tblsize', 'tblsize.idSize','=','tblbinservice.idBinSize')
				->leftJoin('tblroutes', 'tblroutes.idtype','=','tblsize.idtype')
				->leftJoin('tblplaces', 'tblroutes.to','=','tblplaces.idplace')
				->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
				->where([
					'tblbinservice.unqueriable' => '0',
					'tblbinserviceupdates.own' => $own,
				])
				->where(function($q, $from, $to, $idBinType) {
					$q->where(['tblroutes.destination' => $from,
					'tblsize.idtype' => $idBinType])
					->orWhere(['tblroutes.destination' => $to,
					'tblsize.idtype' => $idBinType]);
				})
				->where('tblbinserviceupdates.stock','>',0)
				->where('tblbinserviceupdates.price','>',0)
				->where('tblbinserviceupdates.date','=', $date1)
				->select('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService',
					'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinserviceupdates.own',
					'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to')
				->groupBy('tblbinserviceupdates.price','tblbinserviceupdates.stock', 'tblbinservice.idBinService',
					'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinserviceupdates.own',
					'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to')
				->orderBy('tblbinserviceupdates.price','asc')
				->get();

                if(!is_null($binhire)){
					$nondeliveryfilter = $binhire;
					break;
                } else{
					$binhire = DB::table('tblbinservice')
					
					->leftJoin('tblsupplier', 'tblsupplier.idSupplier', '=', 'tblbinservice.idSupplier')
					->leftJoin('tblbintype', 'tblbinservice.idBinType','=','tblbintype.idBinType')
					->leftJoin('tblsize', 'tblsize.idSize','=','tblbinservice.idBinSize')
					->leftJoin('tblroutes', 'tblroutes.idtype','=','tblsize.idtype')
					->leftJoin('tblplaces', 'tblroutes.to','=','tblplaces.idplace')
					->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
					->where([
						'tblbinservice.unqueriable' => '0',
						'tblbinservice.own' => $own,
					])
					->where(function($q, $from, $to, $idBinType) {
						$q->where(['tblroutes.destination' => $from,
						'tblsize.idtype' => $idBinType])
						->orWhere(['tblroutes.destination' => $to,
						'tblsize.idtype' => $idBinType]);
					})
					->where('tblbinservice.stock','>',0)
					->where('tblbinservice.price','>',0)
					->select('tblbinservice.price','tblbinservice.stock', 'tblbinservice.idBinService',
						'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinservice.own',
						'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to')
					->groupBy('tblbinservice.price','tblbinservice.stock', 'tblbinservice.idBinService',
						'tblbinservice.idBinType', 'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbinservice.own',
						'tblplaces.idplace', 'tblplaces.place', 'tblsize.size','tblsize.from', 'tblsize.to')
					->orderBy('tblbinservice.price','asc')
					->get();

                    if(!is_null($binhire)){
						$nondeliveryfilter = $binhire;
						break;
					} else {
						$nondeliveryfilter = null;
						break;
					}
				}
				$i++;
			}
		}
		return $nondeliveryfilter;
	}
	
    public function showState(Request $request){
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['parentareacode' => '0'])->paginate();
             if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withPaginator($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function showArea(Request $request){
        $parentareacode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['parentareacode' => $parentareacode])->orderBy('area', 'asc')->get();
             if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withCollection($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function showPostcode(Request $request){
        $parentareacode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['parentareacode' => $parentareacode])->get();
            if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withCollection($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }

    public function findArea(Request $request){
        $zipcode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['zipcode' => $zipcode])->first();
            if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withItem($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }
	
	public function findAreaAll(Request $request){
        $zipcode = $request['zipcode'];
        if ($request->isMethod('get')){
             $state = tblservicearea::where(['zipcode' => $zipcode])->get();
            if (!$state) {
                return $this->response->errorNotFound('No state data');
            } else {
                return $this->response->withCollection($state, new  stateTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }
	
    public function binhireOptions(Request $request){
        $idBinType = $request['idBinType'];
        $idSupplier = $request['idSupplier'];

        $binhireoptions = DB::table('tblbinserviceoptions')
                        ->leftJoin('tblbinservice', function ($query){
                             $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                ->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                        })
                        ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                        ->where([
                            'tblbinserviceoptions.idBinType' => $idBinType,
                            'tblbinserviceoptions.idSupplier' =>$idSupplier
                            ])
                        ->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                        ->first();
        if (!$binhireoptions) {
            return $this->response->errorNotFound('No state data');
        } else {
            return $this->response->withItem($binhireoptions, new  binhireoptionsTransformer());
        }
    }
	
	public function nondeliverydays(Request $request){
		$idBinType = $request['idBinType'];
		$idSupplier = $request['idSupplier']; 
		
		if ($request->isMethod('get')){
			//$nondeliverydays = DB::table('tblbinnondelivery')
			//		->select('idNonDeliveryDays', 'date', 'idSupplier', 'idBinType')
			//		->where(['idBinType' => $idBinType, 'idSupplier' => $idSupplier])
			//		->get();
			$nondeliverydays = tblbinnondelivery::where(['idBinType' => $idBinType, 'idSupplier' => $idSupplier])->get();
			
			if (!is_null($nondeliverydays)){
				 return $this->response->withCollection($nondeliverydays, new  nondeliveryTransformer());
			} else {
				return $this->response->errorNotFound('No non delivery data');
			}
		}
	}
	
	public function weekendbin(Request $request){
		$idSupplier = $request['idSupplier']; 
		
		if ($request->isMethod('get')){
			//$nondeliverydays = DB::table('tblbinnondelivery')
			//		->select('idNonDeliveryDays', 'date', 'idSupplier', 'idBinType')
			//		->where(['idBinType' => $idBinType, 'idSupplier' => $idSupplier])
			//		->get();
			$tblsupplier = tblsupplier::where(['idSupplier' => $idSupplier])->first();
			
			if (!is_null($tblsupplier)){
				 return $this->response->withItem($tblsupplier, new  supplierTransformer());
			} else {
				return $this->response->errorNotFound('No weekend bin');
			}
		}
	}
	
	public function alwaysOn(Request $request){
		$idSupplier = $request['idSupplier']; 
		
		if ($request->isMethod('get')){
			//$nondeliverydays = DB::table('tblbinnondelivery')
			//		->select('idNonDeliveryDays', 'date', 'idSupplier', 'idBinType')
			//		->where(['idBinType' => $idBinType, 'idSupplier' => $idSupplier])
			//		->get();
			$tblsupplier = tblsupplier::where(['idSupplier' => $idSupplier])->first();
			
			if (!is_null($tblsupplier)){
				 return $this->response->withItem($tblsupplier, new  supplierTransformer());
			} else {
				return $this->response->errorNotFound('No always on');
			}
		}
	}
	
	
	
    public function payment(Request $request){
        $idBinHire = $request['idbinhire'];
        $zipcode = $request['zipcode'];
        $deliverydate = $request['deliverydate'];
        $collectiondate = $request['collectiondate'];

        if ($request->isMethod('get')){
            $binhire = DB::table('tblbinservice')
                    ->select('idBinService')
                    ->where(['idBinService' => $idBinHire])
                    ->first();
            if (!is_null($idBinHire)){
                $id = DB::table('tblpaymenttemp')->insertGetId(
                    ['idBinHire' => $binhire->idBinService, 'zipcode' => $zipcode, 'deliveryDate' => $deliverydate, 'collectionDate' => $collectiondate, 'paid' => 0]
                );

                if (!is_null($id)){
                    return $this->response->withItem($id, new  paymentTransformer());
                } else {
                    return $this->response->errorInternalError('Internal server error');
                }
                
            } else {
                return $this->response->errorNotFound('No bin hire');
            }

        } else {
            return $this->response->errorInternalError('Internal server error');
        }
    }
}
