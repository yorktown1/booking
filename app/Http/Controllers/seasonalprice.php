<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use App\tblorderservice;
use App\tblbinextraservices;
use App\tblextraservices;
use App\tblseasonalpricing;
use Validator;
use Redirect;
use Illuminate\Support\Facades\DB;

class seasonalprice extends Controller
{
	public function index(){
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$bintype = $this->getbintype();
		$binhire = null;
		$supplierData = $this->getSupplierData($idUser);
		if(is_null($supplierData)){
			return redirect()->route('details_service_zone');
		}
		
		$seasonal_supplier = null;
		$seasonal_wastetype = null;
		$special_days = $this->getdays();
		//supplierchild = $this->getsupplierchild($supplierData->idSupplier);
		return view('seasonalprice', ['binsize' => $getBinSize, 'bintype' => $bintype, 'binhire' => $binhire, 'supplierData' => $supplierData, 'seasonal_supplier' => $seasonal_supplier, 'seasonal_wastetype' => $seasonal_wastetype, 'special_days' => $special_days]);
	}
	
	private function getBinSize (){
		$sizes = DB::table('tblsize')
				->select('idSize','size', 'dimensions')
				->orderBy('idSize', 'asc')
				->get();
		return $sizes;
	}
	
	private function getbintype (){
		$types = DB::table('tblbintype')
				->select('idBinType','name', 'CodeType')
				->orderBy('idBinType', 'asc')
				->get();
		return $types;
	}
	
	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tbluser.idUser' => $idUser,
				'tblsupplier.seasonal_on' => '1'
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}
	
	private function getSupplierData_byID($idSupplier){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tblsupplier.idSupplier' => $idSupplier,
				'tblsupplier.seasonal_on' => '1'
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}
	
	private function getsupplierchild($idSupplier){
		$supplierchild = DB::table('tblsupplierchild')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tblsupplierchild.idParent')
			->select('tblsupplierchild.idSupplier', 'tblsupplierchild.name', 'tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phoneNumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress')
			->where([
				'tblsupplierchild.idParent' => $idSupplier, 
				'tblsupplierchild.seasonal_on' => '1'
			])
			->groupBy('tblsupplierchild.idSupplier', 'tblsupplierchild.name', 'tblsupplierchild.email', 'tblsupplierchild.email2', 'tblsupplierchild.phoneNumber',
				'tblsupplierchild.mobilePhone', 'tblsupplierchild.isOpenSaturday', 'tblsupplierchild.isOpenSunday', 'tblsupplierchild.customerServiceContact', 
				'tblsupplierchild.customerServicePhone','tblsupplierchild.customerServiceMobile', 'tblsupplierchild.fullAddress')
			->get();
		return $supplierchild;
	}
	
	private function getdays(){
		$special_days = DB::table('tblspecialdays')
			->select('tblspecialdays.idspecialdays', 'tblspecialdays.days')
			->get();
		return $special_days;
	}
	
	private function showseasonalprice($idSupplier, $idBinType){
		$seasonal_price = DB::table('tblbinservicespecialprice')
			->leftJoin('tblspecialdays', 'tblspecialdays.idspecialdays', '=', 'tblbinservicespecialprice.extrahireagedays')
			->leftJoin('tblsize', 'tblsize.idSize', '=', 'tblbinservicespecialprice.idBinSize')
			->leftJoin('tblbintype', 'tblbintype.idBinType', '=', 'tblbinservicespecialprice.idBinType')
			->select('tblbinservicespecialprice.idbinservicespecialprice', 'tblbinservicespecialprice.idSupplier', 'tblbinservicespecialprice.idBinType', 'tblbinservicespecialprice.idBinSize', 'tblbinservicespecialprice.price', 'tblbinservicespecialprice.extrahireagedays', 'tblbinservicespecialprice.idDays')
			->where([
				'tblbinservicespecialprice.idSupplier' => $idSupplier, 
				'tblbinservicespecialprice.idBinType' => $idBinType
			])
			->groupBy('tblbinservicespecialprice.idbinservicespecialprice', 'tblbinservicespecialprice.idSupplier', 'tblbinservicespecialprice.idBinType', 'tblbinservicespecialprice.idBinSize', 'tblbinservicespecialprice.price', 'tblbinservicespecialprice.extrahireagedays',  'tblbinservicespecialprice.idDays')
			->get();
		return $seasonal_price;
	}
	public function filter(Request $request){
		$seasonal_supplier = $request['seasonal_supplier'];
		$idSupplier = $request['seasonal_supplier'];
		//if(isset($request['seasonal_child'])){
		//	$seasonal_child = 1;
		//} else {
		//	seasonal_child = 0;
		//}
		$seasonal_wastetype = $request['seasonal_wastetype'];
		
		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'seasonal_supplier' => 'required|string',
				'seasonal_wastetype' => 'required|string',
			],[
				'seasonal_supplier.string' => 'Choose a proper supplier',
				'seasonal_wastetype.string' => 'Select a proper waste type',
			]);
			if($validate->fails()){
				return Redirect::route('seasonalprice')->withErrors($validate)->withInput();
			}else {
				$binhire = $this->showseasonalprice($idSupplier, $seasonal_wastetype);
				$supplierData = $this->getSupplierData_byID($idSupplier);
				$getBinSize = $this->getBinSize();
				$bintype = $this->getbintype();
				$special_days = $this->getdays();
				
				return view('seasonalprice', ['getBinSize' => $getBinSize, 'bintype' => $bintype, 'binhire' => $binhire, 'supplierData' => $supplierData, 'seasonal_supplier' => $seasonal_supplier, 'seasonal_wastetype' => $seasonal_wastetype, 'special_days' => $special_days]);
			}
		}
	}
	
	public function filtered(Request $request){
		$seasonal_supplier = $request['idSupplier'];
		$idSupplier = $request['idSupplier'];
		//if(isset($request['seasonal_child'])){
		//	$seasonal_child = 1;
		//} else {
		//	seasonal_child = 0;
		//}
		$seasonal_wastetype = $request['idBinType'];
		
		$binhire = $this->showseasonalprice($idSupplier, $seasonal_wastetype);
		$supplierData = $this->getSupplierData_byID($idSupplier);
		$getBinSize = $this->getBinSize();
		$bintype = $this->getbintype();
		$special_days = $this->getdays();
		
		return view('seasonalprice', ['getBinSize' => $getBinSize, 'bintype' => $bintype, 'binhire' => $binhire, 'supplierData' => $supplierData, 'seasonal_supplier' => $seasonal_supplier, 'seasonal_wastetype' => $seasonal_wastetype, 'special_days' => $special_days]);
	}
	
	public function edit(Request $request){
		$idSize = $request['idSize'];
		$idBinType = $request['idBinType'];
		$idSupplier = $request['idSupplier'];
		$binhire = $this->showseasonalprice($idSupplier, $idBinType);
		
		$supplierData = $this->getSupplierData_byID($idSupplier);
		$getBinSize = $this->getBinSize();
		$bintype = $this->getbintype();
		$special_days = $this->getdays();
		return view('seasonalpriceform', ['getBinSize' => $getBinSize, 'bintype' => $bintype, 'binhire' => $binhire, 'supplierData' => $supplierData, 'seasonal_supplier' => $idSupplier, 'seasonal_wastetype' => $idBinType, 'special_days' => $special_days, 'idSize' => $idSize, 'idBinType' => $idBinType, 'idSupplier' => $idSupplier]);
	}
	
	public function update(Request $request){
		$idSize = $request['seasonal_size'];
		$idBinType = $request['seasonal_wastetype'];
		$idSupplier = $request['seasonal_supplier'];
		
		$specialdays = $request['seasonal_days'];
		$specialprice = $request['price'];
		
		$pivot = array('specialdays' => $specialdays, 'specialprice'=>$specialprice);
		
		foreach($pivot['specialdays'] as $k=>$v){
			$binservice = DB::table('tblbinservice')
				->select('idBinService')
				->where([
					'idBinSize' => $idSize,
					'idBinType' => $idBinType,
					'idSupplier' => $idSupplier,
				])
				->first();
			if(!is_null($binservice)){
				$binservicespecial = DB::table('tblbinservicespecialprice')
				->select('idbinservicespecialprice')
				->where([
					'idBinService' => $binservice->idBinService,
					'idBinSize' => $idSize,
					'idBinType' => $idBinType,
					'idSupplier' => $idSupplier,
					'idDays' => $pivot['specialdays'][$k]
				])
				->first();
				if(!is_null($binservicespecial)){
					$tblseasonalpricing = new tblseasonalpricing();
					$update = $tblseasonalpricing::where([
						'idBinService' => $binservice->idBinService,
						'idBinSize' => $idSize,
						'idBinType' => $idBinType,
						'idSupplier' => $idSupplier,
						'idDays' => $pivot['specialdays'][$k]
					])
					->update(['price' => $pivot['specialprice'][$k]]);

					$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rate data');
				} else {
					$tblseasonalpricing = new tblseasonalpricing();
					$tblseasonalpricing->idBinService = $binservice->idBinService;
					$tblseasonalpricing->idSupplier = $idSupplier;
					$tblseasonalpricing->idBinType = $idBinType;
					$tblseasonalpricing->idBinSize = $idSize;
					$tblseasonalpricing->idDays = $pivot['specialdays'][$k];
					$tblseasonalpricing->price = $pivot['specialprice'][$k];
					$tblseasonalpricing->extrahireagedays = $pivot['specialdays'][$k];
					if ($tblseasonalpricing->save()){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rate data');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rate data failed. Please insert the main bin hire first.');
					}
				}
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Update rate data failed');
			}
		}
		return redirect()->route('filter_result_seasonalprice',['idSupplier' => $idSupplier, 'idBinType' => $idBinType]);
		//$binhire = $this->showseasonalprice($idSupplier, $idBinType);
		//
		//$supplierData = $this->getSupplierData_byID($idSupplier);
		//$getBinSize = $this->getBinSize();
		//$bintype = $this->getbintype();
		//$special_days = $this->getdays();
		//return view('seasonalpriceform', ['getBinSize' => $getBinSize, 'bintype' => $bintype, 'binhire' => $binhire, 'supplierData' => $supplierData, 'seasonal_supplier' => $idSupplier, 'seasonal_wastetype' => $idBinType, 'special_days' => $special_days, 'idSize' => $idSize, 'idBinType' => $idBinType, 'idSupplier' => $idSupplier]);
	}
}
