<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use App\tblorderservice;
use App\tblbinextraservices;
use App\tblextraservices;
use Validator;
use Redirect;
use Illuminate\Support\Facades\DB;

class CleanFills extends Controller
{
	public function index($result = null){
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$offset = time();

		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);


		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($supplierData->idSupplier);
		
		return view('clean_fills',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset,'serviceOptions' => $serviceOptions, 'nonDelivery' => $nonDelivery, 
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices]);
	}

	public function showResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('l d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($supplierData->idSupplier);
		
		return view('clean_fills',['supplierData' => $supplierData, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices]);
	}

	public function showForm(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$binservicebaseprice = null;
		$binservicediscprice = null;
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));

		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($supplierData->idSupplier);
		
		return view('clean_fills_form',['supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice, 'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices]);
	}

	public function showFormResult(Request $request){
		if (!is_null($request['status'])|| !is_null($request['message'])){
			$result['status'] = $request['status'];
			$result['message'] = $request['message'];
		} else {
			$result = null;
		}
		$bintype = $request['bintype'];
		$binsize = $request['binsize'];
		$idUser = 	session('idUser');
		$getBinSize = $this->getBinSize();
		$dateNow = date('d-m-Y', strtotime('now'));
		$binservicebaseprice = null;
		$binservicediscprice = null;
		if (!is_null($request['offset'])){
			$offset = strtotime($request['offset']);
		} else {
			$offset = time();
		}
		
		$supplierData = $this->getSupplierData($idUser);
		$serviceOptions = $this->getServiceOptions($supplierData->idUser, $supplierData->idSupplier);
		$nonDelivery = $this->getNonDeliveryDays($supplierData->idUser, $supplierData->idSupplier);
		$baseprice = $this->getbinservicebaseprice($supplierData->idSupplier);

		foreach ($baseprice as $k => $v){
			$binservicediscprice[$v->idBinService] = $this->getbinservicediscprice($v->idBinService);
			$binservicebaseprice[$v->idBinSize] = $this->getspecificbaseprice($supplierData->idSupplier,$v->idBinSize);
		}
		
		$extraservices = $this->getextraservices();
		$getbinextraservices = $this->getbinextraservices($supplierData->idSupplier);
		
		return view('clean_fills_form',['supplierData' => $supplierData, 'bintype' => $bintype,'binsize' => $binsize, 'getBinSize' => $getBinSize, 'dateNow' => $dateNow, 
			'offset' => $offset, 'serviceOptions' => $serviceOptions, 'result' => $result,'nonDelivery' => $nonDelivery,
			'baseprice' => $baseprice,'binservicebaseprice' => $binservicebaseprice, 'binservicediscprice' => $binservicediscprice,'extraservices' => $extraservices, 
			'getbinextraservices' => $getbinextraservices]);
	}

	private function getBinSize (){
		$sizes = DB::table('tblsize')
				->where(['idtype' => 3])
				->orderBy('idSize', 'asc')
				->get();
		return $sizes;
	}

	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}

	private function getServiceOptions($idUser, $idSupplier){
		$serviceOptions = DB::table('tblbinserviceoptions')
						->select('idUser', 'idSupplier', 'idBinType', 'extraHireagePrice', 'extraHireageDays', 'excessWeightPrice')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier,
							'idBinType' => 3
						])
						->first();
		return $serviceOptions;
	}

	private function getNonDeliveryDays($idUser, $idSupplier){
		$nonDelivery = DB::table('tblbinnondelivery')
					->select('date')
					->where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3
					])
					->get();
		return $nonDelivery;
	}

	private function getbinservicebaseprice($idSupplier){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize', 'tblbinservice.own')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinType' => 3,
							])
						->get();
		return $binserviceprice;
	}

	private function getbinservicediscprice($idBinService){
		$binserviceprice = DB::table('tblbinserviceupdates')
						->select('tblbinserviceupdates.price','tblbinserviceupdates.stock','tblbinserviceupdates.date', 'tblbinserviceupdates.own')
						->where([
							'tblbinserviceupdates.idBinService' => $idBinService,
							])
						->get();
		return $binserviceprice;
	}

	private function getspecificbaseprice($idSupplier, $idBinSize){
		$binserviceprice = DB::table('tblbinservice')
						->select('tblbinservice.idBinService', 'tblbinservice.idSupplier','tblbinservice.idBinService'
							,'tblbinservice.price AS baseprice','tblbinservice.stock AS basestock','tblbinservice.idBinSize', 'tblbinservice.own')
						->where([
							'tblbinservice.idSupplier' => $idSupplier,
							'tblbinservice.idBinSize' => $idBinSize,
							'tblbinservice.idBinType' => 3,
							])
						->get();
		return $binserviceprice;
	}
	
	private function getextraservices(){
		$extras = DB::table('tblextraservices')
				->select('tblextraservices.idExtraServices', 'tblextraservices.name', 'tblextraservices.code', 'tblextraservices.slug')
				->get();
		return $extras;
	}
	
	private function getbinextraservices($idSupplier){
		$extras = DB::table('tblbinserviceextra')
				->select('tblbinserviceextra.idBinServiceExtra', 'tblbinserviceextra.idBinType', 'tblbinserviceextra.idExtraService', 'tblbinserviceextra.idSupplier', 'tblbinserviceextra.charge')
				->where([
					'tblbinserviceextra.idBinType' => 3,
					'tblbinserviceextra.idSupplier' => $idSupplier
				])
				->get();
		return $extras;
	}
	
	private function validateForm($extraHireage, $extraHireageDays){
		$error = array();
		if ($extraHireage == ''){
			$error[] = 'Extra hireage must not empty';
		} 
		if ($extraHireageDays == ''){
			$error[] = 'Extra hireage days must not empty';
		} 
		return $error;
	}

	private function validatePricing($basePrice, $baseStock){
		$error = array();
		if ($basePrice == ''){
			$error[] = 'Please fill a specific base price for your bin';
		} 
		if ($baseStock == ''){
			$error[] = 'Please fill a specific stock for your bin';
		} 
		return $error;
	}

	public function editMiscDetail(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$extraHireage = $request['extraHireage'];
		$extraHireageDays = $request['extraHireageDays'];
		$excessWeight = $request['excessWeight'];
		if ($request->isMethod('post') == 'POST'){
			$validate = Validator::make($request->all(), [
				'extraHireage' => 'required|numeric',
				'extraHireageDays' => 'required|numeric',
				'excessWeight' => 'required|numeric',
			],[
				'extraHireage.numeric' => 'Extra hireage price should be numeric',
				'extraHireageDays.numeric' => 'Extra hireage days should be numeric',
				'excessWeight.numeric' => 'Excess weight amount should be numeric',
			]);
			if($validate->fails()){
				return Redirect::route('domestic_light')->withErrors($validate)->withInput();
			} else {
				$check = DB::table('tblbinserviceoptions')
						->select('idUser','idSupplier', 'idBinType')
						->where([
							'idUser' => $idUser,
							'idSupplier' => $idSupplier, 
							'idBinType' => 3
						])
						->first();
				if (!is_null($check)){
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions::where(['idUser' => $idUser, 'idSupplier' => $idSupplier, 'idBinType' => $idBinType])
					->update([
						'extraHireagePrice' => $extraHireage, 
						'extraHireageDays' => $extraHireageDays, 
						'excessWeightPrice' => $excessWeight
					]);
					if ($serviceOptions){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rates optional data');
						return redirect()->route('domestic_light');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rates optional data failed');
						return redirect()->route('domestic_light');
					}
				} else {
					$serviceOptions = new tblbinserviceoptions();
					$serviceOptions->idUser = $idUser;
					$serviceOptions->idSupplier = $idSupplier;
					$serviceOptions->idBinType = 3;
					$serviceOptions->extraHireagePrice = $extraHireage;
					$serviceOptions->extraHireageDays = $extraHireageDays;
					$serviceOptions->excessWeightPrice = $excessWeight;
					if ($serviceOptions->save()){
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', 'Success updating rates optional data');
						return redirect()->route('domestic_light');
					} else {
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', 'Update rates optional data failed');
						return redirect()->route('domestic_light');
					}
				}
			}
		}
	}
	
	public function editBinServicesExtra(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$extras_code[] = $request['extras_hidden'];
		$extras[] = $request['extras'];
		
		$extras_pivot = array('idExtraServices' => $extras_code, 'charge' => $extras);
		
		/** check for its existence, if exists override, if its not just add **/
		/** start the form function **/
		if ($request->isMethod('post') == 'POST'){
			
			/** validate extra field **/ 
			$validate = Validator::make($request->all(), [
				"extras_hidden"    => "required|array|min:1",
				'extras_hidden.*' => 'required|numeric',
				"extras"    => "required|array|min:1",
				'extras.*' => 'required|numeric',
			]);
			
			if($validate->fails()){
				return Redirect::route('domestic_light')->withErrors($validate)->withInput();
			} else {
				/** start the loop over the extra field **/ 
				foreach($extras_pivot['idExtraServices'] as $k=>$v){
					foreach($v  as $r => $d){
						$check = DB::table('tblbinserviceextra')
						->select('idSupplier', 'idBinType', 'idExtraService', 'charge')
						->where([
							'idSupplier' => $idSupplier,
							'idBinType' => 3,
							'idExtraService' => $extras_pivot['idExtraServices'][$k][$r]
						])
						->first();
						print_r($check);
						if (!is_null($check)){
							$tblbinextraservices = new tblbinextraservices();
							$tblbinextraservices::where([
								'idSupplier' => $idSupplier,
								'idBinType' => 3,
								'idExtraService' => $extras_pivot['idExtraServices'][$k][$r]
								])
								->update(['charge' => $extras_pivot['charge'][$k][$r]]);
							$request->session()->flash('status', 'success');
							$request->session()->flash('message', 'Success extra charge optional data');
							
						} else {
							$tblbinextraservices = new tblbinextraservices();
							
							$tblbinextraservices->idBinType = 3;
							$tblbinextraservices->idExtraService =  $extras_pivot['idExtraServices'][$k][$r];
							$tblbinextraservices->idSupplier = $idSupplier;
							$tblbinextraservices->charge = $extras_pivot['charge'][$k][$r];
							$tblbinextraservices->save();
							$request->session()->flash('status', 'success');
							$request->session()->flash('message', 'Success extra charge optional data');
						}
					}
				}
				
				return redirect()->route('domestic_light');
			}
		}
	}
	
	public function editNonDeliveryDays(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$date[] = $request['date'];
		$nonDelivery[] = $request['nondelivery'];

		$nondeliverydays = array('date' => $date, 'nondelivery' => $nonDelivery);
		
		$check_child = DB::table('tblsupplierchild')
				->select('idParent', 'idSupplier')
				->where([
					'idParent' => $idSupplier
				])
				->get();
		
		
		/** Delete first **/
		$tblbinnondelivery = new tblbinnondelivery();
		//check for deletion first
		if (is_null($nondeliverydays['nondelivery'][0])){
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					
					//PARENT
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
					}

					if($deletedRows){
						//**CHILD**/ 
						if(!is_null($check_child)){
							foreach($check_child as $d=>$val){
								$check = DB::table('tblbinnondelivery')
								->select('date')
								->where([
									'idSupplier' => $val->idSupplier,
									'idUser' => $idUser,
									'idBinType' => 3,
									'date' => $nondeliverydays['date'][$k][$r]
								])
								->get();
							
								if (!is_null($check)){
								$deletedRows = tblbinnondelivery::where([
									'idSupplier' =>$val->idSupplier,
									'idUser' => $idUser,
									'idBinType' => 3,
									'date' =>  $nondeliverydays['date'][$k][$r]
									])->delete();
								}
								
								if($deletedRows){
									$result['status'] = 'success';
									$result['message'] = 'Success updating non delivery data';
								}else{
									$result['status'] = 'notif';
									$result['message'] = 'Update non delivery days failed';
								}
							}
						}
						$result['status'] = 'success';
						$result['message'] = 'Success updating non delivery data';
					} else {
						$result['status'] = 'notif';
						$result['message'] = 'Update non delivery days failed';
					}
					
					
				}
			}
		} else{
			foreach($nondeliverydays['date'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
						->select('date')
						->where([
							'idSupplier' => $idSupplier,
							'idUser' => $idUser,
							'idBinType' => 3,
							'date' => $nondeliverydays['date'][$k][$r]
						])
						->get();
						
					if (!is_null($check)){
						$deletedRows = tblbinnondelivery::where([
						'idSupplier' => $idSupplier,
						'idUser' => $idUser,
						'idBinType' => 3,
						'date' =>  $nondeliverydays['date'][$k][$r]
						])->delete();
						
						//**CHILD**/ 
						if(!is_null($check_child)){
							foreach($check_child as $d=>$val){
								$check = DB::table('tblbinnondelivery')
								->select('date')
								->where([
									'idSupplier' => $val->idSupplier,
									'idUser' => $idUser,
									'idBinType' => 3,
									'date' => $nondeliverydays['date'][$k][$r]
								])
								->get();
							
								if (!is_null($check)){
								$deletedRows = tblbinnondelivery::where([
									'idSupplier' =>$val->idSupplier,
									'idUser' => $idUser,
									'idBinType' => 3,
									'date' =>  $nondeliverydays['date'][$k][$r]
									])->delete();
								}
							}
						}
					}
					
				}
			}
			
			/** Then insert **/
			foreach($nondeliverydays['nondelivery'] as $k => $v){
				foreach($v as $r => $d){
					$check = DB::table('tblbinnondelivery')
							->select('date')
							->where([
								'idSupplier' => $idSupplier,
								'idUser' => $idUser,
								'idBinType' => 3,
								'date' => $nondeliverydays['date'][$k][$r]
							])
							->first();
					if (!is_null($check)){
						$result['status'] = 'warning';
						$result['message'] = 'Date exists';
					} else{
						$tblbinnondelivery = new tblbinnondelivery();
						$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
						$tblbinnondelivery->idSupplier = $idSupplier;
						$tblbinnondelivery->idUser = $idUser;
						$tblbinnondelivery->idBinType = 3;
						if($tblbinnondelivery->save()){
							//**CHILD**/ 
							if(!is_null($check_child)){
								foreach($check_child as $d=>$val){
									$check = DB::table('tblbinnondelivery')
									->select('date')
									->where([
										'idSupplier' => $val->idSupplier,
										'idUser' => $idUser,
										'idBinType' => 3,
										'date' => $nondeliverydays['date'][$k][$r]
									])
									->get();
								
									if (!is_null($check)){
										$tblbinnondelivery = new tblbinnondelivery();
										$tblbinnondelivery->date = $nondeliverydays['date'][$k][$r];
										$tblbinnondelivery->idSupplier = $val->idSupplier;
										$tblbinnondelivery->idUser = $idUser;
										$tblbinnondelivery->idBinType = 3;
										if($tblbinnondelivery->save()){
											$result['status'] = 'success';
											$result['message'] = 'Success updating non delivery data';
										} else {
											$result['status'] = 'notif';
											$result['message'] = 'Update non delivery days failed';
										}
									}
								}
							}
							$result['status'] = 'success';
							$result['message'] = 'Success updating non delivery data';
							
						} else {
							$result['status'] = 'notif';
							$result['message'] = 'Update non delivery days failed';
						}
						
					}
				}
			}
			
		}

		return redirect()->route('domestic.light', ['status' => $result['status'], 'message' => $result['message']]);
	}

	public function editBinServicePrice(Request $request){
		$idUser = $request['idUser'];
		$idSupplier = $request['idSupplier'];
		$idBinType = $request['idBinType'];
		$idBinSize = $request['idBinSize'];
		
		$basePriceAdultHidden = $request['basePriceAdultHidden'];
		$basePriceAdult = $request['basePriceAdult'];
		$binPriceAdultHidden[] =  $request['binPriceAdultHidden'];
		$binPriceAdult[] =  $request['binPriceAdult'];
		
		$basePriceChildHidden = $request['basePriceChildHidden'];
		$basePriceChild = $request['basePriceChild'];
		$binPriceChildHidden[] = $request['binPriceChildHidden'];
		$binPriceChild[] = $request['binPriceChild'];
		
		$basePriceInfantsHidden = $request['basePriceInfantsHidden'];
		$basePriceInfants = $request['basePriceInfants'];
		$binPriceInfantsHidden[] = $request['binPriceInfantsHidden'];
		$binPriceInfants[] = $request['binPriceInfants'];
		
		$date[] = $request['date'];
		//$binPrice[] = $request['binPrice'];

		/* Main pricing data */
		$mainData_1 = array(
			'date' => $date,
			'binPriceAdultHidden' => $binPriceAdultHidden,
			'binPriceAdult' => $binPriceAdult,
		);
		
		$mainData_2 = array(
			'date' => $date,
			'binPriceChildHidden' => $binPriceChildHidden, 
			'binPriceChild' => $binPriceChild,
		);
		
		$mainData_3 = array(
			'date' => $date,
			'binPriceInfantsHidden' => $basePriceInfantsHidden, 
			'binPriceInfants' => $binPriceInfants
		);
		
		if ($request->isMethod('POST') == 'POST'){
			/****** 
			* check adult base price 
			*****/
			$check_adult = DB::table('tblbinservice')
				->where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 1
				])
				->first();
               
			if (!is_null($check_adult)){
				$validate = Validator::make($request->all(), [
					'basePriceAdult' => 'required|numeric',
				],[
					'basePriceAdult.numeric' => 'Rate must be numeric',
				]);
				
				if($validate->fails()){
					return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				$binservice = new tblbinservice();
               
				$updates = $binservice::where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 1
				])
				->update(['price' => $basePriceAdult, 'stock' => 1]);
               
				$check_updates_adult = DB::table('tblbinservice')
					->where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType,
						'price' => $basePriceAdult,
						'stock' => 1, 
						'own' => 1
					])
					->first();
					
				if(!is_null($check_updates_adult)){
					$tempResultBasePrice = 'success';
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating rate data');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update adult rate data failed');
					return redirect()->route('domestic_light');
				}
               
				/*fetch last id */
				$lastid = DB::table('tblbinservice')
				->where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 1
				])
				->first();
               
			} else {
				$validate = Validator::make($request->all(), [
					'basePriceAdult' => 'required|numeric',
				],[
					'basePriceAdult.numeric' => 'The bin base price should be on numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				$binservice = new tblbinservice();
				$binservice->idSupplier = $idSupplier;
				$binservice->idBinType = $idBinType;
				$binservice->idBinSize = $idBinSize;
				$binservice->price = $basePriceAdult;
				$binservice->stock = 1;
				$binservice->unqueriable = 0;
				$binservice->own = 1;
               
				if ($binservice->save()){
					$tempResultBasePrice = 'success';
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating rate  data');
					/*fetch last id */
					
					$lastid = DB::table('tblbinservice')
						->orderBy('idBinService', 'desc')
						->first();
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update adult rate data failed');
					return redirect()->route('domestic_light');
				}
			}
			
			
			/* validate numeric discount value */
			foreach ($mainData_1['binPriceAdult'] as $k => $v){
				foreach($v  as $d){
					$array_binprice[] = $d;
				}
				
			}
			
			/* price */
			$validate = Validator::make(compact('array_binprice'), [
				'array_binprice' => 'array',
				'array_binprice.*' => 'numeric',
			],[
				'array_binprice.*.numeric' => 'The rate * should has numeric value',
			]);
			
			if($validate->fails()){
				return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
			}
			   
			/* check and update the discount on specific date*/
			if (!is_null($mainData_1['binPriceAdult'])){
				foreach($mainData_1['binPriceAdult'] as $k => $v){
					foreach($v as $r => $d){
						if(!is_null($mainData_1['binPriceAdult'][$k][$r])){
							$checkDisc = DB::table('tblbinserviceupdates')
								->where([
									'idBinService' => $lastid->idBinService,
									'date' => $mainData_1['date'][$k][$r],
									'own' => 1
								])
							->first();
               
							if (!is_null($checkDisc)){
								$binserviceupdates = new tblbinserviceupdates();
								$update = $binserviceupdates::where([
									'idBinService' => $lastid->idBinService,
									'date' => $mainData_1['date'][$k][$r],
									'own' => 1
								])
								->update(['price' => $mainData_1['binPriceAdult'][$k][$r], 'stock' => 1]);
               
								if ($update) {
									$request->session()->flash('status', 'success');
									$request->session()->flash('message', 'Success updating rate data');
								
								} else {
									$request->session()->flash('status', 'danger');
									$request->session()->flash('message', 'Update rate data failed');
								}
							} else {
								if(!is_null($check_adult)){
									if(($check_adult->price != $mainData_1['binPriceAdult'][$k][$r])){
										$binserviceupdates = new tblbinserviceupdates();
										$binserviceupdates->idBinService = $lastid->idBinService;
										$binserviceupdates->price = $mainData_1['binPriceAdult'][$k][$r];
										$binserviceupdates->stock = 1;
										$binserviceupdates->date = $mainData_1['date'][$k][$r];
										$binserviceupdates->own = 1;
										
										if ($binserviceupdates->save()){
											$request->session()->flash('status', 'success');
											$request->session()->flash('message', 'Success updating rate data');
										} else {
											$request->session()->flash('status', 'danger');
											$request->session()->flash('message', 'Update rate data failed');
										}
									} else {
										$request->session()->flash('status', 'success');
										$request->session()->flash('message', 'Success updating rate data');
									}
								}
							}
						}
					}
				}
			}
               
			if ($tempResultBasePrice == 'success'){
				$result['status'] = 'success';
				$result['message'] = 'Success updating rates data';
			} else {
				$result['status'] = 'notif';
				$result['message'] = 'Update rates data failed';
				
			}
			
			
			/****** 
			* check child base price 
			*****/
			$check_child = DB::table('tblbinservice')
				->where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 2
				])
				->first();
               
			if (!is_null($check_adult)){
				$validate = Validator::make($request->all(), [
					'basePriceChild' => 'required|numeric',
				],[
					'basePriceChild.numeric' => 'Rate must be numeric',
				]);
				
				if($validate->fails()){
					return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				$binservice = new tblbinservice();
               
				$updates = $binservice::where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 2
				])
				->update(['price' => $basePriceChild, 'stock' => 1]);
               
				$check_updates_adult = DB::table('tblbinservice')
					->where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType,
						'price' => $basePriceChild,
						'stock' => 1, 
						'own' => 2
					])
					->first();
					
				if(!is_null($check_updates_adult)){
					$tempResultBasePrice = 'success';
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating rate data');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update adult rate data failed');
					return redirect()->route('domestic_light');
				}
               
				/*fetch last id */
				$lastid = DB::table('tblbinservice')
				->where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 2
				])
				->first();
               
			} else {
				$validate = Validator::make($request->all(), [
					'basePriceChild' => 'required|numeric',
				],[
					'basePriceChild.numeric' => 'The bin base price should be on numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				$binservice = new tblbinservice();
				$binservice->idSupplier = $idSupplier;
				$binservice->idBinType = $idBinType;
				$binservice->idBinSize = $idBinSize;
				$binservice->price = $basePriceChild;
				$binservice->stock = 1;
				$binservice->unqueriable = 0;
				$binservice->own = 2;
               
				if ($binservice->save()){
					$tempResultBasePrice = 'success';
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating rate  data');
					/*fetch last id */
					
					$lastid = DB::table('tblbinservice')
						->orderBy('idBinService', 'desc')
						->first();
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update child rate data failed');
					return redirect()->route('domestic_light');
				}
			}
			
			
			/* validate numeric discount value */
			foreach ($mainData_2['binPriceChild'] as $k => $v){
				foreach($v  as $d){
					$array_binprice[] = $d;
				}
				
			}
			
			/* price */
			$validate = Validator::make(compact('array_binprice'), [
				'array_binprice' => 'array',
				'array_binprice.*' => 'numeric',
			],[
				'array_binprice.*.numeric' => 'The rate * should has numeric value',
			]);
			
			if($validate->fails()){
				return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
			}
			   
			/* check and update the discount on specific date*/
			if (!is_null($mainData_2['binPriceChild'])){
				foreach($mainData_2['binPriceChild'] as $k => $v){
					foreach($v as $r => $d){
						if(!is_null($mainData_2['binPriceChild'][$k][$r])){
							$checkDisc = DB::table('tblbinserviceupdates')
								->where([
									'idBinService' => $lastid->idBinService,
									'date' => $mainData_2['date'][$k][$r],
									'own' => 2
								])
							->first();
               
							if (!is_null($checkDisc)){
								$binserviceupdates = new tblbinserviceupdates();
								$update = $binserviceupdates::where([
									'idBinService' => $lastid->idBinService,
									'date' => $mainData_2['date'][$k][$r],
									'own' => 2
								])
								->update(['price' => $mainData_2['binPriceChild'][$k][$r], 'stock' => 1]);
               
								if ($update) {
									$request->session()->flash('status', 'success');
									$request->session()->flash('message', 'Success updating rate data');
								
								} else {
									$request->session()->flash('status', 'danger');
									$request->session()->flash('message', 'Update child rate data failed');
								}
							} else {
								if(!is_null($check_child)){
									if(($check_child->price != $mainData_2['binPriceChild'][$k][$r])){
										$binserviceupdates = new tblbinserviceupdates();
										$binserviceupdates->idBinService = $lastid->idBinService;
										$binserviceupdates->price = $mainData_2['binPriceChild'][$k][$r];
										$binserviceupdates->stock = 1;
										$binserviceupdates->date = $mainData_2['date'][$k][$r];
										$binserviceupdates->own = 2;
										
										if ($binserviceupdates->save()){
											$request->session()->flash('status', 'success');
											$request->session()->flash('message', 'Success updating rate data');
										} else {
											$request->session()->flash('status', 'danger');
											$request->session()->flash('message', 'Update child rate data failed');
										}
									} else {
										$request->session()->flash('status', 'success');
										$request->session()->flash('message', 'Success updating rate data');
									}
								}
							}
						}
					}
				}
			}
               
			if ($tempResultBasePrice == 'success'){
				$result['status'] = 'success';
				$result['message'] = 'Success updating rates data';
			} else {
				$result['status'] = 'notif';
				$result['message'] = 'Update rates data failed';
				
			}
			
			
			/****** 
			* check infants base price 
			*****/
			$check_infants = DB::table('tblbinservice')
				->where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 3
				])
				->first();
               
			if (!is_null($check_infants)){
				$validate = Validator::make($request->all(), [
					'basePriceInfants' => 'required|numeric',
				],[
					'basePriceInfants.numeric' => 'Rate must be numeric',
				]);
				
				if($validate->fails()){
					return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				$binservice = new tblbinservice();
               
				$updates = $binservice::where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 3
				])
				->update(['price' => $basePriceInfants, 'stock' => 1]);
               
				$check_updates_adult = DB::table('tblbinservice')
					->where([
						'idSupplier' => $idSupplier,
						'idBinSize' => $idBinSize,
						'idBinType' => $idBinType,
						'price' => $basePriceInfants,
						'stock' => 1, 
						'own' => 3
					])
					->first();
					
				if(!is_null($check_updates_adult)){
					$tempResultBasePrice = 'success';
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating rate data');
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update infants rate data failed');
					return redirect()->route('domestic_light');
				}
               
				/*fetch last id */
				$lastid = DB::table('tblbinservice')
				->where([
					'idSupplier' => $idSupplier,
					'idBinSize' => $idBinSize,
					'idBinType' => $idBinType,
					'own' => 3
				])
				->first();
               
			} else {
				$validate = Validator::make($request->all(), [
					'basePriceInfants' => 'required|numeric',
				],[
					'basePriceInfants.numeric' => 'The bin base price should be on numeric value',
				]);
				if($validate->fails()){
					return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
				}
				
				$binservice = new tblbinservice();
				$binservice->idSupplier = $idSupplier;
				$binservice->idBinType = $idBinType;
				$binservice->idBinSize = $idBinSize;
				$binservice->price = $basePriceInfants;
				$binservice->stock = 1;
				$binservice->unqueriable = 0;
				$binservice->own = 3;
               
				if ($binservice->save()){
					$tempResultBasePrice = 'success';
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating rate  data');
					/*fetch last id */
					
					$lastid = DB::table('tblbinservice')
						->orderBy('idBinService', 'desc')
						->first();
				} else {
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Update infants rate data failed');
					return redirect()->route('domestic_light');
				}
			}
			
			
			/* validate numeric discount value */
			foreach ($mainData_3['binPriceInfants'] as $k => $v){
				foreach($v  as $d){
					$array_binprice[] = $d;
				}
				
			}
			
			/* price */
			$validate = Validator::make(compact('array_binprice'), [
				'array_binprice' => 'array',
				'array_binprice.*' => 'numeric',
			],[
				'array_binprice.*.numeric' => 'The rate * should has numeric value',
			]);
			
			if($validate->fails()){
				return Redirect::route('domestic.light.waste.pricing.form', ['bintype' => $idBinType, 'binsize' => $idBinSize])->withErrors($validate)->withInput();
			}
			   
			/* check and update the discount on specific date*/
			if (!is_null($mainData_3['binPriceInfants'])){
				foreach($mainData_3['binPriceInfants'] as $k => $v){
					foreach($v as $r => $d){
						if(!is_null($mainData_3['binPriceInfants'][$k][$r])){
							$checkDisc = DB::table('tblbinserviceupdates')
								->where([
									'idBinService' => $lastid->idBinService,
									'date' => $mainData_3['date'][$k][$r],
									'own' => 3
								])
							->first();
               
							if (!is_null($checkDisc)){
								$binserviceupdates = new tblbinserviceupdates();
								$update = $binserviceupdates::where([
									'idBinService' => $lastid->idBinService,
									'date' => $mainData_3['date'][$k][$r],
									'own' => 3
								])
								->update(['price' => $mainData_3['binPriceInfants'][$k][$r], 'stock' => 1]);
               
								if ($update) {
									$request->session()->flash('status', 'success');
									$request->session()->flash('message', 'Success updating rate data');
								
								} else {
									$request->session()->flash('status', 'danger');
									$request->session()->flash('message', 'Update infants rate data failed');
								}
							} else {
								if(!is_null($check_infants)){
									if(($check_infants->price != $mainData_3['binPriceInfants'][$k][$r])){
										$binserviceupdates = new tblbinserviceupdates();
										$binserviceupdates->idBinService = $lastid->idBinService;
										$binserviceupdates->price = $mainData_3['binPriceInfants'][$k][$r];
										$binserviceupdates->stock = 1;
										$binserviceupdates->date = $mainData_3['date'][$k][$r];
										$binserviceupdates->own = 3;
										
										if ($binserviceupdates->save()){
											$request->session()->flash('status', 'success');
											$request->session()->flash('message', 'Success updating rate data');
										} else {
											$request->session()->flash('status', 'danger');
											$request->session()->flash('message', 'Update infants rate data failed');
										}
									} else {
										$request->session()->flash('status', 'success');
										$request->session()->flash('message', 'Success updating rate data');
									}
								}
							}
						}
					}
				}
			}
               
			if ($tempResultBasePrice == 'success'){
				$result['status'] = 'success';
				$result['message'] = 'Success updating rates data';
			} else {
				$result['status'] = 'notif';
				$result['message'] = 'Update rates data failed';
				
			}
			
			return redirect()->route('domestic_light');
		}
	}

	public function resetRates(Request $request){
		$idUser = session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$idBinType = $request['bintype'];
		$idBinSize = $request['binsize'];

		$binservice = new tblbinservice();
		$binserviceupdates = new tblbinserviceupdates();
		$orderservice = new tblorderservice();

		$getbinservice = DB::table('tblbinservice')
						->select('idBinService')
						->where([
							'idSupplier' => $supplierData->idSupplier,
							'idBinSize' => $idBinSize,
							'idBinType' => $idBinType
						])
						->first();
		if (!is_null($getbinservice)){
			$deletedRowsBinUpdates = $binserviceupdates::where([
			'idBinService' => $getbinservice->idBinService
			])->delete();
			
			//$deletedRowsBinOrder = $orderservice::where([
			//'idBinService' => $getbinservice->idBinService
			//])->delete();
			$idSupplier = strval($supplierData->idSupplier);
			$deletedRowsBin = $binservice::where([
				'idSupplier' => $idSupplier,
				'idBinSize' => $idBinSize,
				'idBinType' => $idBinType
			])->update(['price' => 0, 'stock' => 0]);

			if ($deletedRowsBin){
				$request->session()->flash('status', 'success');
				$request->session()->flash('message', 'Success deleting rates data');
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Delete rates data failed');
			}

		} else {
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', 'No data selected');
		}
		return redirect()->route('domestic_light');
	}
}
