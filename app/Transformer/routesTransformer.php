<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class routesTransformer extends TransformerAbstract {
 
    public function transform($route) {
		return [
			'idplace' => $route->idplace,
			'place' => $route->place,
			'idtype' => $route->idtype,
		];
    }
 }