<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class binhireTransformer extends TransformerAbstract {
 
    public function transform($binhire) {
        return [
            'idBinService' => $binhire->idBinService,
            'price' => $binhire->price,
            'stock' => $binhire->stock,
            'idSupplier' => $binhire->idSupplier,
            'idBinType' => $binhire->idBinType,
            'idBinSize' => $binhire->idBinSize,
			'size' => $binhire->size, 
			'from' => $binhire->from, 
			'to' => $binhire->to, 
        ];
    }
 }