<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class extraservicesTransformer extends TransformerAbstract {
 
    public function transform($extras) {
        return [
            'idExtraServices' => $extras->idExtraServices,
            'name' => $extras->name,
            'code' => $extras->code,
            'slug' => $extras->slug,
        ];
    }
 }