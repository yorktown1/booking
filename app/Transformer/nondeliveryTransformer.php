<?php 
namespace App\Transformer;
use League\Fractal\TransformerAbstract;

class nondeliveryTransformer extends TransformerAbstract {
	public function transform($nondeliverydays) {
		return [
			'idNonDeliveryDays' => $nondeliverydays->idNonDeliveryDays,
			'date' => $nondeliverydays->date,
			'idSupplier' => $nondeliverydays->idSupplier,
			'idBinType' => $nondeliverydays->idBinType,
		];
	}
 }