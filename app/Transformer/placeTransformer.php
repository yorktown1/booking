<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class placeTransformer extends TransformerAbstract {
 
    public function transform($place) {
		return [
			'idplace' => $place->idplace,
			'place' => $place->place,
		];
    }
 }