<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class stateTransformer extends TransformerAbstract {
 
    public function transform($state) {
        return [
            'zipcode' => $state->zipcode,
            'area' => $state->area,
            'parentareacode' => $state->parentareacode,
            'idArea' => $state->idArea,
        ];
    }
 }