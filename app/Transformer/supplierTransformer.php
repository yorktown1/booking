<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class supplierTransformer extends TransformerAbstract {
 
    public function transform($supplier) {
		if(!is_null($supplier->stocking_off) && !is_null($supplier->seasonal_on)){
			 return [
            'isOpenSaturday' => $supplier->isOpenSaturday,
            'isOpenSunday' => $supplier->isOpenSunday,
			'stocking_off' => $supplier->stocking_off,
			'seasonal_on' => $supplier->seasonal_on,
			];
		} else {
			return [
            'isOpenSaturday' => $supplier->isOpenSaturday,
            'isOpenSunday' => $supplier->isOpenSunday,
			'stocking_off' => 0,
			'seasonal_on' => 0
			];
		}
       
    }
 }