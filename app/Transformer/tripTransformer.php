<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class tripTransformer extends TransformerAbstract {
 
    public function transform($trip) {
		return [
			'size' => $trip->size,
			'idBinSize' => $trip->idBinSize,
			'idBinService' => $trip->idBinService,
			'own' => $trip->own,
			'price' => $trip->price,
			'from' => $trip->from,
			'to' => $trip->to,
			'idtype' => $trip->idtype,
		];
    }
 }