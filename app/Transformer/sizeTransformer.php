<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class sizeTransformer extends TransformerAbstract {
 
    public function transform($size) {
        return [
            'idSize' => $size->idSize,
            'size' => $size->size,
            'dimensions' => $size->dimensions, 
			'idtype' => $size->idtype,
			'from' => $size->from,
			'to'=> $size->to,
			'to'=> $size->time,
        ];
    }
 }