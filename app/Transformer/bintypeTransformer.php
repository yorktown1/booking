<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class bintypeTransformer extends TransformerAbstract {
 
    public function transform($bintype) {
        return [
            'idBinType' => $bintype->idBinType,
            'name' => $bintype->name,
            'code' => $bintype->CodeType,
            'description' => $bintype->description,
            'description2' => $bintype->description2
        ];
    }
 }