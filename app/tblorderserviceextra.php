<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblorderserviceextra extends Model
{
	protected $table = 'tblorderserviceextra';
	public $timestamps = false;
}
