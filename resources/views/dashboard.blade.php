@extends('template')
@section('content')
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<h1 class="main-title float-left">Instructions & Information</h1>
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						
						@if (!empty(session('access_violated_status')))
							@if (session('access_violated_status') == 'danger')
								<div class="alert alert-danger" role="alert">
									{{session('access_violated_message')}} <br />
								</div>
							@endif
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="card mb-3">
							<div class="card-header">
								<h3><i class="fa fa-bell-o"></i> Welcome, {{session('username')}} !</h3>
							</div>
					
							<div class="card-body">
								<strong>Initial Setup:</strong>
								<ol>
									<li style="list-style-type:decimal">
										<p>Click on the “Suppliers Details” page – please check that all details are correct.</p>
										<p class="text-danger"><strong>Note: You can nominate up to 2 emails that the job booking will be sent to.</strong></p>
									</li>
									<li style="list-style-type:decimal">
										<p>Scroll down to "Saturday & Sunday Deliveries" - if you provide a tickets service on Saturdays and/or Sundays mark the box.</p>
										<p>If the box is left empty the program will assume that you are not able to deliver skip bins on Saturdays and/or Sundays.</p>
										
										<p>Save any changes as you go.</p>
									</li>
									
								</ol>
								
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection
