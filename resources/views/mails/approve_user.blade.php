@extends('login_template')
@section('login_content')
		<style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3>Congratulations ! Your account has been activated</h3><br />
							<p>Hi {{ $supplierData->contactName }},</p>
							<p>You are ready to roll on the system. You registered with these details:</p>
							<p><strong>Username : {{$supplierData->username}} </strong></p>
							<p><strong>Password : <em>Your chosen password</em> </strong></p>
							<p><strong>Primary Email Address : {{$supplierData->email}}</strong></p>
							<p>Please log in and complete your details and your skip bin hire scheduling. Have fun!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
