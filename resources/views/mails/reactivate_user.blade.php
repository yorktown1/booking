@extends('login_template')
@section('login_content')
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3 class="float-left">Welcome back! Your account just has been re-activated</h3><br />
							<p>Hi {{ $supplierData->contactName }},</p>
							<p>According to our final decision, your account has just re-activated. Your previous data remains on our database without any alteration. </p>
							<p>Please note that following our policy is mandatory. So, do your best to respect it. Have fun!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
