@extends('login_template')
@section('login_content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
				background-color:#fff;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
			
			.bin_desc ul li{
				color:#c40005;
			}
			ul li{
				padding-left:0;
			}
		</style>
		<div class="content" style="border:1px solid #000;padding:30px">
			<div class="container">
				<div class="content-wrapper" >
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="card mb-3">
								<div class="card-body">	
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table" border="0" width="100%" cellpadding="3">
														<tr>
															<td style="text-align:left">
																
																<p style="text-transform:uppercase;"><strong>This email is your Tax Invoice #{{$invoiceDetails->paymentUniqueCode}} </strong> </p>
																<p style="text-transform:uppercase;color:#c40005;">Use these details to contact your supplier directly to arrange specific delivery.</p>
																@if(!is_null($supplierdetails))
																	@if(!is_null($supplierdetails->seasonal_title))
																		<p style="text-transform:uppercase;">{{$supplierdetails->seasonal_title}}</p>
																	@endif
																@endif
															</td>
														</tr>
													</table>
												</div>

												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">1. Order details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Order date</strong>
															</td>
															<td >
																<?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Bin type</strong>
															</td>
															<td >
																{{$binhire->name}} <br />
																{{$binhire->size}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Description</strong>
															</td>
															<td  >
																<?php $tags = array("strong", "b");?>
																<p><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
																<div style="color:#c40005;" class="bin_desc"><?php echo $binhire->description; ?></div>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Bin additional info</strong>
															</td>
															<td >
																<?php if(!is_null($binhireoptions)):?>
																	<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
																		<p  >First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </p>
																	<?php else :?>
																		<p>-</p>
																	<?php endif;?>
																<?php endif;?>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Delivery date</strong>
															</td>
															<td >
																<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Collection date</strong>
															</td>
															<td >
																<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
															</td>
														</tr>
														
													</table>
												</div>
												<hr />
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">2. Supplier details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Business name</strong>
															</td>
															<td >
																Coastal Waste
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Contact name</strong>
															</td>
															<td  >
																Coastal Waste
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Business hours phone number</strong>
															</td>
															<td >
																(08) 9592 9420
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Email</strong>
															</td>
															<td >
																<i>info@coastalwaste.com.au</i>
															</td>
														</tr>
													</table>
												</div>
												<hr />
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">3. Customer delivery details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Name</strong>
															</td>
															<td >
																{{$customerdetails->name}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Address</strong>
															</td>
															<td  >
																@if(!is_null($customerdetails->unit_lot))
																	Unit / Lot : {{$customerdetails->unit_lot}}<br /> 
																	Street Name : {{$customerdetails->address}} 
																@else
																	{{$customerdetails->address}} 
																@endif
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Zipcode</strong>
															</td>
															<td >
																{{$customerdetails->zipcode}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Phone</strong>
															</td>
															<td >
																{{$customerdetails->phone}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Email</strong>
															</td>
															<td >
																{{$customerdetails->email}}
															</td>
														</tr>
														@if(!is_null($invoiceDetails->deliveryComments))
														<tr>
															<td width="30%">
																<strong >Bin Placement</strong>
															</td>
															<td >
																{{$invoiceDetails->deliveryComments}}
															</td>
														</tr>
														@endif
													</table>
												</div>
												<hr />
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">4. Payment confirmation</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Note</strong>
															</td>
															<td  >
																@if(!is_null($seasonal_mode))
																	@if($seasonal_mode == '1')
																		{{'Winter 2019 Special Pricing'}}
																	@else
																		{{'Normal Pricing'}}
																	@endif
																@else
																	{{'Normal Pricing'}}
																@endif
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Bin hire price</strong>
															</td>
															<td >
																${{sprintf('%1.2f',$invoiceDetails->subtotal)}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Booking fee</strong>
															</td>
															<td >
																${{sprintf('%1.2f',$invoiceDetails->bookingfee)}}
															</td>
														</tr>
														<tr>
															<td width="30%"><strong >Extras</strong></td>
															<td style="text-transform:uppercase">
																@if(!is_null($orderextras))
																	@foreach($orderextras as $k=> $v)
																		{{$v->name}} / $<?php echo sprintf('%1.2f',$v->charge ).'<br />'?>
																	@endforeach
																@else
																	{{'-'}}
																@endif
																
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >GST 10% paid</strong>
															</td>
															<td >
																${{sprintf('%1.2f',$invoiceDetails->gst)}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Grand total</strong>
															</td>
															<td >
																<strong>${{sprintf('%1.2f',$invoiceDetails->totalServiceCharge)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card category</strong>
															</td>
															<td >
																{{$invoiceDetails->card_category}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card type</strong>
															</td>
															<td >
																{{$invoiceDetails->card_type}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card holder</strong>
															</td>
															<td >
																{{$invoiceDetails->card_holder}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Card number</strong>
															</td>
															<td >
																{{$invoiceDetails->card_number}}
															</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>	
								</div>														
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection