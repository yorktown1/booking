@extends('template_form')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Multi Destinations Tickets</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Multi Destinations Management</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty($result))
								@if ($result['status'] == 'danger')
									<div class="alert alert-danger" role="alert">
										
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'success')
									<div class="alert alert-success" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'warning')
									<div class="alert alert-warning" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@elseif ($result['status'] == 'notif')
									<div class="alert alert-danger" role="alert">
											{{$result['message']}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
										<strong>Manage Multi Destinations Tickets</strong>
								</div>
									
								<div class="card-body">
									<?php /**
									<form class="mb-2" action="3/editMiscDetails" method="POST">
										{{csrf_field()}}
										<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
										<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
										<input type="hidden" name="idBinType" value="3">
										<div class=" form-row mb-2"> 
											<div class="col-12">
												<label for="extraHireage"><strong>Extra Hireage:</strong></label>
											</div>
											<div class="col-12 col-md-4">
												<div class="row">
													<div class="col-1">
														<span>$</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->extraHireagePrice}}" id="extraHireage" type="text" class="form-control ml-2 mr-2" name="extraHireage" required="" readonly />
														@else
															<input id="extraHireage" type="text" class="form-control ml-2 mr-2" name="extraHireage" required="" readonly />
														@endif
														
													</div>
													<div class="col-3 text-center">
														<span>extra hire per day after</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->extraHireageDays}}" id="extraHireageDays" type="text" class="form-control ml-2 mr-2" name="extraHireageDays"  required="" readonly />
														@else
															<input id="extraHireageDays" type="text" class="form-control ml-2 mr-2" name="extraHireageDays"  required="" readonly/>
														@endif
													</div>
													<div class="col-2">
														<span>days</span>
													</div>
												</div>
											</div>
										</div>
										<div class=" form-row mb-2">
											<div class="col-12">
												<label for="excessWeight"><strong>Excess Weight: </strong></label>
											</div>
											<div class="col-12 col-md-5">
												<div class="row">
													<div class="col-1">
														<span>$</span>
													</div>
													<div class="col-3">
														@if(!is_null($serviceOptions))
															<input value="{{$serviceOptions->excessWeightPrice}}" id="excessWeight" type="text" class="form-control ml-2 mr-2" name="excessWeight" readonly />
														@else
															<input  id="excessWeight" type="text" class="form-control ml-2 mr-2" name="excessWeight" readonly />
														@endif
													</div>
													<div class="col-8 text-center">
														<label>per tonne additional customer charge for weights exceeding allowance of 150kg per cubic metre.</label>
													</div>
												</div>
											</div>
										</div>
										<?php
										/**
										<button name="submit" type="submit" class="btn btn-primary">Save</button>**/
										?>
									<?php /**
									</form>
									<hr />
									<div class="extraServices">
										 <div class="row">
											<div class="col-12">
												<p><strong>Extras</strong></p>
												
											</div> 
											<div class="col-12">
												<form class="mb-2" action="/binServicesExtra/3" method="POST">
													{{csrf_field()}}
													<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
													<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
													<input type="hidden" name="idBinType" value="3">
													@if(!is_null($extraservices))
														<div class="form-row align-items-center">
															<?php foreach($extraservices as $k=>$v):?>
																<?php $form_value = null; foreach($getbinextraservices as $kk=>$vv):?>
																	<?php
																		
																		if($vv->idExtraService == $v->idExtraServices){
																			$form_value = $vv->charge;
																		}
																	?>
																<?php endforeach;?>
																<div class="col-auto">
																	<label for="{{$v->slug}}">{{$v->name}}</label>
																	<input type="hidden"  name="extras_hidden[]" value="{{$v->idExtraServices}}">
																	<input name="extras[<?=$k?>]" type="text" class="form-control mb-2" id="{{$v->slug}}" required value="<?=(!is_null($form_value)) ? $form_value : 0 ;?>" readonly>
																</div>
															<?php endforeach; ?>
															
														</div>
														<?php /*
														<div class="form-row align-items-center">
															<div class="col-auto">
																<button name="submit" type="submit" class="btn button-yellow">Save</button>
															</div>
														</div>
														*/ ?>
													<?php /**
													@else
														<div class="form-row align-items-center">
															<div class="col-auto">
																<p>Please add at least one extras for this bin type</p>
															</div>
														</div>
													@endif
												</form>
											</div>
										 </div>
									</div>
									<hr />
									**/
									?>
									<p>Use this page to manage your rates:</p>
									<ul>
										<li style="list-style-type:none">
											<i class="fa fa-pencil mr-2"></i> Click on the edit pencil button to change your daily rates. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-check mr-2"></i>  Click on the tick button to save your changes. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-trash mr-2"></i> Click on the delete trash button to reset all your rates and inventory to 0 for the selected bin size. 
										</li>
										<li style="list-style-type:none">
											<i class="fa fa-ban mr-2"></i> Click on each checkbox to block any delivery dates. Skip bins will not be offered for dates that are checked.
										</li>
									</ul>

									<div class="schedule-wrapper">
										<div class="row">
											<div class="col-12">
												@if(!is_null($offset))
    												<?php $offsetPage = $offset; ?>
    											@else
    												<?php $offsetPage = $dateNow;?>
    											@endif
												<strong class="mb-2">{{date('l d-m-Y', strtotime('now'))}}</strong>

												<table class="table table-responsive table-bordered" style="border:none;">
													
  													<tr>
  														
    													<th class="tg-yw4l" colspan="4">
    														<?php $prevDate = date('d-m-Y', $offsetPage)?>
    														<a href="{{ url('/') }}/3/{{$binsize}}/calendarOffset/<?= date('d-m-Y', strtotime($prevDate.'-2 weeks'))?>">Prev</a>
    													</th>
    													<th class="tg-yw4l" style="background:#eaeaea">Default</th>
    													
														
														@for ($i = 0; $i < 14; $i++)
															<?php $a[] = $offsetPage+($i*24*60*60);?>
    														<th class="tg-yw4l"><?php echo date('D', $a[$i]).' <br/>'.date('d-m', $a[$i])?></th>
    														<?php $lastDate = date('d-m-Y', $a[$i]);?>
														@endfor
    													<th class="tg-yw4l"><a href="{{ url('/') }}/3/{{$binsize}}/calendarOffset/<?= date('d-m-Y', strtotime($lastDate.'+1 day'))?>">Next</a></th>
  													</tr>
													<?php /**
  													<tr>
    													<td class="tg-lqy6" colspan="5">Non Delivery Days</td>
    									
    													<form action="{{ url('/') }}/3/editNonDeliveryDays" method="post">
    														{{csrf_field()}}
    														<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
															<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
															<input type="hidden" name="idBinType" value="3">
															<?php $status = '';?>
									  						
    														@for ($i = 0; $i < 14; $i++)
															<?php $a[] = $offsetPage+($i*24*60*60);?>
																<?php $get_day_now = date('D', $a[$i]); ?>
																@if($get_day_now == 'Sat')
																	@if($supplierData->isOpenSaturday == 0)
																		<?php $status = 'checked';?>
																	@else
																		<?php $status = '';?>
																	@endif
																@elseif($get_day_now == 'Sun')
																	@if($supplierData->isOpenSunday == 0)
																		<?php $status = 'checked';?>
																	@else
																		<?php $status = '';?>
																	@endif
																@else 
																	<?php $status = '';?>
																@endif
																<?php $status_1 = ''?>
																@if(!is_null($nonDelivery))
									  								<?php foreach($nonDelivery as $dataDelivery):?>
																		<?php if($dataDelivery->date == date('Y-m-d', $a[$i])):?>
																			<?php $status_1 = 'checked';break;?>
																		<?php else:?>
																			<?php $status_1 = '';?>
																		<?php endif;?>
																	<?php endforeach;?>
																@else
																		<?php $status_1 = '';?>
																@endif
    															<th class="tg-yw4l">
    																<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																<input name="nondelivery[<?=$i;?>]" <?=$status;?> <?=(!is_null($status_1)) ? $status_1 : '';?> type="checkbox" id="nondelivery">
    															</th>
															@endfor
															<td class="tg-yw4l">
																<button name="submit" type="submit" class="btn btn-danger">
																	<i class="fa fa-ban mr-2"></i>
																</button>
															</td>
    													</form>
  													</tr>
													**/
													?>
  													@foreach($getBinSize as $size)
  													<form action="{{ url('/') }}/3/editBinServicePrice" method="POST">
  														{{csrf_field()}}
    													<input type="hidden" name="idUser" value="{{$supplierData->idUser}}">
														<input type="hidden" name="idSupplier" value="{{$supplierData->idSupplier}}">
														<input type="hidden" name="idBinType" value="3">
														<input type="hidden" name="idBinSize" value="{{$size->idSize}}">
  														<tr>
    														<td class="tg-baqh" colspan="3" rowspan="3">{{$size->size}}</td>
    														<td class="tg-yw4l">Adult</td>
    														<td style="background:#eaeaea">
    															<?php $value = 0; ?>
																@if(!is_null($baseprice))
    																@if ($size->idSize == $binsize)
    																	<?php foreach($baseprice as $data):?>	
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					@if($items->own == 1)
																						<?php $value = $items->baseprice ?>
																					@endif
																					
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<input type="hidden" value="1" class="form-control bin-input" name="basePriceAdultHidden" id="basePriceAdultHidden" >
																		<input type="text" value="{{$value}}" class="form-control bin-input" name="basePriceAdult" id="basePriceAdult" required="">
    																@else
    																	<?php foreach($baseprice as $data):?>
    																		@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					@if($items->own == 1)
																						<?php $value = $items->baseprice ?>
																					@endif
																					
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		{{$value}}
																	@endif
																@else
																	<input type="hidden" value="1" class="form-control bin-input" name="basePriceAdultHidden" id="basePriceAdultHidden" >
																	<input type="text" value="" class="form-control bin-input" name="basePriceAdult" id="basePriceAdult" required="">
																@endif
    														</td>
																<?php $data = '';?>
    															@for ($i = 0; $i < 14; $i++)
																	<?php $a[] = $offsetPage+($i*24*60*60);?>
																	@if(!is_null($baseprice))
																		@if ($size->idSize == $binsize)
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							@if($baseitems->own == 1)
																								<?php $value = 0 ?>
																							@endif
																						@else
																							@if($baseitems->own == 1)
																								<?php $value = $baseitems->baseprice ?>
																							@endif
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							@if($itemsvalue->own == 1)
																								<?php $value =  $itemsvalue->price; break;?>
																							@endif
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<th class="tg-yw4l">
																				<input type="hidden" value="1" class="form-control bin-input" name="binPriceAdultHidden[<?=$i;?>]" id="binPriceAdultHidden" >
    																			<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																			<input type="text" value="{{$value}}" class="form-control bin-input" name="binPriceAdult[<?=$i;?>]" id="binPriceAdult[]">
    																		</th>
																		@else
																			<?php $value = 0; ?>
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							@if($baseitems->own == 1)
																								<?php $value = 0 ?>
																							@endif
																						@else
																							@if($baseitems->own == 1)
																								<?php $value = $baseitems->baseprice ?>
																							@endif
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							@if($itemsvalue->own == 1)
																								<?php $value =  $itemsvalue->price; break;?>
																							@endif
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<th class="tg-yw4l">
																				{{$value}}
																			</th>
																		@endif
																	@else
																		<?php $value = 0?>
																	@endif
																@endfor
															@if($size->idSize == $binsize)
															<td class="tg-yw4l" rowspan="3">
																<button name="button" type="submit" class="btn btn-primary">
																	<i class="fa fa-check"></i>
																</button>
															</td>
															@endif
  														</tr>
														
														<tr>
    														<td class="tg-yw4l">Childs</td>
    														<td style="background:#eaeaea">
    															<?php $value = 0; ?>
																@if(!is_null($baseprice))
    																@if ($size->idSize == $binsize)
    																	<?php foreach($baseprice as $data):?>	
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					@if($items->own == 2)
																						<?php $value = $items->baseprice ?>
																					@endif
																					
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<input type="hidden" value="2" class="form-control bin-input" name="basePriceChildHidden" id="basePriceChildHidden" >
																		<input type="text" value="{{$value}}" class="form-control bin-input" name="basePriceChild" id="basePriceChild" required="">
    																@else
    																	<?php foreach($baseprice as $data):?>
    																		@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					@if($items->own == 2)
																						<?php $value = $items->baseprice ?>
																					@endif
																					
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		{{$value}}
																	@endif
																@else
																	<input type="hidden" value="2" class="form-control bin-input" name="basePriceChildHidden" id="basePriceChildHidden" >
																	<input type="text" value="" class="form-control bin-input" name="basePriceChild" id="basePriceChild" required="">
																@endif
    														</td>
																<?php $data = '';?>
    															@for ($i = 0; $i < 14; $i++)
																	<?php $a[] = $offsetPage+($i*24*60*60);?>
																	@if(!is_null($baseprice))
																		@if ($size->idSize == $binsize)
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							@if($baseitems->own == 2)
																								<?php $value = 0 ?>
																							@endif
																						@else
																							@if($baseitems->own == 2)
																								<?php $value = $baseitems->baseprice ?>
																							@endif
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							@if($itemsvalue->own == 2)
																								<?php $value =  $itemsvalue->price; break;?>
																							@endif
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<th class="tg-yw4l">
																				<input type="hidden" value="2" class="form-control bin-input" name="binPriceChildHidden[<?=$i;?>]" id="binPriceChildHidden[]" >
    																			<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																			<input type="text" value="{{$value}}" class="form-control bin-input" name="binPriceChild[<?=$i;?>]" id="binPriceChild[]">
    																		</th>
																		@else
																			<?php $value = 0; ?>
																			<?php foreach($baseprice as $data):?>
																				@if($size->idSize == $data->idBinSize)
																					<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																						@if(is_null($baseitems->baseprice))
																							@if($baseitems->own == 2)
																								<?php $value = 0 ?>
																							@endif
																						@else
																							@if($baseitems->own == 2)
																								<?php $value = $baseitems->baseprice ?>
																							@endif
																						@endif
																					<?php endforeach?>


																					<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																						@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																							@if($itemsvalue->own == 2)
																								<?php $value =  $itemsvalue->price; break;?>
																							@endif
																						@endif
																					<?php endforeach?>
																				@endif
																			<?php endforeach;?>
																			<th class="tg-yw4l">
																				{{$value}}
																			</th>
																		@endif
																	@else
																		<?php $value = 0?>
																	@endif
																@endfor
															
  														</tr>
														<tr>
    														<td class="tg-yw4l">Infants</td>
    														<td style="background:#eaeaea">
    															<?php $value = 0; ?>
																@if(!is_null($baseprice))
    																@if ($size->idSize == $binsize)
    																	<?php foreach($baseprice as $data):?>	
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					@if($items->own == 3)
																						<?php $value = $items->baseprice ?>
																					@endif
																					
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<input type="hidden" value="3" class="form-control bin-input" name="basePriceInfantsHidden" id="basePriceInfantsHidden" >
																		<input type="text" value="{{$value}}" class="form-control bin-input" name="basePriceInfants" id="basePriceInfants" required="">
    																@else
    																	<?php foreach($baseprice as $data):?>
    																		@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $items):?>
																					@if($items->own == 3)
																						<?php $value = $items->baseprice ?>
																					@endif
																					
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		{{$value}}
																	@endif
																@else
																	<input type="hidden" value="3" class="form-control bin-input" name="basePriceInfantsHidden" id="basePriceInfantsHidden" >
																	<input type="text" value="" class="form-control bin-input" name="basePriceInfants" id="basePriceInfants" required="">
																@endif
    														</td>
															<?php $data = '';?>
    														@for ($i = 0; $i < 14; $i++)
																<?php $a[] = $offsetPage+($i*24*60*60);?>
																@if(!is_null($baseprice))
																	@if ($size->idSize == $binsize)
																		<?php foreach($baseprice as $data):?>
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																					@if(is_null($baseitems->baseprice))
																						@if($baseitems->own == 3)
																							<?php $value = 0 ?>
																						@endif
																					@else
																						@if($baseitems->own == 3)
																							<?php $value = $baseitems->baseprice ?>
																						@endif
																					@endif
																				<?php endforeach?>


																				<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																					@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																						@if($itemsvalue->own == 3)
																							<?php $value =  $itemsvalue->price; break;?>
																						@endif
																					@endif
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<th class="tg-yw4l">
																			<input type="hidden" value="3" class="form-control bin-input" name="binPriceInfantsHidden[<?=$i;?>]" id="binPriceInfantsHidden[]" >
    																		<input type="hidden"  name="date[]" value="<?php echo date('Y-m-d', $a[$i]) ?>">
    																		<input type="text" value="{{$value}}" class="form-control bin-input" name="binPriceInfants[<?=$i;?>]" id="binPriceInfants[]">
    																	</th>
																	@else
																		<?php $value = 0; ?>
																		<?php foreach($baseprice as $data):?>
																			@if($size->idSize == $data->idBinSize)
																				<?php foreach($binservicebaseprice[$data->idBinSize] as $baseitems):?>
																					@if(is_null($baseitems->baseprice))
																						@if($baseitems->own == 3)
																							<?php $value = 0 ?>
																						@endif
																					@else
																						@if($baseitems->own == 3)
																							<?php $value = $baseitems->baseprice ?>
																						@endif
																					@endif
																				<?php endforeach?>


																				<?php foreach($binservicediscprice[$data->idBinService] as $discitems => $itemsvalue):?>
																					@if($itemsvalue->date == date('Y-m-d', $a[$i]))
																						@if($itemsvalue->own == 3)
																							<?php $value =  $itemsvalue->price; break;?>
																						@endif
																					@endif
																				<?php endforeach?>
																			@endif
																		<?php endforeach;?>
																		<th class="tg-yw4l">
																			{{$value}}
																		</th>
																	@endif
																@else
																	<?php $value = 0?>
																@endif
															@endfor
  														</tr>
  													</form>
  													@endforeach
												</table>
											</div>
										</div>
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
