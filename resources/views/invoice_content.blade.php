@extends('login_template')
@section('login_content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
			
			.bin_desc ul li{
				color:#c40005;
			}
			ul li{
				padding-left:0;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														{{Config::get('invoices.company')}}<br/>
														{{Config::get('invoices.full_address')}}<br/>
														{{Config::get('invoices.zipcode')}} <br/>
														{{Config::get('invoices.phone')}}<br/>
														{{Config::get('invoices.email')}}<br/>
														ABN : {{Config::get('invoices.abn')}}
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="card mb-3">
								<div class="card-body">	
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table" border="0" width="100%" cellpadding="3">
														<tr>
															<td style="text-align:center">
																<h3 style="text-transform:uppercase; ">Order slip</h3><br />
																<strong style="text-transform:uppercase;">Invoice no. {{$invoiceDetails->paymentUniqueCode}}</strong><br />
																
																<strong style="text-transform:uppercase;">Date: <?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?></strong>
															</td>
														</tr>
													</table>
												</div>

												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">1. Order details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Bin type</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$binhire->name}} <br />
																{{$binhire->size}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Description</strong>
															</td>
															<td style="text-transform:uppercase" >
																<?php $tags = array("strong", "b");?>
																<p><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
																<div style="color:#c40005;" class="bin_desc"><?php echo $binhire->description; ?></div>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Pickup Date</strong>
															</td>
															<td style="text-transform:uppercase">
																<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Return Date</strong>
															</td>
															<td style="text-transform:uppercase">
																<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
															</td>
														</tr>
														@if(!is_null($invoiceDetails->deliveryComments))
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Customer additional note</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$invoiceDetails->deliveryComments}}
															</td>
														</tr>
														@endif
														<tr>
															<td width="30%"><strong style="text-transform:uppercase">Extras</strong></td>
															<td style="text-transform:uppercase">
																<?php $totalextras =  0; ?>
																@if(!is_null($orderextras))
																	@foreach($orderextras as $k=> $v)
																		<?php $totalextras = $totalextras + $v->charge?>
																	{{$v->name}} / $<?php echo sprintf('%1.2f',$v->charge ).'<br />' ?>
																	@endforeach
																@else
																	{{'-'}}
																@endif
															</td>
														</tr>
													</table>
												</div>
												

												
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">2. Customer delivery details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Name</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$customerdetails->name}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Address</strong>
															</td>
															<td style="text-transform:uppercase" >
																{{$customerdetails->address}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Zipcode</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$customerdetails->zipcode}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Phone</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$customerdetails->phone}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Email</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$customerdetails->email}}
															</td>
														</tr>
													</table>
												</div>
												
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">3. Payment confirmation</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Bin hire price (including extras)</strong>
															</td>
															<td >
																<strong>${{sprintf('%1.2f',$invoiceDetails->subtotal + $totalextras)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong >Grand total (including booking fee)</strong>
															</td>
															<td >
																<strong>${{sprintf('%1.2f',$invoiceDetails->totalServiceCharge)}}</strong>
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Card category</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$invoiceDetails->card_category}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Card type</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$invoiceDetails->card_type}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Card holder</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$invoiceDetails->card_holder}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Card number</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$invoiceDetails->card_number}}
															</td>
														</tr>
													</table>
												</div>
												
												<div class="table-responsive">
													<table class="table table-condensed" border="0" width="100%" cellpadding="3">
														<tr>
															<td colspan="2"><h3  style="text-transform:uppercase;">4. Supplier details</h3></td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Business Name</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$supplierdetails->name}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Contact name</strong>
															</td>
															<td style="text-transform:uppercase" >
																{{$supplierdetails->contactName}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Business hours phone number</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$supplierdetails->phonenumber}}
															</td>
														</tr>
														<tr>
															<td width="30%">
																<strong style="text-transform:uppercase;">Email</strong>
															</td>
															<td style="text-transform:uppercase">
																{{$supplierdetails->email}}
															</td>
														</tr>
														
													</table>
												</div>
											</div>
										</div>
									</div>	
								</div>														
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection