@extends('payment_template')
@section('payment_content')
	<?php
		/**
		<div class="col-xl-12">
			<div class="breadcrumb-holder">
				<ol class="breadcrumb float-left">
					<li class="breadcrumb-item active"><strong>1. Insert your billing address</strong></li>
					<li class="breadcrumb-item">2. Insert your credit card details</li>
					<li class="breadcrumb-item">3. Finish</li>
				</ol>
				<h1 class="main-title float-right">Payment process steps</h1>
				<div class="clearfix"></div>
			</div>
		</div>
		**/
	?>
	
    <div class="col-12 py-3">
        <div class="section-content">
			@if ($errors->has('amount'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('amount') }}
				</div>
			@endif
			@if ($errors->has('first_name'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('first_name') }}
				</div>
			@endif
			@if ($errors->has('last_name'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('last_name') }}
				</div>
			@endif
            @if ($errors->has('address'))
                <div class="alert alert-danger mr-2" role="alert">
                    {{ $errors->first('address') }}
                </div>
            @endif
			@if ($errors->has('suburb'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('suburb') }}
				</div>
			@endif
			@if ($errors->has('phone'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('phone') }}
				</div>
			@endif
			@if ($errors->has('email'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('email') }}
				</div>
			@endif
			@if ($errors->has('agree'))
				<div class="alert alert-danger mr-2" role="alert">
					{{ $errors->first('agree') }}
				</div>
			@endif
            @if ($errors->has('postal_code'))
                <div class="alert alert-danger mr-2" role="alert">
                    {{ $errors->first('postal_code') }}
                </div>
            @endif
            
            @if ($message = Session::get('success'))
                <div class="alert alert-success mr-2" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        {!! $message !!}
                </div>
                <?php Session::forget('success');?>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        {!! $message !!}
                </div>
                <?php Session::forget('error');?>
            @endif
        </div>
    </div>
    <div class="col-12 col-md-6">
		
        <div class="section-header">
            <h5><strong>Enter your details and shipping details. <sup>* required</sup></strong>  </h5>
        </div>
        <div class="section-content">
			
            <form id="form_geo" action="{!! URL::route('payment') !!}" method="POST"> 
				{{ csrf_field() }} 
				<input type="hidden" name="idpaymenttemp" value="{{$idpaymenttemp}}"/>
                <input type="hidden" name="amount" value="{{$purchasedetails->price}}">
                <input type="hidden" name="idBinType" value="{{$purchasedetails->idBinType}}">
                <input type="hidden" name="idBinSize" value="{{$purchasedetails->idBinSize}}">
                <input type="hidden" name="zipcode" value="{{$purchasedetails->zipcode}}">
                <input type="hidden" name="deliverydate" value="{{$purchasedetails->deliveryDate}}">
                <input type="hidden" name="collectiondate" value="{{$purchasedetails->collectionDate}}">
                <input type="hidden" name="idSupplier" value="{{$purchasedetails->idSupplier}}">
                <input type="hidden" id="area" name="area" value="{{$servicearea->area}}">
				<input type="hidden" name="cancel_return" value="{{url('payment-cancel')}}">
				<input type="hidden" name="return" value="{{url('payment-status')}}">
                <div class="form-row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="first_name">First name <sup>* <?=($errors->has('first_name')) ? '<strong class="text-danger">'.$errors->first('first_name').'</strong>' : ''?></sup></label>
                        <input class="form-control <?=($errors->has('first_name')) ? 'is-invalid' : ''?>" id="first_name" name="first_name" placeholder="First name" required="" type="text" value="<?=($errors->has('first_name')) ? '' : old('first_name')?>">
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="last_name">Last name <sup>* <?=($errors->has('last_name')) ? '<strong class="text-danger">'.$errors->first('last_name').'</strong>' : ''?></sup></label>
                        <input class="form-control <?=($errors->has('last_name')) ? 'is-invalid' : ''?>" id="last_name" name="last_name" placeholder="Last name" required="" type="text" value="<?=($errors->has('last_name')) ? '' : old('last_name')?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address">Street Name <sup>* <?=($errors->has('address')) ? '<strong class="text-danger">'.$errors->first('address').'</strong>' : ''?></sup></label>
                    <input class="text form-control <?=($errors->has('address')) ? 'is-invalid' : ''?>" name="address" id="autocomplete" cols="30" rows="10" required=""  placeholder="Type your street name"/>
                </div>
				<div class="form-group">
                    <label for="unit">Unit / Lot</label>
                    <input class="text form-control" name="unit" cols="30" rows="10" placeholder="Type your Unit /Lot"/>
                </div>
                <div class="form-group">
                    <label for="suburb">Suburb <sup>* <?=($errors->has('suburb')) ? '<strong class="text-danger">'.$errors->first('suburb').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('suburb')) ? 'is-invalid' : ''?>" name="suburb" id="locality" required="" placeholder="Suburb" value="" data-geo="locality">
                </div>
                <div class="form-group">
                    <label for="zipcode">Postcode <sup>* <?=($errors->has('zipcode')) ? '<strong class="text-danger">'.$errors->first('zipcode').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('zipcode')) ? 'is-invalid' : ''?>" name="zipcode" id="postal_code" value="{{$purchasedetails->zipcode}}" readonly>
                </div>
                <div class="form-group">
                    <label for="phone">Phone <sup>* <?=($errors->has('phone')) ? '<strong class="text-danger">'.$errors->first('phone').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('phone')) ? 'is-invalid' : ''?>" name="phone" id="phone" required="" placeholder="Phone" value="<?=($errors->has('phone')) ? '' : old('phone')?>">
                </div>
                <div class="form-group">
                    <label for="email">Email <sup>* <?=($errors->has('email')) ? '<strong class="text-danger">'.$errors->first('email').'</strong>' : ''?></sup></label>
                    <input type="text" class="form-control <?=($errors->has('email')) ? 'is-invalid' : ''?>" name="email" id="email" required="" placeholder="Email" value="<?=($errors->has('email')) ? '' : old('email')?>">
                </div>
                <div class="form-group">
					<label for="note">Bin Placement <sup>* <?=($errors->has('note')) ? '<strong class="text-danger">'.$errors->first('note').'</strong>' : ''?></sup></label>
					<input type="text" class="form-control <?=($errors->has('note')) ? 'is-invalid' : ''?>" name="note" id="note" required="" placeholder="Bin Placement" value="<?=($errors->has('note')) ? '' : old('note')?>">
					<p class="my-2 text-small text-danger">Bins are placed on driveways at customers request, no responsibility for damage is accepted. Placement instructions provided by customer will be satisfied where possible and we may need to contact you for alternative placement.</p>
				</div>
				
        </div>
    </div>
    @if(!is_null($purchasedetails))
    <div class="col-12 col-md-6">
		<div class="section-header">
            <h5><strong>Extras</strong>  </h5>
        </div>
		<div class="section-content">
			@if(!is_null($extras_service_availabe))
				<div class="form-row align-items-center">
					<?php foreach($extras_service_availabe as $kreal=>$vreal):?>
						<?php $f = null;foreach($supplier_extras_price as $kkreal=>$vvreal):?>
							<?php
								
								if($vvreal->idExtraService == $vreal->idExtraServices){
									$idES = $vvreal->idBinServiceExtra;
									$f = $vvreal->charge;
								}
							?>
						<?php endforeach;?>
						<div class="col-12 col-md-6">
							<input type="hidden"  name="extras_hidden[]" value="<?=(!is_null($idES)) ? $idES : 0 ;?>">
							<input type="hidden"  name="extras_price_hidden[]" value="<?=(!is_null($f)) ? $f : 0 ;?>">
							<input name="extras[]" type="checkbox" class="extras_checkboxes mb-2" id="{{$vreal->slug}}" value="{{$idES}}" data-value="<?=(!is_null($f)) ? $f : 0 ;?>">
							<label class="ml-3" for="{{$vreal->slug}}">{{$vreal->name}} / $ <?=(!is_null($f)) ? $f : 0 ;?></label>
						</div>
					<?php endforeach; ?>
					
				</div>
			@endif
		</div>
        <table class="table table-striped">
            <tbody>
				 <tr>
                    <td width="30%"><strong>Note</strong></td>
                    <td>
						<?php if (!is_null($seasonal_mode)):?>
							<input type="hidden" name="seasonal_mode" value="{{$seasonal_mode}}"/>
							<?php if($seasonal_mode == '1'):?>
								<p><span>Special winter pricing</span></p>
								<?php if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
									<p><span>Charged $<?php echo sprintf('%1.2f',$binhireoptions->extraHireagePrice )?> per day after <?=$binhireoptions->extraHireageDays?> days hire</span></p>
								<?php endif;?>
								
								<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
									<p><span>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </span></p>
								<?php endif;?>
							<?php else : ?>
								<?php if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
									<p><span>Charged $<?php echo sprintf('%1.2f',$binhireoptions->extraHireagePrice )?> per day after <?=$binhireoptions->extraHireageDays?> days hire</span></p>
								<?php endif;?>
								
								<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
									<p><span>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </span></p>
								<?php endif;?>
							<?php endif;?>
						<?php else :?>
							<?php if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
								<p><span>Charged $<?php echo sprintf('%1.2f',$binhireoptions->extraHireagePrice )?> per day after <?=$binhireoptions->extraHireageDays?> days hire</span></p>
							<?php endif;?>
								
							<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
								<p><span>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </span></p>
							<?php endif;?>
						<?php endif;?>
					</td>
				</tr>
                <tr>
                    <td width="30%"><strong>Days</strong></td>
                    <td>
						{{$daysmargin}} (Day / Days) <br />(<?=date('l d-m-Y', strtotime($purchasedetails->deliveryDate))?> - <?=date('l d-m-Y', strtotime($purchasedetails->collectionDate))?>)
                    </td>
                </tr>
				<tr>
                    <td width="30%"><strong>Extra days</strong></td>
                    <td>
						<?=($exactplusdays > 0) ? $exactplusdays.'<sup>day / days</sup>' : '-'?>
                    </td>
                </tr>
				<tr>
					<td width="30%"><strong>Bin hire price</strong></td>
					<td>
						$<?php echo sprintf('%1.2f',$totalprice )?>
					</td>
				</tr>
				<tr>
					<td width="30%"><strong>Booking fee</strong></td>
					<td>
						$<?php echo sprintf('%1.2f',$bookingprice->price )?>
						<input type="hidden" name="bookingfee" value="{{$bookingprice->price}}"/>
						<input type="hidden" name="subtotal" value="{{$totalprice}}"/> 
					</td>
				</tr>
				<tr>
					<td width="30%"><strong>Extras</strong></td>
					<td id="extras-price">
						-
					</td>
				</tr>
				<tr>
					<td width="30%"><strong>GST 10% Paid</strong></td>
					<td>
						<input type="hidden" name="gst" value="{{sprintf('%1.2f',$gst)}}"/>
						<input type="hidden" name="initial_gst" value="{{sprintf('%1.2f',$gst)}}"/>
						<p id="gst">$<?php 
							echo sprintf('%1.2f',$gst)?></p>
					</td>
				</tr>
				
				<tr>
					<td width="30%"><strong class="text-danger">Grand Total</strong></td>
					<td>
						<strong  id="grandtotal" class="text-danger">$<?php echo sprintf('%1.2f',$grandtotal )?></strong>
						<input type="hidden" name="totalprice" value="{{$grandtotal}}"/>
						<input type="hidden" name="initial" value="{{$grandtotal}}"/>
					</td>
				</tr>
            </tbody>
        </table>
        <h5><strong>Check this box to proceed</strong></h5>
        <strong>Before submitting your bin hire request, please make sure you read the <a href="https://somsdevone.com/coastalwaste_new/refunds-cancellations/">terms and conditions</a> carefully.</strong>
		<div class="form-check checkout">

			<input type="checkbox" name="agree" id="agree" />
			<label for="agree">I agree with the <a href="https://somsdevone.com/coastalwaste_new/refunds-cancellations/">terms and conditions</a></label>
        </div>
		<button type="submit" name="submit" value="Submit" id="submit" class="btn btn-success float-md-right">Checkout</button>
		</form>
    </div>
    
    @endif
    
@endsection
