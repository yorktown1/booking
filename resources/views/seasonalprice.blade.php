@extends('template')
@section('content')
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<h1 class="main-title float-left">Seasonal Price</h1>
							<ol class="breadcrumb float-right">
								<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
								<li class="breadcrumb-item active">Seasonal Price</li>
							</ol>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						
						@if ($errors->has('seasonal_supplier'))
							<div class="alert alert-danger mr-2" role="alert">
								{{ $errors->first('seasonal_supplier') }}
							</div>
						@endif
						@if ($errors->has('seasonal_wastetype'))
							<div class="alert alert-danger mr-2" role="alert">
								{{ $errors->first('seasonal_wastetype') }}
							</div>
						@endif
						
						@if (!empty(session('status')))
							@if (session('status') == 'danger')
								<div class="alert alert-danger" role="alert">
										{{session('message')}} <br />
								</div>
							@elseif (session('status') == 'success')
								<div class="alert alert-success" role="alert">
										{{session('message')}} <br />
									
								</div>
							@endif
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-12">						
						<div class="card mb-3 mt-2">
							<div class="card-header">
								<strong>Seasonal Price for Supplier</strong>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<form action="/seasonal_filter" method="post">
											{{csrf_field()}}
											<div class="form-group row">
												<label for="select_supplier" class="col-sm-2 col-form-label">Supplier</label>
												<div class="col-sm-6">
													<input type="hidden" name="seasonal_supplier" value="{{$supplierData->idSupplier}}"/>
													<select name="seasonal_supplier" id="select_supplier" class="form-control">
														<option value="">Select Supplier</option>
														<?php if(!is_null($supplierData)):?>
																<option value="{{$supplierData->idSupplier}}" <?=($seasonal_supplier == $supplierData->idSupplier) ? 'selected' : ''?>>{{$supplierData->name}}</option>
														<?php endif;?>
													</select>
												</div>
											</div>
											<?php /**
											<div class="form-group row">
												<div class="col-sm-2">Apply this to child accounts</div>
													<div class="col-sm-6">
														<div class="form-check">
															<input class="form-check-input" type="checkbox" id="seasonal_child" name="seasonal_child" checked>
															<label class="form-check-label" for="seasonal_child">
																Yes
															</label>
														</div>
													</div>
											</div>
											**/
											?>
											<div class="form-group row">
												<label for="seasonal_wastetype" class="col-sm-2 col-form-label">Waste Type</label>
												<div class="col-sm-6">
													<select name="seasonal_wastetype" id="seasonal_wastetype" class="form-control">
														<option value="">Select Waste Type</option>
														<?php if(!is_null($bintype)):?>
															<?php foreach($bintype as $k=>$v):?>
																<option value="{{$v->idBinType}}"  <?=($seasonal_wastetype == $v->idBinType) ? 'selected' : ''?>>{{$v->name}}</option>
															<?php endforeach;?>
														<?php endif;?>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-sm-10">
													<button type="submit" class="btn button-yellow">Filter</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								
								@if(!is_null($binhire))
								<div class="row">
									<div class="col-12">
										<div class="table-responsive">
											<table class="table  table-bordered" style="border:none;">
												<tr>
													<th>Size</th>
													@foreach($special_days as $day)
														<th>{{$day->days}} days</th>
													@endforeach
													<th>Action</th>
												</tr>
												@foreach($getBinSize as $size)
													<tr>
														
														<td class="tg-baqh">{{$size->size}}</td>
														@foreach($special_days as $day)
															<?php $value = 0;?>
															@if(!empty($binhire))
																@foreach($binhire as $hire)
																	@if(($size->idSize == $hire->idBinSize) && ($day->idspecialdays == $hire->extrahireagedays))
																		<?php $value = $hire->price;?>
																	@endif
																@endforeach
															@else
																<?php $value = 0;?>
															@endif
															<td>{{$value}}</td>
														@endforeach
														
														<td><a href="/editspecialdays/{{$size->idSize}}/{{$seasonal_wastetype}}/{{$seasonal_supplier}}" class="btn button-yellow">Edit Price</a></td>
													</tr>
												@endforeach
											</table>
										</div>
										
									</div>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
