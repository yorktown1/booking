<htmlpageheader name="page-header">
	Bin Hire Quote from Invoice {{$invoiceDetails->paymentUniqueCode}}
</htmlpageheader>
	<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					body{
						font-size:11px;
						line-height:1.5px;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.3;
					}
					table{
						line-height:1.3;
					}
				} 
				
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr style="text-align:center">
						<td width="50%">
							<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
							</address>
						</td>
					</tr>
					<tr style="text-align:center">
						<td>
							<h3 style="text-transform:uppercase;">Tax Invoice no. {{$invoiceDetails->paymentUniqueCode}}</h3> <br />
							<strong ><?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?></strong ><br />
							@if(!is_null($supplierdetails))
								@if(!is_null($supplierdetails->seasonal_title))
									<h4 style="text-transform:uppercase;">{{$supplierdetails->seasonal_title}}</h4>
								@endif
							@endif
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" cellpadding="3">
					<tr>
						<td >
							<h3 style="text-transform:uppercase;">Customer Delivery Details:</h3>
							
							<address style="font-style:12px;">
								{{$customerdetails->name}}<br/>
								@if(!is_null($customerdetails->unit_lot)){{$customerdetails->unit_lot}}@endif {{$customerdetails->address}}<br/>
								{{$customerdetails->zipcode}} <br/>
								{{$customerdetails->phone}} <br/>
								{{$customerdetails->email}}
							</address>
						</td>
					</tr>
				</table>
			</div>
			
			<!--<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%">
					<tr>
						<td >
							<h5>Delivery Address:</h5>
							<address>
								<?//=$invoiceDetails->deliveryAddress;?>
							</address>
						</td>
					</tr>
				</table>
			</div>-->
			<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%" cellpadding="3">
					<tr>
						<td  width="50%">
							<h3 style="text-transform:uppercase;">Delivery Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
							</address>
						</td>
						<td  width="50%" style="padding-left: 20px">
							<h3 style="text-transform:uppercase">Collection Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
							</address>
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%" cellpadding="3">
					<tr>
						<td  width="50%">
							@if(!is_null($invoiceDetails->deliveryComments))
								<h3 style="text-transform:uppercase;">Bin Placement:</h3>
								<address style="font-style:12px;">
									<?=$invoiceDetails->deliveryComments;?>
								</address>
							@endif
						</td>
						
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table width="100%" class="table table-condensed" border="0" cellpadding="3">
					<tr>
						<td >
							<?php //if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
								<!--<p><span>Charged $<?//=$binhireoptions->extraHireagePrice?> after <?//=$binhireoptions->extraHireageDays?> days hire</span></p>-->
							<?php //endif;?>
							<?php if(!is_null($binhireoptions)):?>
								<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
									<p style="font-style:12px;"><span><strong>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </span></strong></p>
								<?php else:?>
									<p>-</p>
								<?php endif;?>
							<?php endif;?>
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="1" style="border:0.5px solid #b1b2b2;" width="100%" cellpadding="3">
					<tbody>
						<tr style="background:#005343;text-align:center">
							<td colspan="3" style="color:#fff;text-align:center;text-transform:uppercase;"><strong><h3>Order summary</h3></strong></td>
						</tr>
						<tr>
							<td ><strong>Bin type</strong></td>
							<td colspan="2" ><p >{{$binhire->name}}</p></td>
						</tr>
						<tr>
							<td ><strong>Bin size</strong></td>
							<td colspan="2" ><p >{{$binhire->size}}</p></td>
						</tr>
						<tr>
							<td ><strong>Description</strong></td>
							<td colspan="2" >
								<?php $tags = array("strong", "b");?>
								<p ><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
								<p style="color:#c40005;"><?php echo $binhire->description; ?></p>
							</td>
						</tr>
						<tr>
							<td ><strong>Note</strong></td>
							<td colspan="2" >
								@if(!is_null($seasonal_mode))
									@if($seasonal_mode == '1')
										{{'Winter 2019 Special Pricing'}}
									@else
										{{'Normal Pricing'}}
									@endif
								@else
									{{'Normal Pricing'}}
								@endif
							</td>
						</tr>
						<tr>
							<td ><strong>Bin hire price</strong></td>
							<td colspan="2" ><p >${{sprintf('%1.2f',$invoiceDetails->subtotal)}}</p></td>
						</tr>
						<tr>
							<td ><strong>Booking fee</strong></td>
							<td colspan="2"><p >${{sprintf('%1.2f',$invoiceDetails->bookingfee)}}</p></td>
						</tr>
						</tr>
						<tr>
							<td ><strong >Extras</strong></td>
							<td colspan="2" >
								@if(!is_null($orderextras))
									@foreach($orderextras as $k=> $v)
										@if(!is_null($v)  )
											{{$v->name}} / $<?php echo sprintf('%1.2f',$v->charge ).'<br />'?>
										@else 
											{{'-'}}
										@endif
									@endforeach
								@else 
									{{'-'}}
								@endif
							</td>
						</tr>
						<tr>
							<td ><strong>GST 10% paid</strong></td>
							<td colspan="2" ><p>${{sprintf('%1.2f',$invoiceDetails->gst)}}</p></td>
						</tr>
						<tr style="background:#005343;">
							<td style="color:#fff;"><strong>Grand total</strong></td>
							<td colspan="2" style="color:#fff;"><p >${{sprintf('%1.2f',$invoiceDetails->totalServiceCharge)}}</p></td>
						</tr>
						<tr style="text-align:center">
							<td colspan="3" style="text-align:center;text-transform:uppercase;" ><strong>Payment confirmed, due amount 0 (Zero).</strong></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr>
						<td width="50%" >
							<h3 style="text-transform:uppercase;">Supplier Details:</h3>
							<address  style="font-style:12px;">
								Business name : <i>Coastal Waste</i><br/>
								Contact name : <i>Coastal Waste</i><br/>
								Business hours phone number : <i>(08) 9592 9420 </i><br/>
								Email: <i>info@coastalwaste.com.au</i><br/>
							</address>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>