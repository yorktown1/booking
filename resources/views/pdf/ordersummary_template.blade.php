
<htmlpageheader name="page-header">
	Annual Order Summary
</htmlpageheader>
<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					body{
						font-size:11px;
						line-height:1.9px;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.7;
					}
					table{
						line-height:1.7;
					}
				} 
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr >
						<td width="50%" style="text-align:left">
							<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
							</address>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="table-responsive">
				<table class="table table-condensed" border="0" cellpadding="3">
					<tr>
						<td>
							<h3 style="text-transform:uppercase;">Annual Order Summary</h3> <br />
							<strong style="text-transform:uppercase;">Date:</strong> {{$startDate}} - {{$endDate}}</strong> <br />
							<strong style="text-transform:uppercase;">Supplier: {{$selected_supplierdata->name}}</strong>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr>
						<td>
							<h3 style="text-transform:uppercase;">Supplier Details:</h3>
							<address >
								{{$selected_supplierdata->name}}<br/>
								{{$selected_supplierdata->email}} <br/>
								{{$selected_supplierdata->phonenumber}} <br/>
								{{$selected_supplierdata->fullAddress}} <br/>
							</address>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="table-responsive">
				<p ><strong>Note : The Revenue field is the suppliers income after 15% charged for the system.</strong></p>
				<table class="data table table-bordered" border="1" style="border:1px solid #b1b2b2;" width="100%" cellpadding="3">
					<tr class="text-center" align="center" style="color:#fff !important;background:#005343;">
						<td><strong style="color:#fff ">Order Ref</strong></td>
						<td><strong style="color:#fff ">Order Date</strong></td>
						<td><strong style="color:#fff ">Bin Type</strong></td>
						<td><strong style="color:#fff ">Bin Size</strong></td>
						<td><strong style="color:#fff ">Customer Name</strong></td>
						<td><strong style="color:#fff ">Delivery Date</strong></td>
						<td><strong style="color:#fff ">Collection Date</strong></td>
						<td><strong style="color:#fff ">Revenue</strong></td>
					</tr>
					<tbody>
						<?php $sum = 0;?>
						@foreach($suppliesdata as $data)
							<tr>
								<td><a href="{{ url('/') }}/order_detail/{{$data->paymentUniqueCode}}/{{$data->idSupplier}}/{{$data->idCustomer}}/{{$data->idBinType}}/{{$data->idBinService}}">{{$data->paymentUniqueCode}}</a></td>
								<td >{{date('d-m-Y', strtotime($data->orderDate))}}</td>
								<td >{{$data->bintypename}}</td>
								<td >{{$data->binsize}}</td>
								<td >{{$data->customerName}}</td>
								<td >{{$data->deliveryDate}}</td>
								<td >{{$data->collectionDate}}</td>
								<td>
									<?php
										// ** change this if you want to show with or without booking price **/
										//$subtotal = $data->totalServiceCharge - $bookingprice; /* without booking price **/
										$subtotal = $data->totalServiceCharge; /* with booking price **/
										$gst = $data->gst;
										$grand_total = 1 *$subtotal;
										$sum = $sum + $grand_total;
									?>
									${{sprintf('%1.2f',$grand_total)}}
								</td>	
							</tr>	
						@endforeach
						<tr style="color:#fff;background:#005343">
							<td colspan="7" class="text-center">
								<strong style="text-transform:uppercase;color:#fff ">Total Monthly Revenue</strong>
							</td>
							<td>
								<strong style="text-transform:uppercase;color:#fff ">${{sprintf('%1.2f',$sum)}}</strong>
							</td>
						</tr>
					</tbody>
				</table>		
			</div>
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>