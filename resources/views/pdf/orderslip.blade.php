<htmlpageheader name="page-header">
	Bin Hire Quote from Invoice {{$invoiceDetails->paymentUniqueCode}}
</htmlpageheader>
<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					body{
						font-size:11px;
						line-height:1.9px;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.7;
					}
					table{
						line-height:1.7;
					}
				} 
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr style="text-align:center">
						<td width="50%">
							<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
							</address>
						</td>
					</tr>
					<tr style="text-align:center">
						<td colspan="2">
							<h3 style="text-transform:uppercase;">Order Slip </h3> <br />
							<strong style="text-transform:uppercase;">Invoice no. {{$invoiceDetails->paymentUniqueCode}}</strong><br />
							<strong><?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?></strong> </strong><br />
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr>
						<td width="50%">
							<h3 style="text-transform:uppercase;">Customer Delivery Details:</h3>
							<address style="font-style:12px;">
								{{$customerdetails->name}}<br/>
								@if(!is_null($customerdetails->unit_lot)){{$customerdetails->unit_lot}}@endif {{$customerdetails->address}} <br/>
								{{$customerdetails->zipcode}} <br/>
								{{$customerdetails->phone}} <br/>
								{{$customerdetails->email}}
							</address>
						</td>
						
					</tr>
				</table>
			</div>

			<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%" cellpadding="3">
					<tr>
						<td  width="50%" >
							<h3 style="text-transform:uppercase;">Delivery Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
							</address>
						</td>
						<td  width="50%" style="padding-left: 20px;">
							<h3 style="text-transform:uppercase;">Collection Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
							</address>
						</td>
					</tr>
				</table>
			</div>

			<div class="table-responsive">
				<table class="table table-condensed" border="1" style="border:1px solid #b1b2b2;" width="100%" cellpadding="3">
					<tbody>
						<tr style="background:#005343;">
							<td colspan="3" style="color:#fff;text-align:center;text-transform:uppercase;"><strong><h3>Order summary</h3></strong></td>
						</tr>
						<tr>
							<td ><strong>Bin type</strong></td>
							<td colspan="2" >{{$binhire->name}}</td>
						</tr>
						<tr>
							<td ><strong>Bin size</strong></td>
							<td colspan="2" >{{$binhire->size}}</td>
						</tr>
						<tr>
							<td ><strong>Description</strong></td>
							<td colspan="2" >
								<?php $tags = array("strong", "b");?>
								<p><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
								<p style="color:#c40005;"><?php echo $binhire->description; ?></p>
							</td>
						</tr>
						<tr>
							<td ><strong>Note</strong></td>
							<td colspan="2" >
								@if(!is_null($seasonal_mode))
									@if($seasonal_mode == '1')
										{{'Winter 2019 Special Pricing'}}
									@else
										{{'Normal Pricing'}}
									@endif
								@else
									{{'Normal Pricing'}}
								@endif
							</td>
						</tr>
						<!--<tr>
							<td  style="text-transform:uppercase;"><strong>Bin price</strong></td>
							<td colspan="2"  style="text-transform:uppercase;">${{sprintf('%1.2f',$binhire->price)}}</td>
						</tr>-->
						<!--<tr>
							<td><strong>GST 10%</strong></td>
							<?php $gst = $invoiceDetails->subtotal*0.10;?>
							<td colspan="2">${{sprintf('%1.2f',$gst)}}</td>
						</tr>-->
						<?php 
							$subtotal = $invoiceDetails->subtotal;
							$commision = 0.15*$subtotal;
							$grand_total = 0.85*$subtotal;
						?>
						<!--<tr>
							<td><strong>Fee</strong></td>
							<td colspan="2">- ${{sprintf('%1.2f',$commision)}}</td>
						</tr>-->
						
						<tr >
							<td ><strong>Price</strong></td>
							<td colspan="2" >
							$<?php echo sprintf('%1.2f',$invoiceDetails->subtotal);?></td>
						</tr>
						<tr >
							<td ><strong >Extras</strong></td>
							<td colspan="2" >
								<?php $totalextras =  0; ?>
								@if(!is_null($orderextras))
									@foreach($orderextras as $k=> $v)
										<?php $totalextras = $totalextras + $v->charge?>
									{{$v->name}} / $<?php echo sprintf('%1.2f',$v->charge).'<br />';?>
									@endforeach
								@else
									{{'-'}}
								@endif
							</td>
						</tr>
						<tr>
							<td ><strong>Bin hire price (including extras)</strong></td>
							<td colspan="2">
							$<?php echo sprintf('%1.2f',$invoiceDetails->subtotal + $totalextras);?></td>
						</tr>
						<tr style="color:#fff;background:#005343">
							<td style="color:#fff;"><strong>Grand total(including booking fee)</strong></td>
							<td colspan="2" style="color:#fff;">
							$<?php echo sprintf('%1.2f',$invoiceDetails->totalServiceCharge);?></td>
						</tr>
					</tbody>
				</table>
			</div>
			@if(!is_null($invoiceDetails->deliveryComments))
				<div class="table-responsive">
					<table class="table table-condensed" border="0" width="100%" cellpadding="3">
						<tr>
							<td width="50%">
								<h3 style="text-transform:uppercase;">Bin Placement:</h3>
								<address  style="font-style:12px;">
									{{$invoiceDetails->deliveryComments}}
								</address>
							</td>
						</tr>
					</table>
				</div>
			@endif
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>