<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sea Express Backend</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/plugins/datetimepicker/css/daterangepicker.css')}}" rel="stylesheet" /> 
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<body class="adminbody">
	<div id="main" class="forced enlarged">
		<div class="headerbar">
			<div class="headerbar-left">
				<a href="{{url('/away')}}" class="logo"><img alt="Logo" src="{{url('assets/images/logo_03.png')}}" /></a>
			</div>
			<nav class="navbar-custom">
				
				<ul class="list-inline float-right mb-0">
					<li class="list-inline-item dropdown notif">
						<a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<img src="{{url('assets/images/admin.png')}}" alt="Profile image" class="avatar-rounded">
						</a>
						<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5 class="text-overflow"><small>{{session('conName')}} , {{session('email')}}</small> </h5>
							</div>
	
							<!-- item-->
							<a href="/details_service_zone" class="dropdown-item notify-item">
								<i class="fa fa-user"></i> <span>Profile</span>
							</a>
							
							@if(session('role'))
								@if(session('role') == '1')
									<?php /**
									<a href="{{url('manage_user')}}" class="dropdown-item notify-item">
										<i class="fa fa-user-circle"></i> <span>Manage User</span>
									</a>
									**/
									?>
									<a href="{{url('manage_order')}}" class="dropdown-item notify-item">
										<i class="fa fa-shopping-cart"></i> <span>Manage Bookings</span>
									</a>
									<?php /**
									<a href="{{url('all_supplier_order')}}" class="dropdown-item notify-item">
										<i class="fa fa-shopping-cart"></i> <span>All Bookings</span>
									</a>
									
									<a href="{{url('manage_summary')}}" class="dropdown-item notify-item">
										<i class="fa fa-file-pdf-o"></i> <span>Booking Summary</span>
									</a>
									**/
									?>
									<a href="{{url('booking_fee')}}" class="dropdown-item notify-item">
										<i class="fa fa-money"></i> <span>Booking Fee</span>
									</a>
								@endif
							@endif
							

							<!-- item-->
							<a href="/logout" class="dropdown-item notify-item">
								<i class="fa fa-power-off"></i> <span>Logout</span>
							</a>

						</div>
					</li>
				</ul>
				
				<ul class="list-inline menu-left mb-0">
					<li class="float-left">
						<button class="button-menu-mobile open-left">
							<i class="fa fa-fw fa-bars"></i>
						</button>
					</li>
				</ul>
			</nav>
		</div>
		<!-- End Navigation -->
		<!-- Left Sidebar -->
		<div class="left main-sidebar">
			<div class="sidebar-inner leftscroll">
				<div id="sidebar-menu">
					<ul>
						<li class="submenu">
							<a {{{ (Request::is('instructions') ? 'class=active' : '') }}} href="/instructions"><i class="fa fa-fw fa-info-circle"></i><span> Intructions & Informations</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('details_service_zone') ? 'class=active' : '') }}} {{{ (Request::is('details_service_zone/{status}/{message}') ? 'class=active' : '') }}} href="/details_service_zone"><i class="fa fa-fw fa-id-card-o"></i><span> Supplier Details</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('mixed_renovation') ? 'class=active' : '') }}} {{{ (Request::is('mixed_renovation/{status}.{message}') ? 'class=active' : '') }}} href="/mixed_renovation"><i class="fa fa-fw fa-circle"></i><span> Pricing</span> </a>
						</li>
						<?php /** 
						<li class="submenu">
							<a {{{ (Request::is('heavy_landscaping_waste') ? 'class=active' : '') }}} {{{ (Request::is('heavy_landscaping_waste/{status}.{message}') ? 'class=active' : '') }}} href="/heavy_landscaping_waste"><i class="fa fa-fw fa-circle"></i><span> Return</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('domestic_light') ? 'class=active' : '') }}} {{{ (Request::is('domestic_light/{status}.{message}') ? 'class=active' : '') }}} href="/domestic_light"><i class="fa fa-fw fa-circle"></i><span> Multi-destinations </span> </a>
						</li>
						
						**/ ?>
						<li class="submenu">
							<a {{ (Request::is('order_receipt') ? 'class=active' : '') }} href="/order_receipt"><i class="fa fa-fw fa-file"></i><span> Order Receipt </span> </a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- End Sidebar -->
		@section('content')
		@show
		<!-- END content-page -->
		
		<footer class="footer">
			<span class="text-right">
				Copyright <a target="_blank" href="https://baliwebdesignservice.id">Putu Winata</a>
			</span>
		</footer>
	</div>
<!-- END main -->
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
</body>
</html>