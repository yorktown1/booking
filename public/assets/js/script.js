$(document).ready(function(){
	$('iframe#paynow').contents().find("head").append(
			$("<style type='text/css'>"+
			"body {"+
				"background-color: #efefef;"+
			"} "+
			" </style>")
	);
	$('.collapse [name="selectall"]').change(function(){
		var prop = $(this).prop('checked');
		
		if(prop == true){
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', true);
			})
		} else {
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
		}
	});
	
	$('.collapse [name="unselectall"]').change(function(){
		var prop = $(this).prop('checked');
		
		if(prop == true){
			var carbody = $(this).parent().parent();
			var checkboxes = carbody.find('.zipcode');
			checkboxes.each(function(){
				$(this).prop('checked', false);
			})
		} 
	});

	var start = moment().subtract(29, 'days');
	var end = moment();

	function cb(start, end) {
        $('input[name="order_management_datepicker"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        $('input[name="start_order_date"]').val(start.format('YYYY-MM-DD'));
        $('input[name="end_order_date"]').val(end.format('YYYY-MM-DD'));
	}

	$('input[name="order_management_datepicker"]').daterangepicker({
		startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
	}, cb);
	cb(start, end);
	
});