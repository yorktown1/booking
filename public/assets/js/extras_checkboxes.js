$(document).ready(function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(".extras_checkboxes").click(function(){
		var total = 0.00;
		$.each($("input[name='extras[]']:checked"), function() {
			 total += parseFloat($(this).attr("data-value"));
			 console.log(total);
		});
		if (total == 0) {
			$('#extras-price').empty();
			$('#extras-price').html('-');
			var initial = $("input[name='initial']").val();
			$('#grandtotal').empty();
			$('#grandtotal').html('$'+initial);
			$("input[name='totalprice']").val(initial);
			
			var gst_initial = $("input[name='initial_gst']").val();
			$('#gst').empty();
			$('#gst').html('$'+gst_initial);
			$("input[name='gst']").val(gst_initial);
			
		} else {
			$('#extras-price').empty();
			$('#extras-price').html('$'+parseFloat(total)+'.00');
			var initial = $("input[name='initial']").val();
			var grandtotal = $("input[name='totalprice']").val();
			var gst_initial = $("input[name='initial_gst']").val();
			var gst = $("input[name='gst']").val();
			$.ajax({
				dataType: 'json',
				type: "POST",
				url: checkboxes_url,
				data: {grande:initial, total: total, gst:gst_initial}
			}).done(function(data) {
				$("input[name='totalprice']").val(parseFloat(data.super_total));
				$('#grandtotal').empty();
				$('#grandtotal').html('$'+data.super_total);
				$("input[name='gst']").val(parseFloat(data.gst_total));
				$('#gst').empty();
				$('#gst').html('$'+data.gst_total);
			});
		}
	});
	
});